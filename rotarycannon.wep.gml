#define init
global.sprRotaryCannon1 = sprite_add_weapon("sprRotaryCannon1.png", 7, 5);
global.sprRotaryCannon2 = sprite_add_weapon("sprRotaryCannon2.png", 7, 5);
global.rotcanAuto = 1
global.rotcanLoad = 3
global.rotcanCount = 0
global.sprRotaryCannonCurrent = global.sprRotaryCannon1
#define weapon_name
return "ROTARY CANNON";
#define weapon_type
return 1;
#define weapon_auto
return global.rotcanAuto;
#define weapon_cost
return 2;
#define weapon_load
return global.rotcanLoad;
#define weapon_sprt
return global.sprRotaryCannonCurrent;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "weight";
#define weapon_fire
var __angle = gunangle;
var __len = 5;
if global.rotcanCount == 0 {
	global.rotcanLoad = 6
	global.sprRotaryCannonCurrent = global.sprRotaryCannon2
	sound_play(sndHeavyMachinegun);
	with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
	with (instance_create(x+lengthdir_x(__len,__angle), y+lengthdir_y(__len,__angle), HeavyBullet)) {
		motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
		image_angle = direction;
		team = other.team;
		creator = other;
	global.rotcanAuto = 1
	global.rotcanCount = 1
	wait 1;
	global.sprRotaryCannonCurrent = global.sprRotaryCannon1
	wait 1;
	global.sprRotaryCannonCurrent = global.sprRotaryCannon2
	wait 2;
	global.sprRotaryCannonCurrent = global.sprRotaryCannon1
	}
} else {
	global.rotcanLoad = 3
	global.sprRotaryCannonCurrent = global.sprRotaryCannon2
	sound_play(sndHeavyMachinegun);
	with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
	with (instance_create(x+lengthdir_x(__len,__angle), y+lengthdir_y(__len,__angle), HeavyBullet)) {
		motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
		image_angle = direction;
		team = other.team;
		creator = other;
	global.rotcanCount = global.rotcanCount + 1
	if global.rotcanCount == 4 {
		global.rotcanLoad = 3
		global.rotcanAuto = 0
		global.rotcanCount = 0
	}
	}
	wait 1;
	global.sprRotaryCannonCurrent = global.sprRotaryCannon1
}
weapon_post(5, -6, 5);