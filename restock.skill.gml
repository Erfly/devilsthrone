#define init
global.sprValueHUD = sprite_add("mutations/sprValueHUD.png", 1, 7, 8);
global.sprValue = sprite_add("mutations/sprValue.png", 1, 12, 16);
global.restack=0
global.dorestock=1

#define game_start
global.restack=0
global.dorestock=1

#define skill_name
return "Restock"

#define skill_text
return "Gain 3 pickups when#level is clear"

#define skill_take 
sound_play(sndMut)
global.restack+=1

#define step
if instance_exists(SpiralCont){
if global.dorestock=1
{
global.dorestock=0
	with Player{
		temp=0
		while temp<global.restack{
			with Player instance_create(x,y,SuperFrog)
		temp+=1
	}
	}

}
}
else
global.dorestock=1

#define skill_icon
return global.sprValueHUD;

#define skill_button 
sprite_index = global.sprValue;

#define skill_tip
return choose("FREE MONY", "FREE AMMO")
