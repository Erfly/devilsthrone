#define init
global.sprValueHUD = sprite_add("mutations/sprValueHUD.png", 1, 7, 8);
global.sprValue = sprite_add("mutations/sprValue.png", 1, 12, 16);
global.stacks=0

#define game_start
global.stacks=0

#define skill_name
return "Value Mutation"

#define skill_text
return "All stats up"

#define skill_take 
sound_play(sndMutant6Wrld)
global.stacks+=1
with(Player)
{
    maxhealth += 1;
    my_health += 1;
}

#define step
with(Player)
{
    if("valuespeed" not in self)
    {
        valuespeed = 1;
        maxspeed += 0.25;
    }
}
with(Player)
{
    if("valueacc" not in self)
	{
		valueacc = 1;
		accuracy -= 0.15;
	}
}
with(Player)
{
	if("valueammo" not in self)
	{
		valueammo = 1;
		typ_amax[1] += 110;
		typ_amax[2] += 10;
		typ_amax[3] += 10;
		typ_amax[4] += 10;
		typ_amax[5] += 10;	
	}
}

with(Player){
	if reload >0
	{
		reload -= 0.2*global.stacks
	}
}


#define skill_icon
return global.sprValueHUD;

#define skill_button 
sprite_index = global.sprValue;

#define skill_tip
return choose("ALL STATS UP", "ONE OF EVERYTHING")
