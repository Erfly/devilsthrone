#define init
global.sprTheClassic = sprite_add_weapon("sprTheClassic.png", 0, 0);
global.theClassicAmmo = 1
#define weapon_name
return "The Classic(TM)";
#define weapon_type
return global.theClassicAmmo;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprTheClassic;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
if (global.theClassicAmmo == 1) {
	sound_play(sndPistol);
	with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
	with (instance_create(x, y, Bullet1)) {
		motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	global.theClassicAmmo = 2
} if (global.theClassicAmmo == 2) {
sound_play(sndShotgun);
	repeat (7) {
		with (instance_create(x, y, Bullet2)) {
			motion_add(__angle + (random(40) - 20) * other.accuracy, 12 + random(6));
			image_angle = direction;
			team = other.team;
			creator = other;
		}
	}
	global.theClassicAmmo = 3
}  if (global.theClassicAmmo == 3) {
	sound_play(sndCrossbow);
	with (instance_create(x, y, Bolt)) {
		motion_add(__angle, 24);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	global.theClassicAmmo = 4
}   if (global.theClassicAmmo == 3) {
	sound_play(sndGrenade);
	with (instance_create(x, y, Grenade)) {
		sticky = 0;
		motion_add(__angle + (random(6) - 3) * other.accuracy, 10);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	global.theClassicAmmo = 5
} else {
	sound_play(sndLaser);
	with (instance_create(x, y, Laser)) {
		image_angle = __angle + (random(2) - 1) * other.accuracy;
		team = other.team;
		event_perform(ev_alarm, 0);
	}
	global.theClassicAmmo = 1
}
weapon_post(2, -6, 4);