#define init
//load stuff here
global.sprHomingMissile = sprite_add("weps/sprites/sprHomingMissile.png", 4, 8, 8);
global.sprInfiltratorGrenade = sprite_add("weps/sprites/sprInfiltratorGrenade.png", 1, 3, 3);
global.sprFlareBlue = sprite_add("weps/sprites/sprFlareBlue.png", 2, 3, 3);
global.sprFlareGreen = sprite_add("weps/sprites/sprFlareGreen.png", 2, 3, 3);
global.sprFlareYellow = sprite_add("weps/sprites/sprFlareYellow.png", 2, 3, 3);
global.sprFlarePink = sprite_add("weps/sprites/sprFlarePink.png", 2, 3, 3);
global.sprFireGreen = sprite_add("weps/sprites/sprFireGreen.png", 7, 8, 8);
global.sprFireYellow = sprite_add("weps/sprites/sprFireYellow.png", 7, 8, 8);
global.sprFirePink = sprite_add("weps/sprites/sprFirePink.png", 7, 8, 8);
global.sprFireworkRocket = sprite_add("weps/sprites/sprFireworkRocket.png", 1, 8, 3);
global.sprDrill = sprite_add("weps/sprites/sprDrill.png", 4, 8, 8);
global.sprLaserSentry = sprite_add("weps/sprites/sprLaserSentry.png", 1, 6, 6);
global.sprLaserSentryHurt = sprite_add("weps/sprites/sprLaserSentryHurt.png", 3, 6, 6);
global.sprLaserSentryDead = sprite_add("weps/sprites/sprLaserSentryDead.png", 4, 6, 6);
global.sprSentryLaser = sprite_add("weps/sprites/sprSentryLaser.png", 1, 0, 3);
global.sprBulletTrail = sprite_add("weps/sprites/sprBulletTrail.png", 2, 0, 1);
global.sprFireLaser = sprite_add("sprFireLaser.png", 1, 2, 3);
global.sprFireLaserStart = sprite_add("sprFireLaserStart.png", 8, 8, 6);
global.sprFireLaserEnd = sprite_add("sprFireLaserEnd.png", 8, 10, 8);
global.sprChasmBullet = sprite_add("weps/sprites/sprChasmBullet.png", 2, 6, 8);
global.sprMiniPlasmaImpact = sprite_add("weps/sprites/sprMiniPlasmaImpact.png", 7, 8, 8);
global.mskMiniPlasmaImpact = sprite_add("weps/sprites/mskMiniPlasmaImpact.png", 8, 12, 12);
sprite_collision_mask(global.mskMiniPlasmaImpact, false, 1, 0, 0, 0, 0, 0, 0);
global.sprAntiFlakBullet = sprite_add("weps/sprites/sprAntiFlakBullet.png", 2, 8, 8);
global.sprAntiBullet2 = sprite_add("weps/sprites/sprAntiBullet2.png", 2, 8, 8);
global.sprAntiFlakHit = sprite_add("weps/sprites/sprAntiFlakHit.png", 8, 16, 16);
global.sprAntiBullet2Hit = sprite_add("weps/sprites/sprAntiBullet2Hit.png", 5, 8, 8);
global.sprIpecacBall = sprite_add("weps/sprites/sprIpecacBall.png", 2, 8, 8);
global.sprCrystalBullet = sprite_add("weps/sprites/sprCrystalBullet.png", 4, 8, 8);
global.sprImpactSlash = sprite_add("weps/sprites/sprImpactSlash.png", 3, 0, 24);
global.sprLongBullet = sprite_add("weps/sprites/sprLongBullet.png", 3, 0, 4);
global.mskLongBullet = sprite_add("weps/sprites/mskLongBullet.png", 1, 0, 8);
sprite_collision_mask(global.mskLongBullet, false, 1, 0, 0, 0, 0, 0, 0);
global.sprUltraSlug = sprite_add("weps/sprites/sprUltraSlug.png", 2, 16, 16);
global.sprUltraSlugDisappear = sprite_add("weps/sprites/sprUltraSlugDisappear.png", 6, 16, 16);
sprite_collision_mask(global.sprUltraSlug, false, 2, 5, 12, 29, 19, 0, 0);
global.sprJusticeRocket = sprite_add("weps/sprites/sprJusticeRocket.png", 1, 2, 4);
global.sprDynamite = sprite_add("weps/sprites/sprDynamite.png", 1, 7, 3);
global.sprDroneBody = sprite_add("weps/sprites/sprDroneBody.png", 2, 9, 9)
global.sprUltraDroneBody = sprite_add("weps/sprites/sprUltraDroneBody.png", 2, 9, 9)
global.sprDroneBlade = sprite_add("weps/sprites/sprDroneBlade.png", 4, 5, 5)

//areas sprites
global.sprTopDecalVan = sprite_add("areas/sprites/sprTopDecalVan.png", 1, 32, 24);
global.sprTopDecalVanDig = sprite_add("areas/sprites/sprTopDecalVanDig.png", 7, 32, 24);
global.mskTopDecalHuge = sprite_add("areas/sprites/mskTopDecalHuge.png", 1, 32, 24);

//races sprites
global.sprLog = sprite_add("races/sprites/sprLog.png", 1, 12, 12);
global.sprLogHurt = sprite_add("races/sprites/sprLogHurt.png", 3, 12, 12);
sprite_collision_mask(global.sprLog, false, 2, 3, 5, 20, 21, 1, 0);
global.mskMiniPortal = sprite_add("races/sprites/mskMiniPortal.png", 1, 8, 8);
sprite_collision_mask(global.mskMiniPortal, false, 0, 0, 0, 0, 0, 2, 0);
global.sprMiniPortalOutline = sprite_add("races/sprites/sprMiniPortalOutline.png", 3, 9, 9);
global.sprHudKnife = sprite_add("races/sprites/sprHudKnife.png", 1, 0, 0);
global.sprGuardianShock = sprite_add("races/sprites/sprPortalShock.png", 5, 48, 48);

//snd
global.sndMissileExplode = sound_add("weps/sounds/sndMissile.ogg");
global.sndDucked = sound_add("weps/sounds/sndDucked.ogg");

global.sndChasmUpg = sound_add("weps/sounds/sndChasmUpg.ogg");
global.sndChasm = sound_add("weps/sounds/sndChasm.ogg");

//other
global.listIDPD = [Shielder, Inspector, Grunt, EliteShielder, EliteInspector, EliteGrunt, PopoFreak];
global.listIDPDSpawners = [Van, IDPDSpawn];

global.generating = 0;

#define step
if(!(instance_exists(GenCont) || instance_exists(mutbutton)) and global.generating)
{
	with(instances_matching(TopSmall, "decalled", null)) {
		if(GameCont.loops > 0) {
			decalled = 1;
			if(random(200) < 1) {
				with obj_create("Decal", x, y) {
					name = "VanDecal";
					
					var area;
					area = GameCont.area;
					
					if(!(is_string(area)) and area > 0 and area < 7)
					{
						sprite_index = global.sprTopDecalVanDig;
						image_index = area - 1;
					}
					else
					{
					instance_delete(self);
					exit;
					}
					mask_index = global.mskTopDecalHuge;
				
					if(place_meeting(x, y, FloorExplo)) {
						instance_delete(self);
						exit;
					}
					
					on_destroy = script_ref_create(vandecal_destroy);
				}
			}
		}
	}
	
	with instances_matching(Player,"race","assassin"){ //adding this because resetting the global variable on the first player fucks mp
		if(knife_count < 1)
		knife_count = 1;
		
		stealth = 1;
		if instance_exists(wallbuddy) instance_delete(wallbuddy)
	}
	global.generating = 0;
}
else if(instance_exists(GenCont) || instance_exists(mutbutton))
{
	global.generating = 1;
}

with(instances_matching(WepPickup, "pointer_change", null))
{
	pointer_change = 1;

	if(mod_exists("wep", "pointer") and ammo>0 and random(2000) < 1)
	wep = "pointer";
}

with(instances_matching_le(PizzaBox, "my_health", 0))
{
	if(mod_exists("wep", "pizzacutter"))
	{
		with(instance_create(x, y, WepPickup))
		{
			ammo = 50;
			wep = "pizzacutter";
		}
	}
}

//recycle gland buff
with(instances_matching(SkillIcon, "skill", mut_recycle_gland)) {
     if("newtext" not in self) {
          newtext = 1;
          text += "#BONUS @wBULLET@s DAMAGE";
     }
}

if(skill_get(mut_recycle_gland)) {
	with(instances_matching([Bullet1, HeavyBullet, UltraBullet, BouncerBullet], "buffedgland", null)) {
		buffedgland = 1;
		damage++;
		if(object_index = UltraBullet)
		damage+=2;
	}
}

//freakz
with(instances_matching(global.listIDPD, "freakified", null)) {
	freakified = 1;
	if(GameCont.loops >= 3 and random(max(4, 8 - (GameCont.loops - 3))) < 1) xor (object_index = PopoFreak) {
		obj_create("NewFreak", x, y);
		instance_delete(self);
	}
}

with(instances_matching(global.listIDPDSpawners, "freakified", null)) {
	freakified = 1;
	if(random(2) < 1)
	freak = 0;
}

#define draw_gui
with instances_matching(Player,"race","assassin"){
    with Player draw_set_visible(index,0)
    draw_set_visible(index,1)
    draw_sprite(global.sprHudKnife,0, 110, 19)
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
    draw_text_shadow(115, 22, string(knife_count))
}

#define draw_shadows
	with instances_matching(CustomProjectile,"name","Drone","Dronade","Ipecac")
	{
		draw_sprite(shd24,0,x,y)
	}

#define draw_dark
with instances_matching(CustomObject,"name","MiniPortal")
	{
	//draw_set_alpha(1)
	//draw_set_color(0)
	draw_circle(x, y, 40+random(4), false)
	}
	
#define draw_bloom
with instances_matching(CustomProjectile,"name","PopCannon","HyperBullet","ChasmBullet","CrystalBullet")
	{
	draw_sprite_ext(sprite_index, image_index, x, y, 2 * image_xscale, 2 * image_yscale, image_angle, image_blend, image_alpha * 0.1);
	}
with instances_matching(CustomObject,"name","MiniPortal")
	{
	draw_sprite_ext(sprite_index, image_index, x, y, 2 * image_xscale, 2 * image_yscale, image_angle, image_blend, image_alpha * 0.1);
	}
with instances_matching(CustomProjectile,"name","AntiFlakBall","AntiShell","HyperShell","UltraSlug")
	{
	draw_sprite_ext(sprite_index, image_index, x, y, 2 * image_xscale, 2 * image_yscale, image_angle, image_blend, image_alpha * (0.1 + 0.2*sign(bonus)));
	}
with instances_matching(CustomProjectile,"name","SentryLaser")
	{
	draw_sprite_ext(sprite_index,-1,x,y,image_xscale,draw_yscale*2,image_angle,c_white,0.1)
	}
with instances_matching(CustomProjectile,"name","ZapLightning","StraightLightning")
	{
	draw_sprite_ext(sprite_index,-1,x,y,image_xscale,image_yscale*2,image_angle,c_white,0.1)
	}
with instances_matching(CustomObject,"name","BulletTrail")
	{
	draw_sprite_ext(sprite_index,-1,x,y,image_xscale,image_yscale*2,image_angle,c_white,image_alpha/8)
	}
with instances_matching(CustomProjectile,"name","FireLaser")
	{
	draw_sprite_ext(sprite_index,-1,x,y,image_xscale,2,image_angle,c_white,0.1)
	draw_sprite_ext(global.sprFireLaserStart,img,xstart,ystart,2,2,image_angle+180,c_white,0.1)
	draw_sprite_ext(global.sprFireLaserEnd,img,x-lengthdir_x(image_xscale*2,image_angle),y-lengthdir_y(image_xscale*2,image_angle),2,2,image_angle+180,c_white,0.1)
	}
with instances_matching(CustomProjectile,"name","Ipecac")
	{
	draw_sprite_ext(sprite_index,-1,x,y-z,2,2,image_angle,c_white,0.1)
	}
with instances_matching(CustomProjectile,"name","LongBullet")
	{
	draw_sprite_ext(global.sprLongBullet,0,x-lengthdir_x(8,image_angle),y-lengthdir_y(8,image_angle),2,2,image_angle,c_white,0.1)
	if image_xscale>1 draw_sprite_ext(global.sprLongBullet,1,x+lengthdir_x(8,image_angle),y+lengthdir_y(8,image_angle),image_xscale*2-2,2,image_angle,c_white,0.1)
	draw_sprite_ext(global.sprLongBullet,2,x+lengthdir_x(8+16*(image_xscale-1),image_angle),y+lengthdir_y(8+16*(image_xscale-1),image_angle),2,2,image_angle,c_white,0.1)
	if growing>0 draw_sprite_ext(sprBullet1,0,x,y,2,2,image_angle,c_white,0.1)
	}

#define instance_nth_nearest
    var pointx,pointy,object,n,list,nearest,gridnum,objnum;
    pointx = argument0;
    pointy = argument1;
    object = argument2;
	objnum = instance_number(object);
	if objnum = 0 return noone;
	else {
		n = argument3;
		n = min(max(1,n),instance_number(object));
		list = ds_grid_create(2,instance_number(object));
		nearest = noone;
		gridnum = 0;
		with (object) {
			ds_grid_set(list,0,gridnum,id);
			ds_grid_set(list,1,gridnum,distance_to_point(pointx,pointy));
			gridnum+=1
		}
		ds_grid_sort(list,1,true);
		
		/*gridnum=0
		while gridnum<objnum
		{
		trace(string(ds_grid_get(list,0,gridnum))+" "+string(ds_grid_get(list,1,gridnum)))
		gridnum+=1;
		}*/
		
		nearest = ds_grid_get(list,0,n-1);
		
		//trace(string(nearest));
		
		ds_grid_destroy(list);
		return nearest;
	}

#define snd_chasm
if skill_get(17) {
sound_play_gun(global.sndChasmUpg, 0.15, 0.6);
} else {
sound_play_gun(global.sndChasm, 0.15, 0.6);
}

#define obj_create
//obj_create(name,x,y)
var e;
e=noone;
switch(argument0)
{
	case "SeekerMissile":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "SeekerMissile";
			sprite_index = global.sprHomingMissile;
			mask_index = mskSeeker;
			hitid = [global.sprHomingMissile, "MISSILE"];
			image_speed = 0.4;
			damage = 2;
			force = 6;
			typ = 2;
			alarm0 = 180;
			
			bounce = 1
			alarm0 = 35
			hp = 5
			
			on_step = script_ref_create(missil_step);
			on_wall = script_ref_create(missil_wall);
			on_draw = script_ref_create(missil_draw);
			on_destroy = script_ref_create(missil_destroy);
		}
		break;
	case "Infiltrator":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "Infiltrator";
			sprite_index = global.sprInfiltratorGrenade;
			mask_index = sprGrenade;
			hitid = [global.sprInfiltratorGrenade, "INFILTRATOR"];
			friction = 0.1
			alarm1 = 6
			alarm0 = 45
			typ = 1
			
			on_step = script_ref_create(infilt_step);
			on_wall = script_ref_create(infilt_wall);
			on_hit = script_ref_create(infilt_hit);
			on_destroy = script_ref_create(infilt_destroy);
		}
		break;
	case "HyperBullet":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "HyperBullet";
			sprite_index = sprBullet1;
			mask_index = mskBullet1;
			hitid = [sprBullet1, "HYPER BULLET"];
			alarm0 = 1;
			alarm1 = -1;
			typ = 1;
			damage = 3+skill_get(16);
			force = 8;
			image_speed=1;
			
			regen = 1;
			
			on_destroy = script_ref_create(hyperb_destroy);
			on_step = script_ref_create(hyperb_step);
			on_hit = script_ref_create(hyperb_hit);
			on_wall = script_ref_create(hyperb_wall);
			on_anim = script_ref_create(hyperb_anim);
		}
		break;
	case "Firework":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "Firework";
			color = choose(0,1,2,3,4)
			switch (color) {
				case 0:
				sprite_index = sprFlare
				flame_color = sprTrapFire
				break;
				case 1:
				sprite_index = global.sprFlareBlue
				flame_color = sprFireLilHunter
				break;
				case 2:
				sprite_index = global.sprFlareGreen
				flame_color = global.sprFireGreen
				break;
				case 3:
				sprite_index = global.sprFlareYellow
				flame_color = global.sprFireYellow
				break;
				case 4:
				sprite_index = global.sprFlarePink
				flame_color = global.sprFirePink
				break;
			}
			star = choose(0,0,0,1)
			mask_index = sprGrenade;
			hitid = [sprFlare, "Firework"];
			friction = 0.1
			alarm1 = 6
			alarm0 = 30
			typ = 1
			damage = 10
			force = 5
			image_speed = 0.4
			
			on_step = script_ref_create(firework_step);
			on_wall = script_ref_create(firework_wall);
			on_destroy = script_ref_create(firework_destroy);
		}
		break;
	case "ShootingStar":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "ShootingStar";
			sprite_index = global.sprFireworkRocket;
			mask_index = mskBullet1;
			hitid = [global.sprFireworkRocket, "SHOOTING STAR"];
			damage = 25;
			force = 8;
			typ = 1;
			timer = 0;
			angswitch = choose(-1,1);
			sound_play(sndRocketFly)
			
			on_step = script_ref_create(star_step);
			on_draw = script_ref_create(star_draw);
			on_destroy = script_ref_create(star_destroy);
		}
		break;
	case "LaserSentry":
		e=(instance_create(argument1,argument2,CustomHitme))
		with(e)
		{
			name = "LaserSentry";
			snd_hurt = sndHitMetal;
			spr_idle = global.sprLaserSentry;
			spr_hurt = global.sprLaserSentryHurt;
			spr_dead = global.sprLaserSentryDead;
			image_speed = 0.4
			size = 1
			sprite_index = spr_idle;
			mask_index = mskRat;
			maxhealth = 7
			my_health = maxhealth
			active = 30;
			friction = 0.25;
			team = 2;
			death = 500;
			
			tar[0] = noone
			tar[1] = noone
			tar[2] = noone
			
			ammo = 240
			if skill_get(17)=1
			ammo = 340
			
			on_step = script_ref_create(lsentry_step);
			on_destroy = script_ref_create(lsentry_destroy);
			on_hurt = script_ref_create(lsentry_hurt);
		}
		break;
	case "SentryLaser":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "SentryLaser"
			sprite_index = global.sprSentryLaser;
			mask_index = sprite_index;
			draw_yscale=0.5+random(0.2)
			typ = 0
			
			on_hit = script_ref_create(senlaser_hit);
			on_draw = script_ref_create(senlaser_draw);
			on_step = script_ref_create(senlaser_step);
		}
		break;
	case "Drill":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "Drill";
			sprite_index = global.sprDrill;
			mask_index = mskEnemyBullet1;
			hitid = [global.sprDrill, "DRILL"];
			force = 10;
			typ = 1;
			alarm1 = 5;
			sound_play(sndRocketFly)
			stuck[0]=noone
			
			active=0
			image_speed=0.4
			
			on_step = script_ref_create(drill_step);
			on_draw = script_ref_create(drill_draw);
			on_hit = script_ref_create(drill_hit);
			on_destroy = script_ref_create(drill_destroy);
		}
		break;
	case "ZapLightning":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with (e)
		{
			name = "ZapLightning";
			sprite_index = sprLightning;
			mask_index = mskLaser;
			hitid = [sprLightning, "ZAPPER"];
			typ = 0;
			damage = 7;
			force = 4;
			
			alarm1=-1
			image_speed = 0.4

			if skill_get(17) = true {
				image_speed = 0.3
			}
			
			on_step = script_ref_create(zap_step);
			on_anim = script_ref_create(zap_anim);
			on_draw = script_ref_create(zap_draw);
			on_hit = script_ref_create(zap_hit);
			on_wall = script_ref_create(zap_wall);
		}
		break;
	case "BulletTrail":
		e=(instance_create(argument1,argument2,CustomObject))
		with (e)
		{
			name = "BulletTrail";
			sprite_index = global.sprBulletTrail;
			depth = 1;
			
			on_step = script_ref_create(btrail_step);
		}
		break;
	case "FireLaser":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "FireLaser"
			sprite_index = global.sprFireLaser;
			mask_index = mskLaser;
			hitid = [global.sprFireLaser, "FIRE LASER"];
			typ = 0;
			alarm0 = 2;
			damage = 2;
			force = 4;
			
			image_yscale = 1
			if (skill_get(17)) image_yscale = 1.4
			
			on_hit = script_ref_create(flaser_hit);
			on_draw = script_ref_create(flaser_draw);
			on_step = script_ref_create(flaser_step);
			on_wall = script_ref_create(flaser_wall);
			
			img = 0
		}
		break;
	case "ChasmBullet":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "ChasmBullet";
			sprite_index = global.sprChasmBullet;
			mask_index = mskBullet1;
			hitid = [global.sprChasmBullet, "CHASM BULLET"];
			damage = 10+skill_get(17)*3
			force = 8;
			typ = 1;
			alarm1 = -1;
			alarm2 = -1;
			alarm3 = -1;
			
			active=0
			image_speed=1
			
			on_step = script_ref_create(chasm_step);
			on_anim = script_ref_create(chasm_anim);
			on_destroy = script_ref_create(chasm_destroy);
		}
		break;
	case "CrystalBullet":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "CrystalBullet";
			sprite_index = global.sprCrystalBullet;
			mask_index = mskBullet2;
			hitid = [global.sprCrystalBullet, "CRYSTAL BULLET"];
			damage = 6+skill_get(16);
			force = 12;
			typ = 1;
			image_speed = 0.4;
			
			on_step = script_ref_create(crysb_step);
			on_wall = script_ref_create(crysb_wall);
			on_hit = script_ref_create(crysb_hit);
		}
		break;
	case "MiniPlasmaImpact":
		e=(instance_create(argument1,argument2,PlasmaImpact))
		with(e)
		{
			sprite_index = global.sprMiniPlasmaImpact;
			mask_index = global.mskMiniPlasmaImpact;
			damage = 3;
		}
		break;
	case "AntiFlakBall":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "AntiFlakBall"
			sprite_index = global.sprAntiFlakBullet
			mask_index = mskFlakBullet
			hitid = [global.sprAntiFlakBullet, "ANTI FLAK BALL"];
			damage = 8
			bonusdamage = 2
			force = 6
			
			friction = 0.4
			typ = 1
			bonus = 3
			
			on_wall = script_ref_create(aflak_wall);
			on_step = script_ref_create(aflak_step);
			on_hit = script_ref_create(aflak_hit);
			on_destroy = script_ref_create(aflak_destroy);
		}
		break;
	case "AntiShell":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "AntiShell"
			sprite_index = global.sprAntiBullet2
			mask_index = mskBullet2
			hitid = [global.sprAntiBullet2, "ANTI SHELL"];
			friction = 0.6
			damage = 2
			bonusdamage = 1
			force = 3
			lasthit = noone
			
			wallbounce = 0
			if skill_get(15) = 1
			wallbounce = 7
			
			typ = 1
			bonus = 3
			bonusdamage = 1
			
			max_speed = 15
			
			on_wall = script_ref_create(ashell_wall);
			on_step = script_ref_create(ashell_step);
			on_hit = script_ref_create(ashell_hit);
			on_anim = script_ref_create(ashell_anim);
		}
		break;
	case "UltraSlug":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "UltraSlug"
			sprite_index = global.sprUltraSlug
			mask_index = mskBullet2
			force = 9
			friction = 0.55
			wallbounce = 0
			if skill_get(15) = 1
			wallbounce = 5
			typ = 1
			bonus = 3
			maxdmg=70
			dmg=maxdmg
			max_speed = 18
			
			on_wall = script_ref_create(ashell_wall);
			on_step = script_ref_create(uslug_step);
			on_hit = script_ref_create(uslug_hit);
			on_anim = script_ref_create(uslug_anim);
		}
		break;
	case "HyperShell":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "HyperShell"
			sprite_index = sprBullet2
			mask_index = mskBullet2
			hitid = [sprBullet2, "HYPER SHELL"];
			friction = 0.6
			damage = 2
			bonusdamage = 1
			force = 3
			dist = 23+floor(random(5))
			lasthit = noone;
			alarm0 = 1;
			
			wallbounce = 0
			if skill_get(15) = 1
			wallbounce = 5
			
			typ = 1
			bonus = 3
			bonusdamage = 1
			
			max_speed = 18
			
			on_wall = script_ref_create(ashell_wall);
			on_step = script_ref_create(hshell_step);
			on_hit = script_ref_create(hshell_hit);
			on_anim = script_ref_create(hshell_anim);
		}
		break;
	case "Ipecac":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "Ipecac"
			sprite_index = global.sprIpecacBall
			mask_index = mskPlasma
			hitid = [global.sprIpecacBall, "IPECAC"];
			
			typ = 1
			z = 0
			damage = 11
			force = 6
			depth = -11
			
			on_anim = script_ref_create(ipecac_anim)
			on_step = script_ref_create(ipecac_step)
			on_wall = script_ref_create(ipecac_wall)
			on_destroy = script_ref_create(ipecac_destroy)
			on_draw = script_ref_create(ipecac_draw)
		}
		break;
	case "ThrowingKnife":
		e=(instance_create(argument1, argument2,CustomProjectile));
		with(e)
		{
			name = "ThrowingKnife";
			sprite_index = sprShank;
			mask_index = mskBullet1;
			hitid = [sprShank, "KNIFE"];
			image_speed = 0;
			image_index = 0;
			damage = 25;
			force = 6;
			typ = 1;
			friction = -0.8;
			lasthit = -1;
			alarm0 = -1;
			if instance_exists(GameCont) && GameCont.area = 0 alarm0 = 30;
			hit[0] = noone;
			
			on_step = script_ref_create(knife_step);
			on_end_step = script_ref_create(knife_end_step);
			on_hit = script_ref_create(knife_hit);
			on_destroy = script_ref_create(knife_destroy);
		}
		break;
	case "ImpactSlash":
		e=(instance_create(argument1, argument2, CustomSlash));
		with(e)
		{
			name = "ImpactSlash";
			sprite_index = global.sprImpactSlash;
			mask_index = mskSlash;
			image_speed = 0.4;
			damage = 12;
			force = 8;
			friction = 0.2;
			impacting = 0;
			
			on_step = script_ref_create(impact_step);
			on_hit = script_ref_create(impact_hit);
			on_wall = script_ref_create(impact_wall);
			on_anim = script_ref_create(impact_anim);
		}
		break;
	case "LongBullet":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "LongBullet";
			sprite_index = sprBullet1;
			mask_index = global.mskLongBullet;
			hitid = [sprBullet1, "LONG BULLET"];
			damage = 3+skill_get(16);
			force = 2;
			typ = 2;
			image_speed = 0;
			image_index = 1;
			growing = 8;
			
			on_step = script_ref_create(longb_step);
			on_wall = script_ref_create(longb_wall);
			on_hit = script_ref_create(longb_hit);
			on_draw = script_ref_create(longb_draw);
		}
		break;
	case "NewFreak":
		e=(instance_create(argument1, argument2, CustomEnemy)) 
		with(e){
			name = "NewFreak";
			spr_idle = sprPopoFreakIdle;
			spr_walk = sprPopoFreakWalk;
			spr_hurt = sprPopoFreakHurt;
			spr_dead = sprPopoFreakDead;
			spr_weap = sprPopoFreakGun;
			sprite_index = spr_idle;
			image_speed = 0.4;
			depth = -2;
			spr_shadow = shd32;
			spr_shadow_x = 2;
			spr_shadow_y = 6;
			snd_hurt = sndFreakPopoHurt;
			snd_dead = sndFreakPopoDead;
			hitid = [spr_idle, "IDPD FREAK"];
			
			sound_play(sndFreakPopoEnter);
			
			my_health = 30;
			maxhealth = my_health;
			raddrop = 18;
			team = 3;
			size = 2;
			gunangle = random(360);
			target = -1;
			walk = 0;
			right = choose(1, -1);
			direction = random(360);
			friction = 0.4;
			
			alarm[0] = 60 + irandom(30);
			
			on_step = script_ref_create(freak_step);
			on_draw = script_ref_create(freak_draw);
			on_death = script_ref_create(freak_dead);
		}
		break;
		
	case "Decal":
		e=(instance_create(argument1, argument2, CustomObject));
		with(e) {
			name = "Decal";
			
			
			image_speed = 0;
			depth = -8;
			
			on_step = script_ref_create(decal_step);
		}
		break;
	
	case "Log":
		e=(instance_create(argument1, argument2, CustomHitme));
		with(e) {
			name = "Log";
			spr_idle = global.sprLog;
			spr_hurt = global.sprLogHurt;
			spr_dead = mskNone;
			spr_shadow = shd24;
			mask_index = global.sprLog;
			
			snd_hurt = sndHitPlant;
			maxhealth = 10;
			my_health = maxhealth;
			image_speed = 0.4
			
			team = 2;
			
			on_step = script_ref_create(log_step);
			on_hurt = script_ref_create(log_hurt);
			on_destroy = script_ref_create(log_dead);
		}
		break;
	
	case "MiniPortal":
		e=(instance_create(argument1, argument2, CustomObject));
		with(e) {
			name = "MiniPortal";
			sprite_index = global.mskMiniPortal;
			mask_index = global.mskMiniPortal;
			image_speed = 0.4;
			partner = noone;
			
			frequency = 0;
			for (i=0; i<maxp; i++) {
				can_tp[i] = 1;
			}
			
			on_step = miniportal_step;
			on_draw = miniportal_draw;
		}
		break;
	
	case "GuardianRaid":
		e=(instance_create(argument1, argument2, CustomObject));
		with(e) {
			name = "GuardianRaid";
			//visible = 0;
			img_spd = 0.4
			img_index = 0
			
			on_step = guardianraid_step;
			on_draw = guardianraid_draw;
		}
		break;
	
	case "JusticeRocket":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "JusticeRocket";
			sprite_index = global.sprJusticeRocket;
			mask_index = global.sprJusticeRocket;
			hitid = [global.sprJusticeRocket, "JUSTICE"];
			damage = 10;
			force = 5;
			typ = 2;
			image_speed = 0;
			sound_play(sndRocketFly);
			
			alarm1 = 5
			alarm2 = 8
			active = 0

			on_step = script_ref_create(justice_step);
			on_destroy = script_ref_create(justice_destroy);
		}
		break;
	case "Dynamite":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "Dynamite";
			sprite_index = global.sprDynamite;
			mask_index = sprGrenade;
			hitid = [global.sprDynamite, "DYNAMITE"];
			damage = 10;
			force = 10;
			typ = 1;
			image_speed = 0;
			friction = 0.1;
			
			alarm0 = 60
			alarm1 = 6

			on_step = script_ref_create(dynamite_step);
			on_destroy = script_ref_create(dynamite_destroy);
			on_wall = script_ref_create(dynamite_wall);
			on_hit = script_ref_create(dynamite_hit);
		}
		break;
	case "Drone":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "Drone";
			sprite_index = global.sprDroneBody;
			mask_index = mskNone;
			//mask_index = mskPlasma;	does not go through walls -____-
			hitid = [global.sprDroneBody, "DRONE"];
			image_speed = 0;
			damage = 0;
			force = 0;
			typ = 2;
			depth = -12;
			
			blade1 = random(360)
			blade2 = random(360)
			blade3 = random(360)
			blade4 = random(360)
			bladerot = 0
			bladespd = 0
			bladego = 2
			al0 = 160
			al1 = 50
			al2 = 30+ceil(random(10))
			
			ultra = 0;
			
			z=0
			canmove = 0
			max_speed = 2;
			
			on_step = script_ref_create(dronel_step);
			on_hit = script_ref_create(dronel_hit);
			on_wall = script_ref_create(dronel_wall);
			on_draw = script_ref_create(dronel_draw);
			on_destroy = script_ref_create(dronel_destroy);
			}
		break;
	case "Dronade":
		e=(instance_create(argument1,argument2,CustomProjectile))
		with(e)
		{
			name = "Dronade";
			sprite_index = sprNuke;
			mask_index = mskPlasma;
			hitid = [global.sprDroneBody, "DRONE"];
			image_speed = 0;
			creator = other.creator;
			team = other.team;
			depth = -11;
			
			typ = 0
			zgrav = 1
			zspd = 0
			
			on_step = script_ref_create(drnade_step);
			on_hit = script_ref_create(drnade_hit);
			on_wall = script_ref_create(drnade_wall);
			on_draw = script_ref_create(drnade_draw);
			on_destroy = script_ref_create(drnade_destroy);
		}
		break;
	case "GuardianShock":
		e=(instance_create(argument1,argument2,CustomObject))
		with(e)
		{
			name = "GuardianShock";
			sprite_index = global.sprGuardianShock;
			image_speed = 0.4;
			timer = 12;
			
			on_step = script_ref_create(gshock_step);
		}
		break;
}
return e;

#define freak_step
enemyAlarms(1);
enemySprites();
if(alarm[0] = 0) {
	freak_alarm();
}

if(walk > 0) {
	motion_add(direction, 0.8);
	walk--;
}

if(speed > 4)
speed = 4;

#define freak_alarm
alarm[0] = 30 + irandom(10);
target = instance_nearest(x, y, Player);

if(target_is_visible()) {
	if(target_in_distance(0, 160) and random(2) < 1) {
		gunangle = point_direction(x, y, target.x, target.y) + random_range(20, -20);
		with(instance_create(x, y, LastBall)) {
			motion_add(other.gunangle, 6);
			image_angle = direction;
			creator = other.id;
			team = other.team;
			hitid = other.hitid;
		}
		wkick += 8;
		alarm[0] = 60 + irandom(40);
	}
	else
	{
		alarm[0] = 30 + irandom(20);
		direction = point_direction(x,y,target.x,target.y) + random_range(40, -40);
		gunangle = direction + random_range(20, -20);
		walk = 20 + irandom(10);
	}
	
	if target.x < x
	right = -1;
	else if target.x > x
	right = 1;
}
else
{
	direction = random(360);
	gunangle = direction + random_range(20, -20);
	walk = 30;
	
	if hspeed > 0
	right = 1;
	else if hspeed < 0
	right = -1;
}

#define freak_draw
if(gunangle <= 180){ draw_sprite_ext(spr_weap, 0, x + lengthdir_x(-wkick,gunangle), y + lengthdir_y(-wkick,gunangle), image_xscale, right * image_yscale, gunangle, c_white, 1); }

draw_sprite_ext(sprite_index, image_index, x, y, right*image_xscale, image_yscale, image_angle, image_blend, image_alpha);

if(gunangle > 180){ draw_sprite_ext(spr_weap, 0, x + lengthdir_x(-wkick,gunangle), y + lengthdir_y(-wkick,gunangle), image_xscale, right * image_yscale, gunangle, image_blend, image_alpha); }

#define freak_dead
repeat(irandom_range(1, 3))
with(instance_create(x, y, PopoNade)) { team = other.team; creator = other.id; hitid = other.hitid; }

#define enemyAlarms(maxalarms)
for(i = 0; i < maxalarms; i++) {
	if(alarm[i] > -1) {
		alarm[i]--;
	}
}

#define enemySprites
if(nexthurt > current_frame)
{
	if(sprite_index != spr_hurt) {
		image_index = 0;
		sprite_index = spr_hurt;
	}
}
else
{
	if(speed != 0 and sprite_index = spr_idle)
	sprite_index = spr_walk;
	else if(speed = 0)
	sprite_index = spr_idle;
}

#define target_in_distance
if(target != -1 and instance_exists(target) and point_distance(x, y, target.x, target.y) > argument0 and point_distance(x, y, target.x, target.y) < argument1)
{
	return 1;
}
else
{
	return 0;
}

#define target_is_visible
if(target != -1 and instance_exists(target) and !collision_line(x, y, target.x, target.y, Wall, false, false))
{
	return 1;
}
else
{
	return 0;
}

#define missil_step
var target;
target=instance_nearest(x,y,enemy)
if target!=noone {
    if !collision_line(x,y,target.x,target.y,Wall,1,1) {
    var targetdir, targetdist, changedir;
    targetdir=point_direction(x,y,target.x,target.y)
    targetdist=max(6,point_distance(x,y,target.x,target.y))+(0.04*power(abs(angle_difference(direction,targetdir)),1.2))
    targetdist/=2
    changedir=clamp(angle_difference(direction,targetdir),-360/targetdist,360/targetdist)
    direction-=changedir;
}}
direction+=4-random(8)
image_angle=direction

if random(5) < 1 && visible=1 
instance_create(xprevious,yprevious,Dust)

if hp<=0 {instance_destroy(); exit}

//alarms
alarm0-=1
if alarm0=0 {bounce=0}

//collisions
if place_meeting(x,y,Explosion) {hp-=1}

#define missil_wall
instance_create(x,y,Dust)
if bounce = 1 {
    move_bounce_solid(1)
    bounce = 0
}
else {instance_destroy()}

#define missil_draw
draw_sprite_ext(sprScrapBossMissileTrail,-1,x,y,1,1,direction,c_white,1)
draw_self()

#define missil_destroy
sound_play(sndExplosionS)
instance_create(x-1+random(2),y-1+random(2),SmallExplosion)

#define infilt_step

//collisions
if place_meeting(x,y,Explosion) {instance_destroy(); exit}
with(instances_matching(projectile,"object_index",Slash,GuitarSlash,BloodSlash,EnergySlash,EnergyHammerSlash,LightningSlash,EnemySlash)) {
	if place_meeting(x,y,other)
	with other
	{
		direction = other.direction
		speed = 12
		friction = 0.1
		alarm1 = 6
		sleep(40)
		view_shake_at(x, y, 3);
		with instance_create(x,y,Deflect) {
			image_angle = other.direction
		}
	}
}
with(instances_matching(projectile,"object_index",Shank,EnergyShank)) {
	with other
	{
		sleep(40)
		view_shake_at(x, y, 3);
		instance_destroy()
	}
}

//alarms
alarm1-=1
if alarm1=0 {
	friction = 0.4
	
	if visible = 1 repeat(4)
	{
		with instance_create(x,y,Smoke)
		motion_add(random(360),random(2))
	}
}
alarm0-=1
if alarm0<=0 instance_destroy()

#define infilt_hit
if(projectile_canhit(other) and speed > 2)
{
	projectile_hit(other, max(1,round(speed/4)), 6, direction);
}

#define infilt_wall
sound_play_hit(sndGrenadeHitWall,0.1)
move_bounce_solid(true)
speed *= 0.6
instance_create(x,y,Dust)

#define infilt_destroy
sound_play(sndExplosionL)
sound_play_hit(global.sndMissileExplode,0.1)
ang = random(360)
instance_create(x,y,Explosion)
instance_create(x+lengthdir_x(16,ang),y+lengthdir_y(16,ang),SmallExplosion)
instance_create(x+lengthdir_x(16,ang+120),y+lengthdir_y(16,ang+120),SmallExplosion)
instance_create(x+lengthdir_x(16,ang+240),y+lengthdir_y(16,ang+240),SmallExplosion)

repeat(6) with obj_create("SeekerMissile",x,y)
{
hp=15
creator = other.creator
motion_add(random(360),3.5+random(1.5))
image_angle = direction
team = other.team
}

#define hyperb_destroy
if sprite_index = sprBullet1 {
	instance_create(x,y,BulletHit);
	instance_create(x,y,Dust);
}

#define hyperb_step
alarm0-=1
alarm1-=1

if alarm0=0 
{
	move_contact_solid(direction,16)

	if sprite_index = sprBullet1 with instance_create(x,y,Dust)
	motion_add(random(360),random(2))

	dir = 0
	do {dir += 1 x += lengthdir_x(4,direction); y += lengthdir_y(4,direction);
	//if random(6) < 1 instance_create(x,y,Dust)
	}
	until dir > 100 or place_meeting(x,y,Wall) or place_meeting(x,y,enemy) or place_meeting(x,y,prop)
	alarm1 = 2
	if place_meeting(x,y,Wall) {move_outside_solid(direction+180,8); x -= lengthdir_x(1,direction); y -= lengthdir_y(1,direction); speed=0}
	else speed = 4;
	
	if sprite_index = sprBullet1 with obj_create("BulletTrail",xstart,ystart) {
		image_xscale = (point_distance(x,y,other.x,other.y))/2
		image_angle = (point_direction(x,y,other.x,other.y))
	}
}
if alarm1=0
{
	instance_destroy();
}

#define hyperb_hit
if (projectile_canhit(other) && other.my_health > 0) {
	projectile_hit(other, damage, force, direction);
	if (skill_get(16) && random(3) < 2) with (creator) if (ammo[1] < typ_amax[1]) {
		ammo[1] = min(ammo[1] + other.regen, typ_amax[1]);
		with (other) {
			sound_play(sndRecGlandProc);
			instance_create(x, y, RecycleGland);
		}
	}
	instance_destroy();
}

#define hyperb_wall
//nothing

#define hyperb_anim
image_speed = 0
image_index = 1

#define firework_step

if friction > 0.15
{
with instance_create(x,y,Flame)
{
visible=other.visible
motion_add(other.direction+random(20)-10,random(1))
team = other.team
creator = other.creator
sprite_index = other.flame_color}
}

//collisions
with(instances_matching(projectile,"object_index",Slash,GuitarSlash,BloodSlash,EnergySlash,EnergyHammerSlash,LightningSlash,EnemySlash)) {
	if place_meeting(x,y,other)
	with other
	{
		direction = other.direction
		speed = 12
		friction = 0.1
		alarm1 = 6
		sleep(40)
		view_shake_at(x, y, 3);
		with instance_create(x,y,Deflect) {
			image_angle = other.direction
		}
	}
}
with(instances_matching(projectile,"object_index",Shank,EnergyShank)) {
	with other
	{
		sleep(40)
		view_shake_at(x, y, 3);
		instance_destroy()
	}
}

//alarms
alarm1-=1
if alarm1=0 {
	friction = 0.3
}
alarm0-=1
if alarm0<=0 instance_destroy()

#define firework_wall
instance_destroy()

#define firework_destroy
sleep(15)
sound_play(sndFlareExplode)

if star=1 {
    ang=random(360)
    len=8
    lendir=1
    repeat(30) {
        with instance_create(x,y,Flame)
        {motion_add(other.ang,other.len)
        sprite_index=other.flame_color
        team = other.team
		creator = other.creator}
    ang+=360/30
    if len>=8 {lendir=-1}
    if len<=5 {lendir=1}
    len+=lendir
    }
}
else {
repeat(20)
{
with instance_create(x,y,Flame)
{motion_add(random(360),random(1)+4)
team = other.team
sprite_index=other.flame_color
creator = other.creator}}
}

#define star_step
timer+=1
if timer<=15
{
	speed+=.75
	instance_create(x,y,Smoke)
}
else
{
speed=max(speed-.75,0)
instance_create(x,y,Dust)
}
angswitch*=-1;
if timer>=30 instance_destroy()

#define star_draw
draw_sprite_ext(sprite_index,-1,x,y,1,1,image_angle+angswitch*speed*(2/3),c_white,1)
//draw sprScrapBossMissileTrail?

#define star_destroy
instance_create(x,y,Explosion);
ang = random(360);
col = 0;
sound_play(sndExplosionL)
repeat(5) {
	with(obj_create("Firework",x,y))
	{
		creator = other.creator
		motion_add(other.ang, 7);
		image_angle = direction
		team = other.team
		
		star = 1
		color = other.col
		switch (color) {
			case 0:
			sprite_index = sprFlare
			flame_color = sprTrapFire
			break;
			case 1:
			sprite_index = global.sprFlareYellow
			flame_color = global.sprFireYellow
			break;
			case 2:
			sprite_index = global.sprFlareGreen
			flame_color = global.sprFireGreen
			break;
			case 3:
			sprite_index = global.sprFlareBlue
			flame_color = sprFireLilHunter
			break;
			case 4:
			sprite_index = global.sprFlarePink
			flame_color = global.sprFirePink
			break;
		}
	}
	ang+=(360/5);
	col+=1;
}

#define lsentry_destroy
with instance_create(x,y,BulletHit) {sprite_index=global.sprLaserSentryDead}

#define lsentry_step
if my_health <= 0 {
		with obj_create(x,y,"MiniPlasmaImpact") {
			team = other.team;
			creator = other.creator;
		}
		instance_destroy()
		exit;
	}
if ammo <= 0 {instance_destroy(); exit;}

if sprite_index != spr_hurt
sprite_index = spr_idle

if sprite_index = spr_hurt
{if image_index > 2
sprite_index = spr_idle}

//collisions
/*with(instances_matching(CustomHitme,"name","LaserSentry")) {
	if place_meeting(x,y,other) {
		if speed > 0
		instance_create(x,y,Dust)
		speed = 0
		move_contact_all(direction,12)
	}
}*/
with Wall {
	if place_meeting(x,y,other) {
		if other.speed > 0
		{
			with other 
			{
				x -= lengthdir_x(speed,direction)
				y -= lengthdir_y(speed,direction)
				sound_play_hit(sndGrenadeStickWall,0.1)
				instance_create(x,y,Dust)
			}
		}
		with other {
			speed = 0
			move_contact_solid(direction,12)
		}
	}
}

//active
if active<=0 {
	var nulaser, i;
        for (i=1; i<=3; i+=1) {
			retarget = 0;
			if tar[i-1] = noone || !instance_exists(tar[i-1]) || collision_line(x,y,tar[i-1].x,tar[i-1].y,Wall,0,true) > 0 || point_distance(x,y,tar[i-1].x,tar[i-1].y) >= 160
            {
				if instance_number(enemy) >= i
                tar[i-1] = instance_nth_nearest(x,y,enemy,i)
				else
				tar[i-1] = noone;
            }
			else {
                ammo -= 1
                nulaser=obj_create("SentryLaser",x,y)
                nulaser.team = team
                nulaser.x2=tar[i-1].x
                nulaser.y2=tar[i-1].y
                nulaser.draw_yscale+=ammo/300
                with nulaser {
                    image_angle=point_direction(x,y,x2,y2)
                    image_xscale=point_distance(x,y,x2,y2)/2
                }
            }
        }
}
else active-=1
if death>0 {death-=1;} else {instance_destroy();}

#define lsentry_hurt(damage, kb_vel, kb_dir)
my_health -= damage;
motion_add(kb_dir,kb_vel/3);
sprite_index=spr_hurt;
image_index=0;
sound_play_hit(snd_hurt,0.1)

#define senlaser_hit
if projectile_canhit(other)
{
	if other.sprite_index != other.spr_hurt || (other.sprite_index=other.spr_hurt && other.image_index=0)
	{
		projectile_hit(other, 1, 0, direction);
		sound_play_hit(sndLightningReload,0.1)
	}
	other.speed*=(2/3)
	if random(6)<1 instance_create(other.x,other.y,Smoke)
}

#define senlaser_draw
draw_sprite_ext(sprite_index,-1,x,y,image_xscale,draw_yscale,image_angle,image_blend,image_alpha)

#define senlaser_step
instance_destroy();

#define drill_step
if active = 1
{
if visible=1 && choose(1,2) = 1 instance_create(x-hspeed,y-vspeed,Dust)
if speed < 12
speed += 1.2-min(0.7,array_length_1d(stuck)/5)
sound_play_hit(sndJackHammer,0.1)
}

for (i=0; i<array_length_1d(stuck); i+=1) {
if instance_exists(stuck[i]) {
    if !object_is_ancestor(stuck[i].object_index,prop)
        {
        stuck[i].x=(x-speed/4+random(speed/2))
        stuck[i].y=(y-speed/4+random(speed/2))
        }
    }
}
//alarms
alarm1-=1
if alarm1=0 active=1;

#define drill_hit
if projectile_canhit(other) 
{
	have=0
	for (i=0; i<array_length_1d(stuck); i+=1) {
		if other.id = stuck[i] have=1
	}
	if have=0 {
		if other.object_index = Nothing || other.object_index=Nothing2 {
			projectile_hit(other, 25, force, direction)
			instance_destroy()
		}
		else {
			projectile_hit(other, ceil(speed/3), force, direction)
			speed*=0.75
			speed=max(2,speed)
			stuck[array_length_1d(stuck)] = other.id
		}
	}
}

#define drill_draw
draw_sprite_ext(sprScrapBossMissileTrail,-1,x,y,1,1,direction,c_white,1)
draw_self()

#define drill_destroy
//sound_stop(sndRocketFly)

sound_play(sndExplosionL)
instance_create(x,y,Explosion)
ang = random(360)
instance_create(x+lengthdir_x(12,ang),y+lengthdir_y(12,ang),SmallExplosion)
instance_create(x+lengthdir_x(12,ang+120),y+lengthdir_y(12,ang+120),SmallExplosion)
instance_create(x+lengthdir_x(12,ang+240),y+lengthdir_y(12,ang+240),SmallExplosion)

#define zap_expand(expandtype)
if instance_exists(enemy)
dir = instance_nearest(x+lengthdir_x(80,direction),y+lengthdir_y(80,direction),enemy)
var oldx, oldy;
oldx = x
oldy = y
direction = image_angle+(random(30)-15)
speed = 4
if instance_exists(enemy)
{
	if point_distance(x,y,dir.x,dir.y) < 120
	motion_add(point_direction(x,y,dir.x,dir.y),1)
}
image_angle = direction
speed = 0

//move_contact_solid(direction,8+random(4))
var _dis = 8+random(4);
x += lengthdir_x(_dis,direction)
y += lengthdir_y(_dis,direction)

speed = 0
image_xscale = -point_distance(x,y,oldx,oldy)/2

ammo -= 1

if place_meeting(x,y,Wall)
{
	x = xprevious
	y = yprevious
	direction += 140+random(80);
	//direction += 180; less random
}

if ammo <= 0
{
	instance_create(x+lengthdir_x(image_xscale/2,image_angle),y+lengthdir_y(image_xscale/2,image_angle),LightningHit)
}
else
{
	image_index += 0.4/ammo
	with obj_create("ZapLightning",x,y)
	{
		direction = other.direction
		image_angle = direction
		ammo = other.ammo
		team = other.team
		if expandtype=1 {zap_expand(2)}
		else {alarm1 = 1}
	}
}

#define zap_step
alarm1-=1
if alarm1=0 zap_expand(1)

#define zap_draw
image_yscale = 1+random(1.5)
draw_sprite_ext(sprite_index,-1,x,y,image_xscale,image_yscale/2,image_angle,c_white,1)

#define zap_hit
if projectile_canhit(other) {
	if projectile_canhit_melee(other) {
		projectile_hit(other,damage,force,image_angle)
	}
	if other.my_health<=0 && random(250)<1 sound_play(global.sndDucked);
	sound_play_hit(other.snd_hurt,0.1)
	if random(4)<1 instance_create(x,y,Smoke)
}

#define zap_wall
//nothing

#define zap_anim
instance_destroy()

#define btrail_step
if image_index>=1 {image_index=1; image_speed=0;}
image_alpha/=2;
if image_alpha<0.2 instance_destroy();

#define flaser_extend
var dir, newx, newy;
dir = 0
if image_yscale>0 do {x -= lengthdir_x(2,image_angle); y -= lengthdir_y(2,image_angle); dir += 1}
until ((place_meeting(x,y,hitme) and dir > 16) or place_meeting(x,y,Wall) or dir > 160)

alarm0 = 2

image_xscale = image_xscale+point_distance(x,y,xstart,ystart)/2
x=xstart
y=ystart

instance_create(x-lengthdir_x(image_xscale*2,image_angle),y-lengthdir_y(image_xscale*2,image_angle),Smoke)

#define flaser_step
image_yscale -= 0.25;
if image_yscale < 0 {
instance_destroy();
exit;
}

img += 0.5;

//alarms
alarm0 -= 1
if alarm0 = 0 {
	flaser_extend()
}

#define flaser_hit
if projectile_canhit(other) {
	with instance_create(other.x,other.y,Flame) {
		motion_add(other.image_angle+180,2);
		team = other.team;
		creator = other.id;
		//sound_play_hit(sndBurn,0.1); too loud
	}
	projectile_hit(other,damage,force,image_angle+180)
	instance_create(other.x,other.y,Smoke)
}

#define flaser_draw
draw_sprite_ext(sprite_index,-1,x,y,image_xscale,image_yscale,image_angle,c_white,1)
draw_sprite_ext(global.sprFireLaserStart,img,xstart,ystart,1,1,image_angle+180,c_white,1)
draw_sprite_ext(global.sprFireLaserEnd,img,x-lengthdir_x(image_xscale*2,image_angle),y-lengthdir_y(image_xscale*2,image_angle),1,1,image_angle+180,c_white,1)

#define flaser_wall
//ugh

#define chasm_step
alarm1 -= 1
alarm2 -= 1
alarm3 -= 1

if alarm1 = 0 {
	direction -= 45
	image_angle=direction
}
if alarm2 = 0 {
	direction += 45
	image_angle=direction
}
if alarm3 = 0 speed = 11

#define chasm_anim
image_speed = 0;
image_index = 1;

#define chasm_destroy
with obj_create("MiniPlasmaImpact",x,y) {
	team = other.team;
	creator = other.creator;
}
sound_play_hit(sndPlasmaHit,0.1);

#define aflak_wall
sound_play_hit(sndHitWall,0.1);
instance_destroy();

#define aflak_step
if random(3) < 1
{
	with instance_create(x,y,Dust)
	motion_add(random(360),random(2))
}

image_speed = speed/12

if bonus > 0 bonus -= 1
if bonus < 0 bonus = 0

if speed < .5
instance_destroy()

#define aflak_hit
if projectile_canhit(other)
{
x=other.x
y=other.y
projectile_hit(other,damage+sign(bonus)*bonusdamage,force,direction)
sleep(50)
instance_destroy()
}

#define aflak_destroy
sound_play_hit(sndFlakExplode,0.1)
var ang;
repeat(16)
{
	ang = random(360);
	with obj_create("AntiShell",x+lengthdir_x(26,ang),y+lengthdir_y(26,ang))
	{
		motion_add(point_direction(x,y,other.x,other.y)-3+random(6),9+random(3))
		image_angle = direction
		team = other.team
		move_outside_solid(direction,18)
	}
}
sleep(20)

//view_shake_at(x,y,8)

with instance_create(x,y,BulletHit) {
    sprite_index = global.sprAntiFlakHit
}

#define ashell_step
image_angle = direction

if speed < 6 and sprite_index != global.sprAntiBullet2Hit {
	sprite_index = global.sprAntiBullet2Hit
	image_index = 0
	image_speed = 0.4
}

if bonus>0 bonus -= 1
if bonus<0 bonus = 0

#define ashell_wall
if speed > 6
sound_play_hit(sndShotgunHitWall,0.1)
move_bounce_solid(false)
speed *= 0.8
if speed+wallbounce > max_speed
speed = max_speed
else
speed += wallbounce

wallbounce *= 0.95
instance_create(x,y,Dust)

if wallbounce > 0 bonus = 3

lasthit = noone;

#define ashell_hit
if projectile_canhit(other) && other.my_health>0 && other.id!=lasthit
{
	lasthit = other.id
	projectile_hit(other,damage+sign(bonus)*bonusdamage,force,direction)
	//with instance_create(x,y,BulletHit)
	//sprite_index = global.sprAntiBullet2Hit
	move_bounce_all(true)
	direction+=2-random(4)
	speed *= 0.8
	if speed+wallbounce > max_speed
	speed = max_speed
	else
	speed += wallbounce

	wallbounce *= 0.95
	instance_create(x,y,Dust)

	if wallbounce > 0 bonus = 3
	
}

#define ashell_anim
image_speed = 0
image_index = 1
if sprite_index = global.sprAntiBullet2Hit
instance_destroy()

#define hshell_step
image_angle = direction

if speed < 6 and sprite_index != sprBullet2Disappear {
	sprite_index = sprBullet2Disappear
	image_index = 0
	image_speed = 0.4
}

if bonus>0 bonus -= 1
if bonus<0 bonus = 0

//alarmshit
alarm0-=1

if alarm0=0 
{
	move_contact_solid(direction,8)

	with instance_create(x,y,Smoke)
	motion_add(random(360),random(2))

	dir = 0
	do {dir += 1 x += lengthdir_x(4,direction); y += lengthdir_y(4,direction);
	if random(14) < 1 instance_create(x,y,Dust)
	}
	until dir > dist or place_meeting(x,y,Wall) or place_meeting(x,y,enemy) or place_meeting(x,y,prop)
	
	speed=10+random(3)
	if place_meeting(x,y,Wall) {move_outside_solid(direction+180,8)}
	
	xprevious=x; yprevious=y
	/*with obj_create("BulletTrail",xstart,ystart) {
		image_xscale = (point_distance(x,y,other.x,other.y))/2
		image_angle = (point_direction(x,y,other.x,other.y))
	}*/
}

#define hshell_hit
if projectile_canhit(other) && other.my_health>0
{
	projectile_hit(other,damage+sign(bonus)*bonusdamage,force,direction)
	
	with instance_create(x,y,BulletHit)
	sprite_index = sprBullet2Disappear
	instance_destroy();
}

#define hshell_anim
image_speed = 0
image_index = 1
if sprite_index = sprBullet2Disappear
instance_destroy()

#define ipecac_anim
image_speed = 0
image_index = 1

#define ipecac_destroy
sound_play(sndExplosion)
instance_create(x+random(2)-1,y+random(2)-1,GreenExplosion)

#define ipecac_step
zspd-=zgrav
z+=zspd
if z<0 {
if bounces<=0 {instance_destroy(); exit;}
else {team=-1; bounces-=1; z=0; zspd = zspdinit; sound_play_hit(sndBouncerBounce,0.1)}
}
if position_meeting(x, y, TopSmall) {
	instance_destroy();
	exit;
}
if mask_index = mskNone {
	mask_index = mskPlasma
	if place_meeting(x,y,Wall) mask_index = mskNone
}

#define ipecac_wall
mask_index = mskNone;

#define ipecac_draw
draw_sprite(sprite_index,image_index,x,y-z)

#define knife_step
if instance_exists(enemy) and instance_exists(creator)
{
	if speed > 0 and creator.object_index = Player and creator.race = "assassin" and skill_get(mut_throne_butt) 
	{
		var hitcheck, _i;
		hitcheck = 0
		dir = instance_nearest(x,y,enemy);
		for (_i=0; _i<array_length_1d(hit); _i+=1) {
			if dir.id = hit[_i] hitcheck = 1
		}
		if(hitcheck = 0 and point_distance(x,y,dir.x,dir.y) < 24)
		{
			mask_index = mskBolt;
			x = dir.x-hspeed;
			y = dir.y-vspeed;
		}
	}
}
with(projectile)
{
	if(place_meeting(x, y, other) and other.team != team and (typ = 2 or typ = 1))
	{
		instance_destroy();
	}
}
//alarms
alarm0 -= 1
if alarm0 = 0 instance_destroy();

#define knife_end_step
with(instance_create(x,y,BoltTrail)) {
	creator = other.id;
	
	direction = point_direction(x,y,other.xprevious,other.yprevious)
	image_angle = direction
	image_yscale += 0.4;
	image_xscale = point_distance(x,y,other.xprevious,other.yprevious)
}

#define knife_hit
var hitcheck, _i;
hitcheck = 0
for (_i=0; _i<array_length_1d(hit); _i+=1) {
    if other.id = hit[_i] hitcheck = 1
}
if(projectile_canhit(other) and hitcheck = 0 and other.my_health > 0) {
	hit[array_length_1d(hit)]=other.id
	projectile_hit_np(other, damage, force, 50);
	lasthit = other.id;
}

#define knife_destroy
with(instance_create(x,y,WepPickup)) { wep = "throwingknife"; }

#define crysb_step
with(instances_matching_ne(projectile, "team", team)) {
	if place_meeting(x,y,other) {
		if typ = 1 {
			team = other.team
			direction = other.direction+random(16)-8
			with instance_create(x,y,Deflect)
			image_angle = other.direction
		}
		else if typ = 2 {
			instance_destroy();
		}
	}
}
			
#define crysb_wall
instance_create(x,y,Dust)
sound_play_hit(sndHitWall,0.1)
instance_destroy()

#define crysb_hit
if (projectile_canhit(other) && other.my_health > 0) {
	projectile_hit(other, damage, force, direction);
	if (skill_get(16) && random(3) < 2) with (creator) if (ammo[1] < typ_amax[1]) {
		ammo[1] = min(ammo[1] + 3, typ_amax[1]);
		with (other) {
			sound_play(sndRecGlandProc);
			instance_create(x, y, RecycleGland);
		}
	}
	instance_destroy();
}

#define impact_step
if impacting=0 impacting=1
else with(Corpse) {
	if other.size<=size && (place_meeting(x, y, other)) {
		direction = other.direction
		speed = 12 + (skill_get(mut_impact_wrists) * 4)
		
        if size > 0
        speed /= (0.5+size/2);
	}
}

#define impact_hit
if(projectile_canhit_melee(other)) {
	projectile_hit(other, damage, force);
}

#define impact_wall
x -= hspeed;
y -= vspeed;

#define impact_anim
instance_destroy();

#define longb_step
if !place_meeting(x,y,Floor) {
	image_xscale = -1;
}

if image_xscale<=0 {
	instance_create(x,y,Dust);
	instance_destroy();
	exit;
}

if growing>0 {
	image_xscale+=1
	growing-=1
	x=xstart
	y=ystart
}

#define longb_wall
image_xscale = max(sign(growing),image_xscale-1);
sound_play_hit(sndHitWall,0.1)

#define longb_hit
if projectile_canhit(other) {
	projectile_hit(other, damage, force);
	image_xscale = max(sign(growing),image_xscale-1);
	instance_create(other.x,other.y,BulletHit)
	
	if (skill_get(16) && random(3) < 2) with (creator) if (ammo[1] < typ_amax[1]) {
		ammo[1] = min(ammo[1] + 1, typ_amax[1]);
		with (other) {
			sound_play(sndRecGlandProc);
			instance_create(x, y, RecycleGland);
		}
	}
}

#define longb_draw
draw_sprite_ext(global.sprLongBullet,0,x,y,1,1,image_angle,c_white,1)
if image_xscale>1 draw_sprite_ext(global.sprLongBullet,1,x+lengthdir_x(8,image_angle),y+lengthdir_y(8,image_angle),image_xscale*2-2,1,image_angle,c_white,1)
draw_sprite_ext(global.sprLongBullet,2,x+lengthdir_x(8+16*(image_xscale-1),image_angle),y+lengthdir_y(8+16*(image_xscale-1),image_angle),1,1,image_angle,c_white,1)
if growing>0 draw_sprite_ext(sprBullet1,0,x,y,1,1,image_angle,c_white,1)

#define decal_step
if(place_meeting(x, y, FloorExplo)) {
	instance_destroy();
	exit;
}

if(place_meeting(x, y, Floor) xor place_meeting(x, y, TopPot) xor place_meeting(x, y, Bones)) {
	instance_delete(self);
}

#define vandecal_destroy
with instance_create(x, y, PopoExplosion){ team = 3; }
sound_play(sndIDPDNadeExplo);
sound_play(sndVanHurt);
instance_destroy();

#define log_dead
ang=random(360);
instance_create(x+lengthdir_x(24,ang),y+lengthdir_y(24,ang),MeatExplosion);
instance_create(x+lengthdir_x(24,ang+120),y+lengthdir_y(24,ang+120),MeatExplosion);
instance_create(x+lengthdir_x(24,ang+240),y+lengthdir_y(24,ang+240),MeatExplosion);
instance_create(x,y,MeatExplosion);

#define log_hurt(damage, kb_vel, kb_dir)
my_health -= damage;
sprite_index=spr_hurt;
image_index=0;
sound_play_hit(snd_hurt,0.1)

#define log_step
if sprite_index != spr_hurt
sprite_index = spr_idle

if sprite_index = spr_hurt
{if image_index > 2
sprite_index = spr_idle}

speed=0

if my_health<=0 instance_destroy();

#define miniportal_step
with CustomObject {
	if name="MiniPortal" && frequency = -1*other.frequency other.partner=id;
}

//nightmarish trash player teleportation
if partner!=noone {
	with Player {
		tp_check = 2;
		if other.can_tp[index]=1 && place_meeting(x,y,other) {
			with other.partner {
				other.x = x;
				other.y = y;
				sound_play(sndGuardianDisappear);
				can_tp[other.index] = 0;
			}
			other.can_tp[index] = 0;
			speed = 0;
			
			//portal shock ultra
			if ultra_get("guardian", 1) {
				obj_create("GuardianShock",x,y)
				with projectile {
					if team!=2 && point_distance(x,y,other.x,other.y)<96 && typ>0 instance_destroy()
				}
				with enemy {
					if point_distance(x,y,other.x,other.y)<96 {
						repeat(3) move_contact_solid(point_direction(other.x,other.y,x,y),16-min(16,point_distance(other.x,other.y,x,y)))
						motion_add(point_direction(other.x,other.y,x,y),16)
					}
				}
			}
			
			//raid ultra
			if ultra_get("guardian", 2) {
					with obj_create("GuardianRaid",0,0) {
						timer = 90;
						my_index = other.index;
						with instances_matching(CustomObject,"name","GuardianRaid") {
							if id!=other.id && my_index=other.my_index instance_destroy();
						}
					}
				}
		}
		else if other.can_tp[index] != 1 {
			if !place_meeting(x,y,other) tp_check-=1;
			with other.partner {
				if !place_meeting(x,y,other) {
					other.tp_check-=1;
				}
			}
			if tp_check<=0 {
				with other {
					can_tp[other.index]=1
					partner.can_tp[other.index]=1
				}
			}
		}
	}
	with projectile {
		if place_meeting(x,y,other) && team = other.team && (typ = 1 || typ = 2) && !variable_instance_exists(id,"_teleported") {
			var _canttouchthis;
			_canttouchthis = other.partner;
			sound_play_hit(sndEliteShielderFire,0.2);
			x = _canttouchthis.x;
			y = _canttouchthis.y;
			// old teleporting method.
			/*while place_meeting(x,y,_canttouchthis) {
				x += lengthdir_x(4,direction)
				y += lengthdir_y(4,direction)
			}*/
			_teleported = 1;
		}
	}
}

#define guardianraid_step
with Player {
	if index = other.my_index {
		if reload>0 reload-=1;
		if race = "steroids" && breload>0 breload-=1;
	}
}
img_index += img_spd
timer -= 1
if timer <= 0 instance_destroy();

#define guardianraid_draw
with Player {
	if index = other.my_index {
		draw_sprite(sprImpactWrists,other.img_index,x,y)
	}
}

#define miniportal_draw
draw_sprite_ext(global.sprMiniPortalOutline,image_index,x,y,image_xscale,image_yscale,image_angle,player_get_color(index),image_alpha)
draw_self()

#define justice_step
if active = 1
{
if random(3)<1 instance_create(x,y,Smoke)
if speed < 12
speed += 1
}
//alarms
alarm1-=1
if alarm1 = 0 {
	active = 1
	//BackCont.shake += 2
}
alarm2-=1
if alarm2 = 0 {
	oldspd=speed
	motion_add(goingto,12)
	speed=oldspd
	image_angle=direction
}

#define justice_destroy
sound_stop(sndRocketFly)

sound_play(sndExplosionS)
instance_create(x,y,SmallExplosion)

#define uslug_step
image_angle = direction

if speed < 6 and sprite_index != global.sprUltraSlugDisappear
{
sprite_index = global.sprUltraSlugDisappear
image_index = 0
image_speed = 0.4
}

if bonus > 0 bonus -= 1
if bonus < 0 bonus = 0

image_xscale = power(max(0,dmg/maxdmg),0.8)

if dmg<=0 {
	with instance_create(x,y,BulletHit)
	sprite_index = global.sprUltraSlugDisappear
	instance_destroy()
}

#define uslug_hit
if projectile_canhit(other) and other.my_health > 0
{
var _damage;
_damage = dmg;
if bonus>0 projectile_hit(other,floor(sqrt(dmg)),3,direction)
dmg -= max(0,min(dmg,other.my_health))
projectile_hit(other,_damage,force,direction)
x=xprevious
y=yprevious
}

#define uslug_anim
image_speed = 0
image_index = 1
if sprite_index = global.sprUltraSlugDisappear
instance_destroy()

#define dynamite_destroy
sound_play(sndExplosion)
instance_create(x,y,Explosion)
ang = random(360)
instance_create(x+lengthdir_x(16,ang),y+lengthdir_y(16,ang),SmallExplosion)
instance_create(x+lengthdir_x(16,ang+120),y+lengthdir_y(16,ang+120),SmallExplosion)
instance_create(x+lengthdir_x(16,ang+240),y+lengthdir_y(16,ang+240),SmallExplosion)

repeat(12) {
	with instance_create(x,y,Flame) {
		motion_add(random(360),random(1)+4)
		team = other.team
		creator = other.creator
	}
}

#define dynamite_step
if speed = 0 && random(2)<1 {
    with instance_create(x-lengthdir_x(4,direction),y-lengthdir_y(4,direction),Flame) {
    team = other.team
	creator = other.creator
    visible = other.visible
    }
}

//collisiionsss
if place_meeting(x,y,Explosion) {instance_destroy(); exit;}

//alarms
alarm0 -= 1
alarm1 -= 1

if alarm1 = 0 friction = 0.4;
if alarm0 = 0 instance_destroy();

#define dynamite_wall
sound_play_hit(sndShotgunHitWall,0.15)
move_bounce_solid(true)
speed *= 0.6
instance_create(x,y,Dust)

#define dynamite_hit
if projectile_canhit(other) {
	projectile_hit(other,damage,force,direction)
	if other.my_health > 0 instance_destroy();
}

#define dronel_draw
if ultra = 1 sprite_index = global.sprUltraDroneBody
draw_sprite_ext(sprite_index,1,x,y-z,1,1,image_angle,image_blend,image_alpha)
draw_sprite_ext(sprite_index,0,x,y-z-3,1,1,image_angle,image_blend,image_alpha)
var len;
len = sqrt(2*sqr(7))
draw_sprite_ext(global.sprDroneBlade,min(3,floor(bladespd/30+random(1))),x+lengthdir_x(len,direction+45),y-3+lengthdir_y(len,direction+45)-z,image_xscale,image_yscale,bladerot+blade1,image_blend,image_alpha)
draw_sprite_ext(global.sprDroneBlade,min(3,floor(bladespd/30+random(1))),x+lengthdir_x(len,direction+135),y-3+lengthdir_y(len,direction+135)-z,image_xscale,image_yscale,bladerot+blade2,image_blend,image_alpha)
draw_sprite_ext(global.sprDroneBlade,min(3,floor(bladespd/30+random(1))),x+lengthdir_x(len,direction+225),y-3+lengthdir_y(len,direction+225)-z,image_xscale,image_yscale,bladerot+blade3,image_blend,image_alpha)
draw_sprite_ext(global.sprDroneBlade,min(3,floor(bladespd/30+random(1))),x+lengthdir_x(len,direction+315),y-3+lengthdir_y(len,direction+315)-z,image_xscale,image_yscale,bladerot+blade4,image_blend,image_alpha)

#define dronel_step
bladespd += bladego

if bladespd > 100
bladespd = 100;

bladerot += bladespd
z=5*sqrt(abs(bladespd))
if random(200) < bladespd && bladego>0
with instance_create(x,y-z,Smoke) {
	depth = other.depth;
}

var tar;
if canmove!=0
{
    tar = instance_nearest(x,y,enemy)
    if tar>0
	{
		motion_add(point_direction(x,y,tar.x,tar.y),max_speed/10)
	}
}
image_angle=direction
if speed>max_speed speed=max_speed

al0-=1
al1-=1
al2-=1

//alarms
if(al0 = 0)
bladego = -1;

if(al1 = 0)
{
	if z>30 
	{
		if (position_meeting(x,y,Floor) || position_meeting(x,y,FloorExplo)) 
		{
			with (obj_create("Dronade",x,y))
			{
				motion_add(other.direction,other.speed);
				z = other.z;
				team = other.team;
				if other.ultra = 1 {
					z = 2;
					visible = 0;
				}
			}
			if ultra = 0 sound_play_hit(sndGrenade, 0.2)
			else {
				var d;
				d = z;
				repeat(floor(z/8)) {
					instance_create(x,y-d,Dust)
					d-=8
				}
				sound_play_hit(sndHyperLauncher, 0.2)
			}
		}
		al1 = 30;
		if ultra=1 al1 = 8;
	}
}

if(al2 = 0)
canmove = 1;

//destroy
if bladespd < 0
{
	bladespd = 0;
	instance_destroy();
	exit;
}

#define dronel_hit
//also nothing

#define dronel_destroy
sound_play(sndExplosionL)
ang = random(360)

if ultra = 1 {
	instance_create(x,y,GreenExplosion)
	instance_create(x+lengthdir_x(16,ang),y+lengthdir_y(16,ang),GreenExplosion)
	instance_create(x+lengthdir_x(16,ang+120),y+lengthdir_y(16,ang+120),GreenExplosion)
	instance_create(x+lengthdir_x(16,ang+240),y+lengthdir_y(16,ang+240),GreenExplosion)
	
	repeat(2) with instance_create(x,y,BigRad) {
		motion_add(random(360),3+random(20))
		repeat(speed)
		speed *= 0.9
	}
	repeat(5) with instance_create(x,y,Rad) {
		motion_add(random(360),3+random(20))
		repeat(speed)
		speed *= 0.9
	}
}
else {
	instance_create(x+lengthdir_x(16,ang),y+lengthdir_y(16,ang),Explosion)
	instance_create(x+lengthdir_x(16,ang+120),y+lengthdir_y(16,ang+120),Explosion)
	instance_create(x+lengthdir_x(16,ang+240),y+lengthdir_y(16,ang+240),Explosion)
	instance_create(x+lengthdir_x(24,ang+60),y+lengthdir_y(16,ang),SmallExplosion)
	instance_create(x+lengthdir_x(24,ang+180),y+lengthdir_y(16,ang+120),SmallExplosion)
	instance_create(x+lengthdir_x(24,ang+300),y+lengthdir_y(16,ang+240),SmallExplosion)
}

#define dronel_wall
//nothing!!!

#define drnade_step
zspd += zgrav
z -= zspd
if z<0 instance_destroy()

#define drnade_hit
if(projectile_canhit(other) && z < 7) {
	projectile_hit(other, 10, 0, direction);
	instance_destroy()
}

#define drnade_destroy
instance_create(x,y,Explosion)
sound_play(sndExplosion)

#define drnade_wall
//HYPERNOTHING

#define drnade_draw
draw_sprite_ext(sprNuke,0,x,y-z,1,1,270,image_blend,image_alpha)

#define gshock_step
timer -= 1
if timer <= 0 instance_destroy()