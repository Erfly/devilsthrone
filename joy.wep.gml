#define init
global.sprJoy = sprite_add_weapon("sprJoy.png", 0, 8);
#define weapon_name
return "Joy";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 6;
#define weapon_load
return 16;
#define weapon_sprt
return global.sprJoy;
#define weapon_area
return 0;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 22.5, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 45, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 67.5, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 90, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 112.5, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 135, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 157.5, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 180, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 202.5, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 225, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 247.5, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 270, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 292.5, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 315, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wait 1;
sound_play(sndBigBanditShootLaugh);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy + 337.5, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(0, 0, 0);