#define init
global.sprHammerheadDiscGun = sprite_add_weapon("sprites/hammerhead_discgun.png", -2, 6);
#define weapon_name
return "HAMMERHEAD DISC GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 8;
#define weapon_sprt
return global.sprHammerheadDiscGun;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "twice the fun";
#define weapon_fire
var __angle = gunangle;
var __len = 15;
sound_play(sndDiscgun);
with (instance_create(x+lengthdir_x(__len,__angle), y+lengthdir_y(__len,__angle), Disc)) {
	motion_add(__angle - 90 + (random(10) - 5) * other.accuracy, 5);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x+lengthdir_x(__len,__angle), y+lengthdir_y(__len,__angle), Disc)	) {
	motion_add(__angle + 90 + (random(10) - 5) * other.accuracy, 5);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);
