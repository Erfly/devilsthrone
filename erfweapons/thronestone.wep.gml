#define init
global.curvedpistol = sprite_add_weapon("sprites/curved_revolver.png", -2, 10);
#define weapon_name
return "THRONESTONE";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return global.curvedpistol;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "pew pew pew";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
var i;
for (i = 0; i < 3; i += 1)
with (instance_create(x, y, SentryGun)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
weapon_post(2, -6, 4);
