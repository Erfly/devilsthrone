#define init
	global.allyl1 = sprite_add_weapon("sprites/ally_launcher.png", 3, 6);

#define weapon_sprt
	return global.allyl1;

#define weapon_name
	return "ALLY LAUNCHER";

#define weapon_type
	return 5;

#define weapon_auto
	return 0;

#define weapon_cost
	return 10;

#define weapon_load
	return 30;

#define weapon_area
	return 8; // L0 4-1+

#define weapon_swap
	return sndSwapExplosive;

#define weapon_text
	return "FRIENDS TIL THE END!";

#define weapon_fire
	var __angle = gunangle;
	
	sound_play(sndPistol);
	weapon_post(2, -6, 4);
	
	//Spawn Ally
		with (instance_create(x, y, Ally)) {
			motion_add(__angle + (random(8) - 4) * other.accuracy, 20);
			image_angle = 0;
			team = other.team;
			creator = other;
		}
