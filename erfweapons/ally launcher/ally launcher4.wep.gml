#define init
	global.allyl4 = sprite_add_weapon("sprites/ally_launcher_4.png", 3, 6);

#define weapon_sprt
	return global.allyl4;

#define weapon_name
	return "QUADRUPLE ALLY LAUNCHER";

#define weapon_type
	return 5;

#define weapon_auto
	return 0;

#define weapon_cost
	return 1;

#define weapon_load
	return 30;

#define weapon_area
	return 14; // L0 7-2+

#define weapon_swap
	return sndSwapExplosive;

#define weapon_text
	return "pew pew pew";

#define weapon_fire
	var __angle = gunangle;

	sound_play(sndPistol);
	weapon_post(2, -6, 4);

	//Spawn Ally
		var i;
		for (i = 0; i < 4; i += 1)
		with (instance_create(x, y, Ally)) {
			motion_add(__angle + 22.5 - (i * 15) + (random(8) - 4) * other.accuracy, 20);
			image_angle = 0;
			team = other.team;
			creator = other;
		}

