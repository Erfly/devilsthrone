#define init
	global.allyl2 = sprite_add_weapon("sprites/ally_launcher_2.png", 3, 6);

#define weapon_sprt
	return global.allyl2;
	
#define weapon_name
	return "DOUBLE ALLY LAUNCHER";

#define weapon_type
	return 5;

#define weapon_auto
	return 0;

#define weapon_cost
	return 20;

#define weapon_load
	return 30;

#define weapon_area
	return 10; // L0 5-2+

#define weapon_swap
	return sndSwapExplosive;

#define weapon_text
	return "DOUBLE THE FRIENDS";

#define weapon_fire
	var __angle = gunangle;

	sound_play(sndPistol);
	weapon_post(2, -6, 4);

	//Spawn Ally
		var i;
		for (i = 0; i < 2; i += 1)
		with (instance_create(x, y, Ally)) {
			motion_add(__angle + 15 - (i * 30) + (random(8) - 4) * other.accuracy, 20);
			image_angle = 0;
			team = other.team;
			creator = other;
		}
