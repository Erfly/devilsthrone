#define init

	global.allylc = sprite_add_weapon("sprites/ally_chain_launcher.png", 3, 6);

#define weapon_name
	return "ALLY CHAIN LAUNCHER";

#define weapon_type
	return 5; // Energy Wep

#define weapon_auto
	return 1;
	
#define weapon_cost
	return 10;

#define weapon_load
	return 1;

#define weapon_sprt
	return global.allylc;

#define weapon_area
	return 14; // L0 7-2+

#define weapon_swap
	return sndSwapExplosive;

#define weapon_text
	return "LIKE MOTHER NATURE INTENDED";

#define weapon_fire
	var __angle = gunangle;
	sound_play(sndPistol);
	with (instance_create(x, y, Ally)) {
		motion_add(__angle + (random(8) - 4) * other.accuracy, 60);
		image_angle = 0;
		team = other.team;
		creator = other;
	}
	weapon_post(2, -6, 4);
