#define init

	global.allyls = sprite_add_weapon("sprites/ally_launcher_super.png", 3, 6);

#define weapon_name
	return "SUPER ALLY LAUNCHER";

#define weapon_type
	return 5; // Energy Wep

#define weapon_auto
	return 0;
	
#define weapon_cost
return 10;
#define weapon_load
return 30;
#define weapon_sprt
return global.allyls;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "pew pew pew";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
with (instance_create(x, y, BanditBoss)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 50);
	image_angle = 0;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);
