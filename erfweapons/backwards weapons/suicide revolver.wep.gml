#define init
global.suicide_pistol = sprite_add_weapon("sprites/suicide_pistol.png", -2, 3);
#define weapon_name
return "SUICIDE REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return global.suicide_pistol;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "pew pew pew";
#define weapon_fire
var __angle = gunangle;
var __len = 10;
sound_play(sndPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x+lengthdir_x(__len,__angle), y+lengthdir_y(__len,__angle), Bullet1)) {
	motion_add(__angle + 180 + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = enemy.team;
	creator = other;
}
weapon_post(2, -6, 4);
