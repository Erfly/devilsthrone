#define init
global.backwardsrevolver = sprite_add_weapon("sprites/backwards_revolver.png", -2, 6);
#define weapon_name
return "BACKWARDS REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return global.backwardsrevolver;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "wep wep wep";
#define weapon_fire
var __angle = gunangle;
var __len = 10;
sound_play(sndPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x+lengthdir_x(__len,__angle), y+lengthdir_y(__len,__angle), Bullet1)) {
	motion_add(__angle + 180 + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);
