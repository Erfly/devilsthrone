#define init
global.backwardsassault = sprite_add_weapon("sprites/backwards_assault_rifle.png", -2, 10);
#define weapon_name
return "BACKWARDS ASSAULT RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 11;
#define weapon_sprt
return global.backwardsassault;
#define weapon_area
return 2;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
with (instance_create(x, y, Burst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 3;
	time = 2;
	event_perform(ev_alarm, 0);
}

