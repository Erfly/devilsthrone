#define init
global.reversever = sprite_add_weapon("sprites/backwards_curly_revolver.png", 38, 6);
#define weapon_name
return "REVERSE CURLY REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return global.reversever;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "pew pew pew";
#define weapon_fire
var __angle = gunangle;
var __len = -20;
sound_play(sndPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x+lengthdir_x(__len,__angle), y+lengthdir_y(__len,__angle), Bullet1)) {
	motion_add(__angle + 180 + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);
