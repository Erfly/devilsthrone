#define init
global.curvedpistol = sprite_add_weapon("sprites/infinigun.png", -2, 10);
#define weapon_name
return "PISTOL";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return -(random(20000000900));
#define weapon_load
return 1;
#define weapon_sprt
return global.curvedpistol;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "pew pew pew";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
with (instance_create(x, y, BigRad)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);
