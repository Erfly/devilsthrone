#define init
	global.sprRawCoin = sprite_add("sprites/resizedcoins.png", 12, 1, 1);
	global.mskRawCoin = sprite_add("sprites/mskresizedcoins.png", 1, 16, 16);

#define rawcoin_create(_x, _y)
	with instance_create(_x, _y, CustomProjectile){
		name = "RawCoin";
		sprite_index = global.sprRawCoin;
		mask_index = global.mskRawCoin;
		hitid = [sprite_index, "RAW COIN"];
		damage = 12;
		force = 12;
		depth = 0;
		bounce = 0;
		typ = 1;
		on_wall = bb_wall;
		on_destroy = bb_destroy;
		return id;
	}

#define bb_wall
	 // Sound:
	sound_play(sndBouncerBounce);
	sound_play(sndBouncerSmg);

	 // Bounce:
	move_bounce_solid(true);
	repeat(3) instance_create(x, y, Dust); // Dust Visual
	view_shake_at(x, y, 5); // Bit of screenshake

	 // Shrink A Lil':
	image_xscale -= 0.05;
	image_yscale -= 0.05;

	 // Bounce Less:
	bounce += 1;
	if(bounce > 10) instance_destroy();

#define bb_destroy
	 // Sound:
	sound_play(sndBouncerShotgun);
	sound_play(sndLilHunterBouncer);

	 // Screenshake:
	view_shake_at(x, y, 30);

	 // Destroy Visual:
	var _ang = random(360);
	for(var i = _ang; i < _ang + 360; i += 180) with instance_create(x, y, BulletHit){
		sprite_index = sprHeavyBulletHit;
		image_angle = i;
	}