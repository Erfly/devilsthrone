#define init
	global.sprCoinGun = sprite_add_weapon("sprites/coingun.png", 2, 3);

#define weapon_name
	return "COIN GUN"; // Name 

#define weapon_type
	return 1; // Bullet Wep

#define weapon_area
	return 4; // L0 1-3+

#define weapon_load
	return 5; // 0.17 Seconds

#define weapon_swap
	return sndSwapPistol; // Swap Sound

#define weapon_fire
	sound_play(sndBouncerSmg);	// Sound
	weapon_post(5, -6, 3);		// Kick, Shift, Shake

	 // Empty Bullet Casing:
	scrShells(100, 25, 3 + random(2), sprBulletShell);

	 // Bouncer Bullet:
	with mod_script_call("mod", "coinproj", "rawcoin_create", x, y){
		move_contact_solid(other.gunangle, 8);
		motion_add(other.gunangle + (random_range(-4, 4) * other.accuracy), 4.5);
		image_angle = direction;
		team = other.team;
		creator = other;
	}

#define scrShells(_angle, _spread, _speed, _sprite)
	with instance_create(x, y, Shell){
		motion_add(other.gunangle + (other.right * _angle) + random_range(-_spread, _spread), _speed);
		sprite_index = _sprite;
	};

#define weapon_sprt
	return global.sprCoinGun; // Wep Sprite

#define weapon_text
	return "DO A TRICK SHOT"; // Loading Tip