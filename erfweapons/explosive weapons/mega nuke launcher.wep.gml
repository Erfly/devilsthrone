#define init
global.pocketnuke = sprite_add_weapon("sprites/mega_nuke_launcher.png", 3, 5);
#define weapon_name
return "MEGA NUKE LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 3;
#define weapon_load
return 15;
#define weapon_sprt
return global.meganukelauncher;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "big badda BOOM";
#define weapon_fire
var __angle = gunangle;
sound_play(sndNukeFire);
with (instance_create(x, y, Nuke)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 20);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);