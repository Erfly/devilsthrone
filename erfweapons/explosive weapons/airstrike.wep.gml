#define init
global.airstrike_launcher = sprite_add_weapon("sprites/airstrike launcher.png", -2, 10);
#define weapon_name
return "AIR STRIKE";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 50;
#define weapon_sprt
return global.airstrike_launcher;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "pew pew pew";
#define weapon_fire
var __angle = gunangle;
var __len = 110;
sound_play(sndPistol);
with (instance_create(x+lengthdir_x(__len,__angle), y+lengthdir_y(__len,__angle), RogueStrike)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 5);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);
