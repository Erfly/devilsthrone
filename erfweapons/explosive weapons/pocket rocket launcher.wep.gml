#define init
global.pocketrocket = sprite_add_weapon("sprites/pocket_rocket_launcher.png", 3, 5);
#define weapon_name
return "POCKET ROCKET";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 15;
#define weapon_sprt
return global.pocketrocket;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "is that a rocket in your pocket?";
#define weapon_fire
var __angle = gunangle;
var PocketRocketSound = audio_play_sound(sndRocket, 10, false);
audio_sound_pitch(PocketRocketSound, 1.2)
with (instance_create(x, y, Rocket)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 3);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);