#define init
global.pocketnuke = sprite_add_weapon("sprites/pocket_nuke_launcher.png", 3, 5);
#define weapon_name
return "NUKIE LAUNCHA";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 3;
#define weapon_load
return 15;
#define weapon_sprt
return global.pocketnuke;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "is that a rocket in your pocket?";
#define weapon_fire
var __angle = gunangle;
sound_play(sndNukeFire);
with (instance_create(x, y, Nuke)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 20);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);