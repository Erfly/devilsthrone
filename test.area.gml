#define init
global.sprFloorSeptic = sprite_add("septic/sprFloorSeptic.png",8 ,0 ,0)
global.sprFloorSepticExplo = sprite_add("septic/sprFloorSepticExplo.png",4 ,1 ,1)
global.sprWallSepticBot = sprite_add("septic/sprWallSepticBot.png",4 ,0 ,0)
global.sprWallSepticTop = sprite_add("septic/sprWallSepticTop.png",4 ,0 ,0)
global.sprWallSepticOut = sprite_add("septic/sprWallSepticOut.png",1 ,0 ,0)


#define area_name
return "BROSKI";

#define area_sprite(q)
switch (q) {
    case sprFloor1: return global.sprFloorSeptic;
    case sprFloor1B: return global.sprFloorSeptic;
    case sprFloor1Explo: return global.sprFloorSepticExplo;
    case sprWall1Trans: return sprWall2Trans;
    case sprWall1Bot: return global.sprWallSepticBot;
    case sprWall1Out: return global.sprWallSepticOut;
    case sprWall1Top: return global.sprWallSepticTop
    case sprDebris1: return sprDebris2;
}

#define area_transit
if (lastarea != "test" && area == 1) {
    area = "test";
}

#define area_finish
area = 2;
subarea = 1;

#define area_setup
goal = 40;
background_color = make_color_rgb(75, 91, 67);
BackCont.shadcol = c_black;
TopCont.fog = sprFog2;

#define area_make_floor
instance_create(x, y, Floor);
var turn = choose(0, 0, 180, 0, -90, 0, 0, 0, 0, 90, -90, 90, -90, 180);
direction += turn;
if (turn == 180 && point_distance(x, y, 10016, 10016) > 48) {
    // turnarounds - weapon chests spawn in such
    instance_create(x, y, Floor);
    instance_create(x + 16, y + 16, WeaponChest);
}
if (random(19 + instance_number(FloorMaker)) > 22) {
    // dead ends - ammo chests spawn in such
    if (point_distance(x, y, 10016, 10016) > 48) {
        instance_create(x + 16, y + 16, AmmoChest);
        instance_create(x, y, Floor);
    }
    instance_destroy();
} else if (random(4) < 1) {
    // branching
    instance_create(x, y, FloorMaker);
}

#define area_pop_enemies
instance_create(x + 16, y + 16, Bandit);
instance_create(x + 16, y + 16, Bandit);
instance_create(x + 16, y + 16, Turtle);

#define area_pop_props

#define area_mapdata(lx, ly, lp, ls, ws, ll)
return [lx, 9];