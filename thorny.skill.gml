#define init
global.sprThornyHUD = sprite_add("mutations/sprThornyHUD.png", 1, 7, 8);
global.sprThorny = sprite_add("mutations/sprThorny.png", 1, 12, 16);

#define skill_name
return "Thorny"

#define skill_text
return "EXTRA @ySPLINTERS@s ON SHOT"

#define skill_take 
sound_play(sndMutBoltMarrow)
with(GameCont){
wepmuts += 1
}

#define step
var thornySplinters = floor(GameCont.level / 3)
var p=[0,0,0,0]
with instances_matching(projectile, "thornyVar", null) {
	thornyVar = 1;
	if instance_is(creator, Player) {
		sound_play(sndSplinterGun);
		if p[creator.index]==1{
			continue
		}
		p[creator.index]=1;
		repeat(thornySplinters){
			with instance_create(x, y, Splinter) {
				thornyVar = 1;
				damage = 1;
				creator = other
				team = other.team
				motion_add(other.direction + (random(30) - 15), 20 + random(4));
				image_angle = direction;
			}
		}
	}
}


#define skill_icon
return global.sprThornyHUD;

#define skill_button 
sprite_index = global.sprThorny;

#define skill_tip
return choose("PRICKLY", "IT'S PAINFUL", "JUST LIKE PLANT")
