#define init
global.sprDeathaxe = sprite_add_weapon("sprDeathaxe.png", 11, 15);
#define weapon_name
return "DEATHAXE";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 45;
#define weapon_sprt
return global.sprDeathaxe;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapShotgun;
#define weapon_melee
return 1
#define weapon_text
return "";
#define weapon_fire
sound_play_gun(sndChickenSword, 0.1, 0.3);
var __angle = gunangle;
sound_play_gun(sndChickenSword, 0.1, 0.3);
var i;
with (instance_create(x, y, Slash)) {
	motion_add(__angle, 1);
	image_angle = direction;
	team = other.team;
	creator = other;
} 
wait 6;
sound_play_gun(sndChickenSword, 0.1, 0.3);
var __angle = gunangle;
sound_play_gun(sndChickenSword, 0.1, 0.3);
var i;
with (instance_create(x, y, Slash)) {
	motion_add(__angle + (random(44) - 22), 1);
	image_angle = direction;
	team = other.team;
	creator = other;
} 
wait 6;
sound_play_gun(sndChickenSword, 0.1, 0.3);
var __angle = gunangle;
sound_play_gun(sndChickenSword, 0.1, 0.3);
var i;
with (instance_create(x, y, Slash)) {
	motion_add(__angle + (random(88) - 44), 1);
	image_angle = direction;
	team = other.team;
	creator = other;
} 
wait 6;
sound_play_gun(sndChickenSword, 0.1, 0.3);
var __angle = gunangle;
sound_play_gun(sndChickenSword, 0.1, 0.3);
var i;
with (instance_create(x, y, Slash)) {
	motion_add(__angle + (random(176) - 88), 1);
	image_angle = direction;
	team = other.team;
	creator = other;
} 
