#define init
global.sprPurifier = sprite_add_weapon("sprJoy.png", 0, 8);

#define weapon_name
return "PURIFIER";

#define weapon_text
return "LEAVE ASHES";

#define weapon_sprt
return global.sprPurifier;

#define weapon_type
return 5;

#define weapon_melee
return 0;

#define weapon_cost
return 1;

#define weapon_load
return 10;

#define weapon_swap
return sndSwapEnergy;

#define weapon_area
return 7;

#define weapon_fire
var __angle = gunangle;
sound_play(sndLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle;
	team = other.team;
	event_perform(ev_alarm, 0);
	on_hit = null;
	on_wall = sound_play(sndLaser);
	with (instance_create(x, y, Laser)) {
		image_angle = -__angle;
		team = other.team;
		event_perform(ev_alarm, 0);
}

#define test_impact
sound_play(sndLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle;
	team = other.team;
	event_perform(ev_alarm, 0);
}

