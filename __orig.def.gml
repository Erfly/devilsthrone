
///// NTU REVOLVER.wep.gml
#define weapon_name
return "REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 6;
#define weapon_sprt
return sprRevolver;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "trusty old revolver";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);

///// NTU TRIPLE MACHINEGUN.wep.gml
#define weapon_name
return "TRIPLE MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 3;
#define weapon_load
return 4;
#define weapon_sprt
return sprTripleMachinegun;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "triple machinegun, so much fun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndTripleMachinegun);
repeat (3) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(70) - 35, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(6) - 3) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + 15 * other.accuracy + (random(6) - 3) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle - 15 * other.accuracy + (random(6) - 3) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -8, 4);

///// NTU WRENCH.wep.gml
#define weapon_name
return "WRENCH";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 22;
#define weapon_sprt
return sprWrench;
#define weapon_area
return 2;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "ratchet";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), Slash)) {
	damage = 4;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 12, 1);

///// NTU MACHINEGUN.wep.gml
#define weapon_name
return "MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return sprMachinegun;
#define weapon_area
return 0;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndMachinegun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 3);

///// NTU SHOTGUN.wep.gml
#define weapon_name
return "SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 18;
#define weapon_sprt
return sprShotgun;
#define weapon_area
return 0;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (7) {
	with (instance_create(x, y, Bullet2)) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -12, 8);

///// NTU CROSSBOW.wep.gml
#define weapon_name
return "CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 25;
#define weapon_sprt
return sprCrossbow;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (instance_create(x, y, Bolt)) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -40, 4);

///// NTU GRENADE LAUNCHER.wep.gml
#define weapon_name
return "GRENADE LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 20;
#define weapon_sprt
return sprNader;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "be careful with those grenades";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (instance_create(x, y, Grenade)) {
	sticky = 0;
	motion_add(__angle + (random(6) - 3) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -10, 2);

///// NTU DOUBLE SHOTGUN.wep.gml
#define weapon_name
return "DOUBLE SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 34;
#define weapon_sprt
return sprSuperShotgun;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "double shotgun, double fun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDoubleShotgun);
repeat (14) {
	with (instance_create(x, y, Bullet2)) {
		motion_add(__angle + (random(50) - 30) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 2);
weapon_post(8, -15, 16);

///// NTU MINIGUN.wep.gml
#define weapon_name
return "MINIGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 1;
#define weapon_sprt
return sprMinigun;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "time to rain bullets";
#define weapon_fire
var __angle = gunangle;
sound_play(sndMinigun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(80) - 40, 3 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(26) - 13) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 0.6);
weapon_post(4, -7, 4);

///// NTU AUTO SHOTGUN.wep.gml
#define weapon_name
return "AUTO SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 4;
#define weapon_sprt
return sprAutoShotgun;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "just hold down the trigger";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (6) {
	with (instance_create(x, y, Bullet2)) {
		motion_add(__angle + (random(30) - 15) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(5, -12, 8);

///// NTU AUTO CROSSBOW.wep.gml
#define weapon_name
return "AUTO CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 8;
#define weapon_sprt
return sprAutoCrossbow;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "225 bolts per minute";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (instance_create(x, y, Bolt)) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -40, 5);

///// NTU SUPER CROSSBOW.wep.gml
#define weapon_name
return "SUPER CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 34;
#define weapon_sprt
return sprSuperCrossbow;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "5 bolts per shot";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSuperCrossbow);
with (instance_create(x, y, Bolt)) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bolt)) {
	motion_add(__angle + 5 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bolt)) {
	motion_add(__angle - 5 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bolt)) {
	motion_add(__angle + 10 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bolt)) {
	motion_add(__angle - 10 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 1);
weapon_post(8, -60, 14);

///// NTU SHOVEL.wep.gml
#define weapon_name
return "SHOVEL";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 35;
#define weapon_sprt
return sprShovel;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "dig";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndShovel);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), Slash)) {
	damage = 8;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x + lengthdir_x(__long_arms * 15, __angle + 60 * accuracy), y + lengthdir_y(__long_arms * 15, __angle + 60 * accuracy), Slash)) {
	damage = 8;
	motion_add(__angle + 60 * other.accuracy, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x + lengthdir_x(__long_arms * 15, __angle - 60 * accuracy), y + lengthdir_y(__long_arms * 15, __angle - 60 * accuracy), Slash)) {
	damage = 8;
	motion_add(__angle - 60 * other.accuracy, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 24, 1);

///// NTU BAZOOKA.wep.gml
#define weapon_name
return "BAZOOKA";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 30;
#define weapon_sprt
return sprBazooka;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRocket);
with (instance_create(x, y, Rocket)) {
	motion_add(__angle + (random(4) - 2) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(10, -30, 4);

///// NTU STICKY LAUNCHER.wep.gml
#define weapon_name
return "STICKY LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 25;
#define weapon_sprt
return sprStickyNader;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "dont touch sticky nades";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (instance_create(x, y, Grenade)) {
	sprite_index = sprStickyGrenade;
	sticky = 1;
	motion_add(__angle + (random(6) - 3) * other.accuracy, 11);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -10, 2);

///// NTU SMG.wep.gml
#define weapon_name
return "SMG";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return sprSmg;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(60) - 30, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(32) - 16) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 3);

///// NTU ASSAULT RIFLE.wep.gml
#define weapon_name
return "ASSAULT RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 11;
#define weapon_sprt
return sprARifle;
#define weapon_area
return 2;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "";
#define weapon_fire
with (instance_create(x, y, Burst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 3;
	time = 2;
	event_perform(ev_alarm, 0);
}

///// NTU DISC GUN.wep.gml
#define weapon_name
return "DISC GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 8;
#define weapon_sprt
return sprDiscGun;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "we hereby sincerely apologize";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDiscgun);
with (instance_create(x, y, Disc)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 5);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -10, 6);

///// NTU LASER PISTOL.wep.gml
#define weapon_name
return "LASER PISTOL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 10;
#define weapon_sprt
return sprLaserGun;
#define weapon_area
return 2;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "futuristic weaponry";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle + (random(2) - 1) * other.accuracy;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(2, -3, 2);

///// NTU LASER RIFLE.wep.gml
#define weapon_name
return "LASER RIFLE";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 7;
#define weapon_sprt
return sprLaserRifle;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle + (random(8) - 4) * other.accuracy;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(5, -3, 2);

///// NTU SLUGGER.wep.gml
#define weapon_name
return "SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 22;
#define weapon_sprt
return sprSlugger;
#define weapon_area
return 2;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSlugger);
with (instance_create(x, y, Slug)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -14, 10);

///// NTU GATLING SLUGGER.wep.gml
#define weapon_name
return "GATLING SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 8;
#define weapon_sprt
return sprGatlingSlugger;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "time to gatle";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSlugger);
with (instance_create(x, y, Slug)) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -10, 10);

///// NTU ASSAULT SLUGGER.wep.gml
#define weapon_name
return "ASSAULT SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 24;
#define weapon_sprt
return sprAssaultSlugger;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "";
#define weapon_fire
with (instance_create(x, y, SlugBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 3;
	time = 3;
	event_perform(ev_alarm, 0);
}

///// NTU ENERGY SWORD.wep.gml
global.sndLaserSwordUpg = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSwordUpg");
global.sndLaserSword = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSword");
#define weapon_name
return "ENERGY SWORD";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 11;
#define weapon_sprt
return sprEnergySword;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "zzzwwoonggg";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLaserSwordUpg);
} else sound_play(global.sndLaserSword);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), EnergySlash)) {
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 7);
weapon_post(-4, 24, 1);

///// NTU SUPER SLUGGER.wep.gml
#define weapon_name
return "SUPER SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 40;
#define weapon_sprt
return sprSuperSlugger;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "no need to aim";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSuperSlugger);
motion_add(__angle + 180, 3);
with (instance_create(x, y, Slug)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Slug)) {
	motion_add(__angle + 10 * other.accuracy + (random(8) - 4) * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Slug)) {
	motion_add(__angle + 20 * other.accuracy + (random(8) - 4) * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Slug)) {
	motion_add(__angle - 10 * other.accuracy + (random(8) - 4) * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Slug)) {
	motion_add(__angle - 20 * other.accuracy + (random(8) - 4) * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -10, 15);

///// NTU HYPER RIFLE.wep.gml
#define weapon_name
return "HYPER RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 3;
#define weapon_sprt
return sprHyperRifle;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "hyper time";
#define weapon_fire
with (instance_create(x, y, Burst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 5;
	time = 1;
	event_perform(ev_alarm, 0);
}

///// NTU SCREWDRIVER.wep.gml
#define weapon_name
return "SCREWDRIVER";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 11;
#define weapon_sprt
return sprScrewDriver;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "screwdriver will fix it";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndScrewdriver);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 10, __angle), y + lengthdir_y(__long_arms * 10, __angle), Shank)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 3 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 4);
weapon_post(-8, 12, 1);

///// NTU LASER MINIGUN.wep.gml
#define weapon_name
return "LASER MINIGUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 2;
#define weapon_sprt
return sprLaserMinigun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "energy bill off the charts";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle + (random(16) - 8) * other.accuracy;
	team = other.team;
	event_perform(ev_alarm, 0);
}
motion_add(__angle + 180, 1);
weapon_post(6, -5, 2);
motion_add(__angle + 180, 0.6);

///// NTU BLOOD LAUNCHER.wep.gml
#define weapon_name
return "BLOOD LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 12;
#define weapon_sprt
return sprBloodNader;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "built with spare parts";
#define weapon_fire
var __angle = gunangle;
sound_play(sndBloodLauncher);
with (instance_create(x, y, BloodGrenade)) {
	sticky = 0;
	motion_add(__angle + (random(12) - 6) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -5, 3);

///// NTU SPLINTER GUN.wep.gml
#define weapon_name
return "SPLINTER GUN";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 23;
#define weapon_sprt
return sprSplinterGun;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "this will hurt";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSplinterGun);
with (instance_create(x, y, Splinter)) {
	motion_add(__angle + (random(6) - 3) * other.accuracy, 20 + random(4));
	image_angle = direction;
	team = other.team;
	creator = other;
}
repeat (2) {
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(20) - 10) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(10) - 5) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(-3, -15, 3);

///// NTU TOXIC BOW.wep.gml
#define weapon_name
return "TOXIC BOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 29;
#define weapon_sprt
return sprToxicBow;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "hold breath while firing";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (instance_create(x, y, ToxicBolt)) {
	motion_add(__angle, 22);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -40, 5);

///// NTU SENTRY GUN.wep.gml
#define weapon_name
return "SENTRY GUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 24;
#define weapon_load
return 60;
#define weapon_sprt
return sprSentryGun;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (instance_create(x, y, SentryGun)) {
	sticky = 0;
	motion_add(__angle, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(-10, -5, 0);

///// NTU WAVE GUN.wep.gml
#define weapon_name
return "WAVE GUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 17;
#define weapon_sprt
return sprWaveGun;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "shoot em up";
#define weapon_fire
with (instance_create(x, y, WaveBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 7;
	time = 1;
	event_perform(ev_alarm, 0);
}

///// NTU PLASMA GUN.wep.gml
#define weapon_name
return "PLASMA GUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 16;
#define weapon_sprt
return sprPlasmaGun;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "fun fun";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaUpg);
} else sound_play(sndPlasma);
with (instance_create(x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), PlasmaBall)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 1.5);
weapon_post(5, -3, 3);
resetSpeed = 0;

///// NTU PLASMA CANNON.wep.gml
#define weapon_name
return "PLASMA CANNON";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 8;
#define weapon_load
return 45;
#define weapon_sprt
return sprPlasmaCannon;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "fun fun fun";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaBigUpg);
} else sound_play(sndPlasmaBig);
with (instance_create(x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), PlasmaBig)) {
	motion_add(__angle + (random(4) - 2) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 6);
weapon_post(10, -8, 8);
resetSpeed = 0;

///// NTU ENERGY HAMMER.wep.gml
global.sndLaserSwordUpg = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSwordUpg");
global.sndLaserSword = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSword");
#define weapon_name
return "ENERGY HAMMER";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 24;
#define weapon_sprt
return sprEnergyHammer;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "break a leg";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLaserSwordUpg);
} else sound_play(global.sndLaserSword);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), EnergyHammerSlash)) {
	motion_add(__angle, 1 + 2 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 7);
weapon_post(-3, 32, 2);

///// NTU JACKHAMMER.wep.gml
#define weapon_name
return "JACKHAMMER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 12;
#define weapon_sprt
return sprJackHammer;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapMotorized;
#define weapon_text
return "break some legs";
#define weapon_fire
with (instance_create(x, y, SawBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 12;
	time = 1;
	event_perform(ev_alarm, 0);
}

///// NTU FLAK CANNON.wep.gml
#define weapon_name
return "FLAK CANNON";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 26;
#define weapon_sprt
return sprFlakCannon;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "10/10";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFlakCannon);
with (instance_create(x, y, FlakBullet)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 11 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(7, -32, 4);

///// NTU GOLDEN REVOLVER.wep.gml
#define weapon_name
return "GOLDEN REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return sprGoldRevolver;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "B-)";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -8, 5);

///// NTU GOLDEN WRENCH.wep.gml
global.sprGoldenSlash = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenSlash");
#define weapon_name
return "GOLDEN WRENCH";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 17;
#define weapon_sprt
return sprGoldWrench;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "shiny wrench";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), Slash)) {
	sprite_index = global.sprGoldenSlash;
	damage = 4;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-6, 16, 2);

///// NTU GOLDEN MACHINEGUN.wep.gml
#define weapon_name
return "GOLDEN MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return sprGoldMachinegun;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "expensive machinegun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndMachinegun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -8, 4);

///// NTU GOLDEN SHOTGUN.wep.gml
#define weapon_name
return "GOLDEN SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 20;
#define weapon_sprt
return sprGoldShotgun;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "beautiful shotgun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (8) {
	with (instance_create(x, y, Bullet2)) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(8, -16, 10);

///// NTU GOLDEN CROSSBOW.wep.gml
global.sprGoldBolt = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldBolt");
#define weapon_name
return "GOLDEN CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 23;
#define weapon_sprt
return sprGoldCrossbow;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "velvet handles";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (instance_create(x, y, Bolt)) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	sprite_index = global.sprGoldBolt;
}
weapon_post(6, -44, 6);

///// NTU GOLDEN GRENADE LAUNCHER.wep.gml
#define weapon_name
return "GOLDEN GRENADE LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 20;
#define weapon_sprt
return sprGoldNader;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "even the grenades are gold";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (instance_create(x, y, Grenade)) {
	sprite_index = sprGoldGrenade;
	sticky = 0;
	motion_add(__angle, 12);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(7, -12, 4);

///// NTU GOLDEN LASER PISTOL.wep.gml
#define weapon_name
return "GOLDEN LASER PISTOL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 8;
#define weapon_sprt
return sprGoldLaserGun;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "this thing gets hot";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(4, -6, 4);

///// NTU CHICKEN SWORD.wep.gml
global.sndSword1 = mod_script_call("mod", "NTU", "ntu_sound", "sndSword1");
global.sndSword2 = mod_script_call("mod", "NTU", "ntu_sound", "sndSword2");
#define weapon_name
return "CHICKEN SWORD";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 16;
#define weapon_sprt
return sprSword;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "chicken loves her sword";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(choose(global.sndSword1, global.sndSword2));
instance_create(x, y, Dust);
ang = __angle;
move_contact_solid(ang, 5);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), Slash)) {
	ang = other.ang;
	damage = 3;
	motion_add(ang, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
speed = -speed * 0.5;
weapon_post(-6, 8, 1);

///// NTU NUKE LAUNCHER.wep.gml
#define weapon_name
return "NUKE LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 50;
#define weapon_sprt
return sprNukeLauncher;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "this is what started it all";
#define weapon_fire
var __angle = gunangle;
sound_play(sndNukeFire);
with (instance_create(x, y, Nuke)) {
	motion_add(__angle + (random(4) - 2) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(10, -40, 8);

///// NTU ION CANNON.wep.gml
#define weapon_name
return "ION CANNON";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 14;
#define weapon_sprt
return sprIonCannon;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "death from above";
#define weapon_fire
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (instance_create(x, y, IonBurst)) {
	team = other.team;
	creator = other;
	ammo = 20;
	time = 1;
	alarm[0] = 25;
}
weapon_post(3, 0, 6);

///// NTU QUADRUPLE MACHINEGUN.wep.gml
#define weapon_name
return "QUADRUPLE MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 4;
#define weapon_sprt
return sprQuadrupleMachinegun;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "the future is here";
#define weapon_fire
var __angle = gunangle;
sound_play(sndQuadMachinegun);
repeat (4) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(70) - 35, 4 + random(3));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + 6 * other.accuracy + (random(6) - 3) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle - 6 * other.accuracy + (random(6) - 3) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + 18 * other.accuracy + (random(6) - 3) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle - 18 * other.accuracy + (random(6) - 3) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -10, 6);

///// NTU FLAMETHROWER.wep.gml
#define weapon_name
return "FLAMETHROWER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 6;
#define weapon_sprt
return sprFlameThrower;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "burn burn burn";
#define weapon_fire
if (!instance_exists(FlameSound)) instance_create(x, y, FlameSound);
with (instance_create(x, y, FlameBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 9;
	time = 1;
	event_perform(ev_alarm, 0);
}

///// NTU DRAGON.wep.gml
#define weapon_name
return "DRAGON";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return sprDragon;
#define weapon_area
return 13;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "hot breath";
#define weapon_fire
if (!instance_exists(DragonSound)) instance_create(x, y, DragonSound);
with (instance_create(x, y, DragonBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 5;
	time = 1;
	event_perform(ev_alarm, 0);
}

///// NTU FLARE GUN.wep.gml
#define weapon_name
return "FLARE GUN";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 25;
#define weapon_sprt
return sprFlareGun;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "signal for help";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFlare);
with (instance_create(x, y, Flare)) {
	sticky = 0;
	motion_add(__angle + (random(14) - 7) * other.accuracy, 9);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -10, 5);

///// NTU ENERGY SCREWDRIVER.wep.gml
#define weapon_name
return "ENERGY SCREWDRIVER";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return sprEnergyScrewDriver;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "future fixing";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndScrewdriver);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 10, __angle), y + lengthdir_y(__long_arms * 10, __angle), EnergyShank)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 3 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 3);
weapon_post(-8, 12, 2);

///// NTU HYPER LAUNCHER.wep.gml
#define weapon_name
return "HYPER LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 9;
#define weapon_sprt
return sprHyperLauncher;
#define weapon_area
return 14;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "point and click";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHyperLauncher);
with (instance_create(x, y, HyperGrenade)) {
	direction = __angle + (random(4) - 2) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -20, 4);

///// NTU LASER CANNON.wep.gml
#define weapon_name
return "LASER CANNON";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 3;
#define weapon_load
return 32;
#define weapon_sprt
return sprLaserCannon;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "oh laser cannon";
#define weapon_fire
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (instance_create(x, y, LaserCannon)) {
	creator = other.id;
	ammo = 5 + __laser_brain * 2;
	time = 1;
	team = other.team;
	alarm[0] = 15;
}

///// NTU RUSTY REVOLVER.wep.gml
#define weapon_name
return "RUSTY REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 7;
#define weapon_sprt
return sprRustyRevolver;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "this revolver is ancient";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRustyRevolver);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle, 15.6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -8, 5);

///// NTU LIGHTNING PISTOL.wep.gml
global.sndLightning3 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning3");
global.sndLightning1 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning1");
#define weapon_name
return "LIGHTNING PISTOL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 10;
#define weapon_sprt
return sprLightningPistol;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "thunder";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLightning3);
} else sound_play(global.sndLightning1);
with (instance_create(x, y, Lightning)) {
	image_angle = __angle + (random(30) - 15) * other.accuracy;
	team = other.team;
	ammo = 14;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
}
weapon_post(4, -3, 5);

///// NTU LIGHTNING RIFLE.wep.gml
global.sndLightning2 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning2");
global.sndLightning3 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning3");
global.sndLightning1 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning1");
#define weapon_name
return "LIGHTNING RIFLE";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 28;
#define weapon_sprt
return sprLightningRifle;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "a storm is coming";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(choose(global.sndLightning2, global.sndLightning3));
} else sound_play(global.sndLightning1);
with (instance_create(x, y, Lightning)) {
	image_angle = __angle + (random(6) - 3) * other.accuracy;
	team = other.team;
	ammo = 30;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
}
weapon_post(8, -6, 8);

///// NTU LIGHTNING SHOTGUN.wep.gml
global.sndThunder = mod_script_call("mod", "NTU", "ntu_sound", "sndThunder");
global.sndLightning2 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning2");
global.sndLightning3 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning3");
#define weapon_name
return "LIGHTNING SHOTGUN";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 24;
#define weapon_sprt
return sprLightningShotgun;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "hurricane";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndThunder);
} else sound_play(choose(global.sndLightning2, global.sndLightning3));
repeat (8) {
	with (instance_create(x, y, Lightning)) {
		image_angle = __angle + (random(180) - 60) * other.accuracy;
		team = other.team;
		ammo = 9 + random(3);
		event_perform(ev_alarm, 0);
		visible = false;
		with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
	}
}
weapon_post(5, -4, 10);

///// NTU BLOOD SHOTGUN.wep.gml
global.sprBloodShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBloodShotgun");
#define weapon_name
return "BLOOD SHOTGUN";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 16;
#define weapon_sprt
return global.sprBloodShotgun;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "explosions from your heart";
#define weapon_fire
var __angle = gunangle;
sound_play(sndBloodLauncher);
repeat (2) {
	with (instance_create(x, y, BloodGrenade)) {
		visible = false;
		sticky = 0;
		motion_add(__angle + (random(30) - 15) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		with (instance_create(x, y, BloodStreak)) image_angle = other.direction;
		alarm[0] = 3;
	}
}
weapon_post(5, -5, 4);

///// NTU BLOOD PISTOL.wep.gml
global.sprBloodPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprBloodPistol");
global.sndBloodPistol = mod_script_call("mod", "NTU", "ntu_sound", "sndBloodPistol");
#define weapon_name
return "BLOOD PISTOL";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 1;
#define weapon_sprt
return global.sprBloodPistol;
#define weapon_area
return 0;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "blood weapons destroy projectiles";
#define weapon_fire
var __angle = gunangle;
sound_play_gun(global.sndBloodPistol, 0.2, 0.4);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU Blood Bullet", "scr_create", x, y)) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 2);

///// NTU POP GUN.wep.gml
#define weapon_name
return "POP GUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 2;
#define weapon_sprt
return sprPopGun;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "not the real one";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPopgun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet2)) {
	motion_add(__angle + (random(16) - 8) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 2);

///// NTU BLACKHOLE GENERATOR.wep.gml
global.sprBlackholeGenerator = mod_script_call("mod", "NTU", "ntu_sprite", "sprBlackholeGenerator");
global.sndLightningPlasma2 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightningPlasma2");
#define weapon_name
return "BLACKHOLE GENERATOR";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 7;
#define weapon_load
return 50;
#define weapon_sprt
return global.sprBlackholeGenerator;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "this thing sucks";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndLightningPlasma2);
repeat (2) {
	with (instance_create(x, y, Lightning)) {
		image_angle = __angle + (random(60) - 30) * other.accuracy;
		team = other.team;
		ammo = 7;
		event_perform(ev_alarm, 0);
		visible = false;
		with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
	}
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Implosion")) {
	sticky = 0;
	motion_add(__angle + (random(6) - 3) * other.accuracy, 5);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -14, 6);

///// NTU POP RIFLE.wep.gml
#define weapon_name
return "POP RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 9;
#define weapon_sprt
return sprPopRifle;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "just a copy";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Burst2")) {
	creator = other.id;
	ammo = 3;
	time = 2;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU HYPER POP RIFLE.wep.gml
global.sprHyperPopRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperPopRifle");
#define weapon_name
return "HYPER POP RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprHyperPopRifle;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "hyper pop time";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Burst2")) {
	creator = other.id;
	ammo = 5;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU QUADRUPLE SHOTGUN.wep.gml
global.sprQuadrupleShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprQuadrupleShotgun");
#define weapon_name
return "QUADRUPLE SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 43;
#define weapon_sprt
return global.sprQuadrupleShotgun;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "quadruple shotgun, quadruple fun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDoubleShotgun);
sound_play(sndShotgun);
repeat (28) {
	with (instance_create(x, y, Bullet2)) {
		motion_add(__angle + (random(50) - 30) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 2);
weapon_post(12, -30, 20);

///// NTU SWORD GUN.wep.gml
global.sprSwordGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSwordGun");
#define weapon_name
return "SWORD GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 12;
#define weapon_sprt
return global.sprSwordGun;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "have you tried sword gun#with bolt marrow?#Or shotgun shoulders?";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SwordBullet")) {
	motion_add(__angle + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);

///// NTU AUTO GRENADE LAUNCHER.wep.gml
global.sprAutoNader = mod_script_call("mod", "NTU", "ntu_sprite", "sprAutoNader");
#define weapon_name
return "AUTO GRENADE LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprAutoNader;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "BOOM BOOM BOOM BOOM";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (instance_create(x, y, Grenade)) {
	sticky = 0;
	motion_add(__angle + (random(10) - 3) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -10, 2);

///// NTU OOPS GUN.wep.gml
global.sprOopsGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprOopsGun");
#define weapon_name
return "OOPS GUN";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 7;
#define weapon_sprt
return global.sprOopsGun;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "suicidal";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
Player.my_health = 0;
weapon_post(40, -800, 500);

///// NTU MORPH-O-RAY.wep.gml
global.sprMorphORay = mod_script_call("mod", "NTU", "ntu_sprite", "sprMorphORay");
#define weapon_name
return "MORPH-O-RAY";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 7;
#define weapon_sprt
return global.sprMorphORay;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "Eagle eyes also increases#MORPH-O-RAY's range";
#define weapon_fire
if (!instance_exists(FlameSound)) instance_create(x, y, FlameSound);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Ray")) {
	creator = other.id;
	ammo = 9;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
	explosive = 2;
}

///// NTU SUPER BAZOOKA.wep.gml
#define weapon_name
return "SUPER BAZOOKA";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 34;
#define weapon_sprt
return sprSuperBazooka;
#define weapon_area
return 13;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return '"JW could not resist"#- Rami 2015';
#define weapon_fire
var __angle = gunangle;
sound_play(sndSuperBazooka);
motion_add(__angle + 180, 4);
with (instance_create(x, y, Rocket)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Rocket)) {
	motion_add(__angle + 10 * other.accuracy + (random(4) - 2) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Rocket)) {
	motion_add(__angle + 20 * other.accuracy + (random(4) - 2) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Rocket)) {
	motion_add(__angle - 10 * other.accuracy + (random(4) - 2) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Rocket)) {
	motion_add(__angle - 20 * other.accuracy + (random(4) - 2) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -60, 14);

///// NTU GATLING BAZOOKA.wep.gml
#define weapon_name
return "GATLING BAZOOKA";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 12;
#define weapon_sprt
return sprGatlingBazooka;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRocket);
with (instance_create(x, y, Rocket)) {
	motion_add(__angle + (random(6) - 3) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(10, -30, 4);

///// NTU EXPLOSIVE MORPH-O-RAY.wep.gml
global.sprExplosiveMorphORay = mod_script_call("mod", "NTU", "ntu_sprite", "sprExplosiveMorphORay");
#define weapon_name
return "EXPLOSIVE MORPH-O-RAY";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 7;
#define weapon_sprt
return global.sprExplosiveMorphORay;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "butcher";
#define weapon_fire
if (!instance_exists(FlameSound)) instance_create(x, y, FlameSound);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Ray")) {
	creator = other.id;
	ammo = 9;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
	explosive = 3;
}

///// NTU TRIPLE POPGUN.wep.gml
global.sprTriplePopGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprTriplePopGun");
#define weapon_name
return "TRIPLE POPGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 2;
#define weapon_sprt
return global.sprTriplePopGun;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "rapid fire!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndTripleMachinegun);
repeat (2) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(70) - 35, 2 + random(2));
with (instance_create(x, y, Bullet2)) {
	motion_add(__angle + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet2)) {
	motion_add(__angle + 15 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet2)) {
	motion_add(__angle - 15 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -8, 4);

///// NTU IDKWID.wep.gml
global.sprIdkwid = mod_script_call("mod", "NTU", "ntu_sprite", "sprIdkwid");
#define weapon_name
return "IDKWID";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 12;
#define weapon_load
return 2;
#define weapon_sprt
return global.sprIdkwid;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "I don't know what it does";
#define weapon_fire
// todo

///// NTU PLASMA RIFLE.wep.gml
#define weapon_name
return "PLASMA RIFLE";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 9;
#define weapon_sprt
return sprPlasmaRifle;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaUpg);
} else sound_play(sndPlasma);
with (instance_create(x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), PlasmaBall)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 3);
weapon_post(5, -3, 2);
resetSpeed = 0;

///// NTU PLASMA MINIGUN.wep.gml
#define weapon_name
return "PLASMA MINIGUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 3;
#define weapon_sprt
return sprPlasmaMinigun;
#define weapon_area
return 13;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaUpg);
} else sound_play(sndPlasma);
with (instance_create(x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), PlasmaBall)) {
	motion_add(__angle + (random(16) - 8) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 3);
weapon_post(5, -3, 3);
resetSpeed = 0;

///// NTU HYPER SLUGGER.wep.gml
#define weapon_name
return "HYPER SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 11;
#define weapon_sprt
return sprHyperSlugger;
#define weapon_area
return 14;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "slagger";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHyperSlugger);
with (instance_create(x, y, HyperSlug)) {
	direction = __angle + (random(4) - 2) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -20, 4);

///// NTU SUPER PLASMA CANNON.wep.gml
global.sndSuperPlasmaCannon = mod_script_call("mod", "NTU", "ntu_sound", "sndSuperPlasmaCannon");
#define weapon_name
return "SUPER PLASMA CANNON";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 24;
#define weapon_load
return 350;
#define weapon_sprt
return sprSuperPlasmaCannon;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "SPC!";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndSuperPlasmaCannon);
with (instance_create(x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), PlasmaHuge)) {
	motion_add(__angle + (random(4) - 2) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 6);
weapon_post(10, -8, 8);
resetSpeed = 0;

///// NTU HUNTER SNIPER.wep.gml
global.sprSniper = mod_script_call("mod", "NTU", "ntu_sprite", "sprSniper");
#define weapon_name
return "HUNTER SNIPER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 16;
#define weapon_sprt
return global.sprSniper;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "hunter prefers bolt weapons";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSniperFire);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + 4, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle - 4, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(3, -13, 4);

///// NTU SUPER SWORD GUN.wep.gml
global.sprSuperSwordGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperSwordGun");
#define weapon_name
return "SUPER SWORD GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 10;
#define weapon_load
return 35;
#define weapon_sprt
return global.sprSuperSwordGun;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "SLASH SLASH SLASH SLASH SLASH";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHeavyCrossbow);
motion_add(__angle + 180, 16);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SwordBullet")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SwordBullet")) {
	motion_add(__angle + 10 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SwordBullet")) {
	motion_add(__angle + 20 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SwordBullet")) {
	motion_add(__angle - 10 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SwordBullet")) {
	motion_add(__angle - 20 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -60, 14);

///// NTU MEGA LASER PISTOL.wep.gml
global.sprMegaLaserPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprMegaLaserPistol");
#define weapon_name
return "MEGA LASER PISTOL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 35;
#define weapon_sprt
return global.sprMegaLaserPistol;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "IMA FIRIN MAH LAZOR!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndLaserUpg);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), "MegaLaser")) {
	image_angle = __angle + (random(2) - 1) * other.accuracy;
	team = other.team;
	event_perform(ev_alarm, 0);
}
motion_add(__angle + 180, 0.6);
weapon_post(6, -3, 4);

///// NTU BLOOD RIFLE.wep.gml
global.sprBloodRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprBloodRifle");
global.sndBloodPistol = mod_script_call("mod", "NTU", "ntu_sound", "sndBloodPistol");
#define weapon_name
return "BLOOD RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprBloodRifle;
#define weapon_area
return 2;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "bloody hell";
#define weapon_fire
var __angle = gunangle;
sound_play_gun(global.sndBloodPistol, 0.2, 0.3);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU Blood Bullet", "scr_create", x, y)) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 2.2);

///// NTU TRIPLE BLOOD GUN.wep.gml
global.sprTripleBloodGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprTripleBloodGun");
global.sndBloodPistol = mod_script_call("mod", "NTU", "ntu_sound", "sndBloodPistol");
#define weapon_name
return "TRIPLE BLOOD GUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 3;
#define weapon_load
return 2;
#define weapon_sprt
return global.sprTripleBloodGun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "fueled by fleshwounds";
#define weapon_fire
var __angle = gunangle;
sound_play_gun(global.sndBloodPistol, 0.2, 0.4);
repeat (3) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(70) - 35, 2 + random(2));
for (var i = -15; i <= 15; i += 15) {
	with (mod_script_call("mod", "NTU Blood Bullet", "scr_create", x, y)) {
		motion_add(__angle + (i + random_range(-3, 3)) * other.accuracy, 16);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(5, -8, 3);

///// NTU ERASER.wep.gml
#define weapon_name
return "ERASER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 18;
#define weapon_sprt
return sprEraser;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "drawer";
#define weapon_fire
sound_play(sndEraser);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EraserBurst")) {
	mox = mouse_x;
	moy = mouse_y;
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(6, -8, 5);

///// NTU SUPER ERASER.wep.gml
global.sprSuperEraser = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperEraser");
#define weapon_name
return "SUPER ERASER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 10;
#define weapon_load
return 40;
#define weapon_sprt
return global.sprSuperEraser;
#define weapon_area
return 14;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "pencils";
#define weapon_fire
var __angle = gunangle;
var __dist = mod_script_call("mod", "NTU", "ntu_wep_dist");
motion_add(__angle + 180, 16);
sound_play(sndEraser);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EraserBurst")) {
	mox = mouse_x;
	moy = mouse_y;
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EraserBurst")) {
	mox = x + lengthdir_x(__dist, __angle + 10 * other.accuracy + (random(4) - 2) * other.accuracy);
	moy = y + lengthdir_y(__dist, __angle + 10 * other.accuracy + (random(4) - 2) * other.accuracy);
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EraserBurst")) {
	mox = x + lengthdir_x(__dist, __angle + 20 * other.accuracy + (random(4) - 2) * other.accuracy);
	moy = y + lengthdir_y(__dist, __angle + 20 * other.accuracy + (random(4) - 2) * other.accuracy);
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EraserBurst")) {
	mox = x + lengthdir_x(__dist, __angle - 10 * other.accuracy + (random(4) - 2) * other.accuracy);
	moy = y + lengthdir_y(__dist, __angle - 10 * other.accuracy + (random(4) - 2) * other.accuracy);
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EraserBurst")) {
	mox = x + lengthdir_x(__dist, __angle - 20 * other.accuracy + (random(4) - 2) * other.accuracy);
	moy = y + lengthdir_y(__dist, __angle - 20 * other.accuracy + (random(4) - 2) * other.accuracy);
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(8, -60, 14);

///// NTU  DOUBLE MINIGUN.wep.gml
#define weapon_name
return " DOUBLE MINIGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 1;
#define weapon_sprt
return sprDoubleMinigun;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "time to rain more bullets";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDoubleMinigun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(80) - 40, 3 + random(2));
repeat (2) {
	with (instance_create(x, y, Bullet1)) {
		motion_add(__angle + (random(32) - 16) * other.accuracy, 16);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 1.2);
weapon_post(8, -7, 8);

///// NTU SPLINTER PISTOL.wep.gml
#define weapon_name
return "SPLINTER PISTOL";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 10;
#define weapon_sprt
return sprSplinterPistol;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "wooden pieces";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSplinterGun);
repeat (2) {
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(20) - 10) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(10) - 5) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(-2, -15, 2);

///// NTU DIRECTOR SLUGGER.wep.gml
global.sprDirectorSlugger = mod_script_call("mod", "NTU", "ntu_sprite", "sprDirectorSlugger");
#define weapon_name
return "DIRECTOR SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 23;
#define weapon_sprt
return global.sprDirectorSlugger;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "curveball";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSlugger);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DirectorSlug")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -14, 11);

///// NTU ASSAULT PISTOL.wep.gml
global.sprAPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprAPistol");
#define weapon_name
return "ASSAULT PISTOL";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 8;
#define weapon_sprt
return global.sprAPistol;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "";
#define weapon_fire
with (instance_create(x, y, Burst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 2;
	time = 2;
	event_perform(ev_alarm, 0);
}

///// NTU ASSAULT SHOTGUN.wep.gml
global.sprAssaultShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprAssaultShotgun");
#define weapon_name
return "ASSAULT SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 23;
#define weapon_sprt
return global.sprAssaultShotgun;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ShotgunBurst")) {
	creator = other.id;
	ammo = 3;
	time = 3;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU BEAM SHOTGUN.wep.gml
global.sprBeamShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBeamShotgun");
#define weapon_name
return "BEAM SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 9;
#define weapon_sprt
return global.sprBeamShotgun;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "drawer";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "BeamShotgunBurst")) {
	creator = other.id;
	ammo = 9;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU BOUNCER SMG.wep.gml
global.sndBouncerFire = mod_script_call("mod", "NTU", "ntu_sound", "sndBouncerFire");
#define weapon_name
return "BOUNCER SMG";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return sprBouncerSMG;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "first reddit requested";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndBouncerFire);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(60) - 30, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet3")) {
	motion_add(__angle + (random(34) - 17) * other.accuracy, 5.1);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 3);

///// NTU BOUNCER SHOTGUN.wep.gml
#define weapon_name
return "BOUNCER SHOTGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 7;
#define weapon_load
return 19;
#define weapon_sprt
return sprBouncerShotgun;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "pop pop diz";
#define weapon_fire
var __angle = gunangle;
sound_play(sndBouncerShotgun);
motion_add(__angle + 180, 2);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet3")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 5.1);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet3")) {
	motion_add(__angle + 10 * other.accuracy + (random(8) - 4) * other.accuracy, 5.1);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet3")) {
	motion_add(__angle + 20 * other.accuracy + (random(8) - 4) * other.accuracy, 5.1);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet3")) {
	motion_add(__angle - 10 * other.accuracy + (random(8) - 4) * other.accuracy, 5.1);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet3")) {
	motion_add(__angle - 20 * other.accuracy + (random(8) - 4) * other.accuracy, 5.1);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet3")) {
	motion_add(__angle - 30 * other.accuracy + (random(8) - 4) * other.accuracy, 5.1);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet3")) {
	motion_add(__angle + 30 * other.accuracy + (random(8) - 4) * other.accuracy, 5.1);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -10, 10);

///// NTU BOUNCER ASSAULT RIFLE.wep.gml
global.sprBouncerAssaultRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprBouncerAssaultRifle");
#define weapon_name
return "BOUNCER ASSAULT RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 3;
#define weapon_load
return 12;
#define weapon_sprt
return global.sprBouncerAssaultRifle;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "bounce shake";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "BouncerBurst")) {
	creator = other.id;
	ammo = 3;
	time = 2;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU HYPER BOUNCER RIFLE.wep.gml
global.sprHyperBouncerRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperBouncerRifle");
#define weapon_name
return "HYPER BOUNCER RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 6;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprHyperBouncerRifle;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "hyper bounce time";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "BouncerBurst")) {
	creator = other.id;
	ammo = 6;
	time = 2;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU BOUNCER FLAK CANNON.wep.gml
global.sprBouncerFlakCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprBouncerFlakCannon");
#define weapon_name
return "BOUNCER FLAK CANNON";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 10;
#define weapon_load
return 22;
#define weapon_sprt
return global.sprBouncerFlakCannon;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "10/bounce";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFlakCannon);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "BouncerFlakBullet")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 5.1);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(7, -32, 4);

///// NTU MORPH-O-PISTOL.wep.gml
global.sprMorphOPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprMorphOPistol");
#define weapon_name
return "MORPH-O-PISTOL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprMorphOPistol;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "sheeps!";
#define weapon_fire
if (!instance_exists(FlameSound)) instance_create(x, y, FlameSound);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Ray")) {
	creator = other.id;
	ammo = 7;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
	explosive = 2;
}

///// NTU SUPER FLAK CANNON.wep.gml
#define weapon_name
return "SUPER FLAK CANNON";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 8;
#define weapon_load
return 64;
#define weapon_sprt
return sprSuperFlakCannon;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "11/10";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSuperFlakCannon);
with (instance_create(x, y, SuperFlakBullet)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 11 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -32, 14);

///// NTU SLEDGEHAMMER.wep.gml
#define weapon_name
return "SLEDGEHAMMER";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 35;
#define weapon_sprt
return sprHammer;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "hammer time";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), Slash)) {
	sprite_index = sprHeavySlash;
	damage = 16;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 12, 1);

///// NTU LIGHTNING SMG.wep.gml
global.sndLightning3 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning3");
global.sndLightning1 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning1");
#define weapon_name
return "LIGHTNING SMG";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 7;
#define weapon_sprt
return sprLightningSMG;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "tornado";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLightning3);
} else sound_play(global.sndLightning1);
with (instance_create(x, y, Lightning)) {
	image_angle = __angle + (random(30) - 15) * other.accuracy;
	team = other.team;
	ammo = 14;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
}
weapon_post(3, -3, 5);

///// NTU FROST PISTOL.wep.gml
global.sprFrostPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprFrostPistol");
global.sndFrostShot1 = mod_script_call("mod", "NTU", "ntu_sound", "sndFrostShot1");
#define weapon_name
return "FROST PISTOL";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 10;
#define weapon_sprt
return global.sprFrostPistol;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "frozen enemies #move while frozen";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndFrostShot1);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FreezeBullet")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -6, 6);

///// NTU FROST MACHINEGUN.wep.gml
global.sprFrostMachinegun = mod_script_call("mod", "NTU", "ntu_sprite", "sprFrostMachinegun");
global.sndFrostShot2 = mod_script_call("mod", "NTU", "ntu_sound", "sndFrostShot2");
#define weapon_name
return "FROST MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 6;
#define weapon_sprt
return global.sprFrostMachinegun;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "freeze!";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndFrostShot2);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FreezeBullet")) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -6, 5);

///// NTU HEAVY REVOLVER.wep.gml
global.sndHeavyRevolver = mod_script_call("mod", "NTU", "ntu_sound", "sndHeavyRevolver");
#define weapon_name
return "HEAVY REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 5;
#define weapon_sprt
return sprHeavyRevolver;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "heavy bullets";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndHeavyRevolver);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, HeavyBullet)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -6, 6);

///// NTU HEAVY MACHINEGUN.wep.gml
global.sprHeavyMachineGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprHeavyMachineGun");
#define weapon_name
return "HEAVY MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 4;
#define weapon_sprt
return global.sprHeavyMachineGun;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "weight";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHeavyMachinegun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, HeavyBullet)) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -6, 5);

///// NTU HEAVY ASSAULT RIFLE.wep.gml
#define weapon_name
return "HEAVY ASSAULT RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 6;
#define weapon_load
return 10;
#define weapon_sprt
return sprHeavyARifle;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "hefty";
#define weapon_fire
with (instance_create(x, y, HeavyBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 3;
	time = 2;
	event_perform(ev_alarm, 0);
}

///// NTU SHORTGUN.wep.gml
global.sprShortgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprShortgun");
#define weapon_name
return "SHORTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 12;
#define weapon_sprt
return global.sprShortgun;
#define weapon_area
return 0;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "thats what she said";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (20) {
	with (instance_create(x, y, Bullet2)) {
		motion_add(__angle + (random(80) - 40) * other.accuracy, 6 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(7, -12, 9);

///// NTU DEAD GLOVE.wep.gml
global.sprDeadGlove = mod_script_call("mod", "NTU", "ntu_sprite", "sprDeadGlove");
#define weapon_name
return "DEAD GLOVE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 23;
#define weapon_sprt
return global.sprDeadGlove;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "cold touch of death";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
ang = __angle;
repeat (3) {
	move_contact_solid(ang, 44);
	instance_create(x, y, Dust);
	with (instance_create(x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), Slash)) {
		ang = other.ang;
		damage = 8;
		motion_add(ang, 2 + 3 * __long_arms);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(ang, 3);
alarm[3] = 4;
speed = -speed * 0.5;
weapon_post(-12, 8, 4);

///// NTU BLOOD GLOVE.wep.gml
global.sprBloodGlove = mod_script_call("mod", "NTU", "ntu_sprite", "sprBloodGlove");
#define weapon_name
return "BLOOD GLOVE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprBloodGlove;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "five point palm#exploding heart technique";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
ang = __angle;
repeat (3) {
	move_contact_solid(ang, 32);
	instance_create(x, y, Dust);
	with (instance_create(x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), BloodSlash)) {
		ang = other.ang;
		damage = 4;
		motion_add(ang, 2 + 3 * __long_arms);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(ang, 3);
move_contact_solid(ang, 12);
alarm[3] = 4;
speed = -speed * 0.5;
weapon_post(-12, 8, 4);

///// NTU MINI FLAME CANNON.wep.gml
global.sprMiniFlameCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprMiniFlameCannon");
#define weapon_name
return "MINI FLAME CANNON";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 25;
#define weapon_sprt
return global.sprMiniFlameCannon;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "flame family";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFiretrap);
sound_play(sndFlakExplode);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "MiniFlameCannonBall")) {
	image_angle = random(360);
	motion_add(__angle + (random(8) - 4) * other.accuracy, 3);
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 2.5);
weapon_post(8, -12, 10);

///// NTU FLAME CANNON.wep.gml
#define weapon_name
return "FLAME CANNON";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 30;
#define weapon_sprt
return sprFlameCannon;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "fireball!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFiretrap);
sound_play(sndSuperFlakExplode);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FlameCannonBall")) {
	image_angle = random(360);
	motion_add(__angle + (random(8) - 4) * other.accuracy, 3);
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 2);
weapon_post(8, -6, 7);

///// NTU INFINITY REVOLVER.wep.gml
global.sprInfinityPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprInfinityPistol");
#define weapon_name
return "INFINITY REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 5;
#define weapon_sprt
return global.sprInfinityPistol;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "trusty new revolver";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(3, -6, 5);

///// NTU INFINITY MACHINEGUN.wep.gml
global.sprInfinityMachinegun = mod_script_call("mod", "NTU", "ntu_sprite", "sprInfinityMachinegun");
#define weapon_name
return "INFINITY MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 0;
#define weapon_load
return 4;
#define weapon_sprt
return global.sprInfinityMachinegun;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "endless";
#define weapon_fire
var __angle = gunangle;
sound_play(sndMachinegun);
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(3, -7, 4);

///// NTU ULTRA REVOLVER.wep.gml
#define weapon_name
return "ULTRA REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 3;
#define weapon_sprt
return sprUltraRevolver;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "radical";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 7)) exit;
var __angle = gunangle;
sound_play(sndUltraPistol);
repeat (2) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet4")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(3, -7, 5);

///// NTU ULTRA SHOTGUN.wep.gml
#define weapon_name
return "ULTRA SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 12;
#define weapon_sprt
return sprUltraShotgun;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "radiculous";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 17)) exit;
var __angle = gunangle;
sound_play(sndUltraShotgun);
repeat (9) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet5")) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 14 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(7, -13, 9);

///// NTU ULTRA CROSSBOW.wep.gml
#define weapon_name
return "ULTRA CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 17;
#define weapon_sprt
return sprUltraCrossbow;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "green bow";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 15)) exit;
var __angle = gunangle;
sound_play(sndUltraCrossbow);
with (instance_create(x, y, UltraBolt)) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -41, 5);

///// NTU ULTRA LASER PISTOL.wep.gml
#define weapon_name
return "ULTRA LASER PISTOL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 7;
#define weapon_sprt
return sprUltraLaserGun;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "ultraaaaaaa";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 18)) exit;
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndUltraLaserUpg);
} else sound_play(sndUltraLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle + (random(2) - 1) * other.accuracy;
	team = other.team;
	image_yscale += 0.2;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
with (instance_create(x, y, Laser)) {
	image_angle = __angle + random(2) - 1 + 7 * other.accuracy;
	team = other.team;
	image_yscale += 0.2;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
with (instance_create(x, y, Laser)) {
	image_angle = __angle + random(2) - 1 + 14 * other.accuracy;
	team = other.team;
	image_yscale += 0.2;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
with (instance_create(x, y, Laser)) {
	image_angle = __angle + random(2) - 1 - 7 * other.accuracy;
	team = other.team;
	image_yscale += 0.2;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
with (instance_create(x, y, Laser)) {
	image_angle = __angle + random(2) - 1 - 14 * other.accuracy;
	team = other.team;
	image_yscale += 0.2;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
weapon_post(9, -20, 12);

///// NTU ULTRA SHOVEL.wep.gml
#define weapon_name
return "ULTRA SHOVEL";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 14;
#define weapon_sprt
return sprUltraShovel;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "dig you to death";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 17)) exit;
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndUltraShovel);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), Slash)) {
	damage = 30;
	sprite_index = sprUltraSlash;
	motion_add(__angle, 3 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x + lengthdir_x(__long_arms * 15, __angle + 60 * accuracy), y + lengthdir_y(__long_arms * 15, __angle + 60 * accuracy), Slash)) {
	damage = 30;
	sprite_index = sprUltraSlash;
	motion_add(__angle + 60 * other.accuracy, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x + lengthdir_x(__long_arms * 15, __angle - 60 * accuracy), y + lengthdir_y(__long_arms * 15, __angle - 60 * accuracy), Slash)) {
	damage = 30;
	sprite_index = sprUltraSlash;
	motion_add(__angle - 60 * other.accuracy, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 26, 5);

///// NTU AUTO FLAK CANNON.wep.gml
global.sprAutoFlakCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprAutoFlakCannon");
#define weapon_name
return "AUTO FLAK CANNON";
#define weapon_type
return 2;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 13;
#define weapon_sprt
return global.sprAutoFlakCannon;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "you asked for it";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFlakCannon);
with (instance_create(x, y, FlakBullet)) {
	motion_add(__angle + (random(20) - 10) * other.accuracy, 11 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(7, -32, 4);

///// NTU ULTRA LIGHTNING RIFLE.wep.gml
global.sprUltraLightningRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraLightningRifle");
global.sndLightning2 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning2");
global.sndLightning3 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning3");
global.sndLightning1 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning1");
#define weapon_name
return "ULTRA LIGHTNING RIFLE";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprUltraLightningRifle;
#define weapon_area
return 21;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "ultranado";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 19)) exit;
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(choose(global.sndLightning2, global.sndLightning3));
} else sound_play(global.sndLightning1);
sound_play(sndUltraLaser);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraLightning")) {
	image_angle = __angle + (random(6) - 3) * other.accuracy;
	team = other.team;
	ammo = 60;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
}
weapon_post(10, -12, 10);

///// NTU ULTRA FLAMETHROWER.wep.gml
global.sprUltraFlamethrower = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraFlamethrower");
#define weapon_name
return "ULTRA FLAMETHROWER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprUltraFlamethrower;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "ultra breath";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 8)) exit;
if (!instance_exists(DragonSound)) instance_create(x, y, DragonSound);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraFlameBurst")) {
	creator = other.id;
	ammo = 5;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU FLARE SHOTGUN.wep.gml
global.sprFlareShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprFlareShotgun");
#define weapon_name
return "FLARE SHOTGUN";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 32;
#define weapon_sprt
return global.sprFlareShotgun;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "4 flares for the cost of 3";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFlare);
repeat (4) {
	with (instance_create(x, y, Flare)) {
		sticky = 1;
		motion_add(__angle + (random(60) - 30) * other.accuracy, 9);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(7, -12, 7);

///// NTU FROST GLOVE.wep.gml
global.sprFrostGlove = mod_script_call("mod", "NTU", "ntu_sprite", "sprFrostGlove");
#define weapon_name
return "FROST GLOVE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 24;
#define weapon_sprt
return global.sprFrostGlove;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "cold hands";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
ang = __angle;
repeat (3) {
	move_contact_solid(ang, 44);
	instance_create(x, y, Dust);
	with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), "FrostSlash")) {
		ang = other.ang;
		damage = 12;
		motion_add(ang, 2 + 3 * __long_arms);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
alarm[3] = 4;
speed = -speed * 0.5;
weapon_post(-12, 8, 4);

///// NTU LIGHTNING HAMMER.wep.gml
global.sndLaserSwordUpg = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSwordUpg");
global.sndLaserSword = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSword");
#define weapon_name
return "LIGHTNING HAMMER";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 32;
#define weapon_sprt
return sprLightningHammer;
#define weapon_area
return 13;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "shocking";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLaserSwordUpg);
} else sound_play(global.sndLaserSword);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), LightningSlash)) {
	if (__laser_brain) {
		damage = 15 + (__laser_brain - 1) * 2;
	} else damage = 10;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 12, 2);

///// NTU LIGHTNING SHOVEL.wep.gml
global.sprLightningShovel = mod_script_call("mod", "NTU", "ntu_sprite", "sprLightningShovel");
global.sndLaserSwordUpg = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSwordUpg");
global.sndLaserSword = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSword");
#define weapon_name
return "LIGHTNING SHOVEL";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 48;
#define weapon_sprt
return global.sprLightningShovel;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "storm digger";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLaserSwordUpg);
} else sound_play(global.sndLaserSword);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), LightningSlash)) {
	if (__laser_brain) {
		damage = 17 + (__laser_brain - 1) * 2;
	} else damage = 12;
	motion_add(__angle, 3 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x + lengthdir_x(__long_arms * 15, __angle + 60 * accuracy), y + lengthdir_y(__long_arms * 15, __angle + 60 * accuracy), LightningSlash)) {
	if (__laser_brain) {
		damage = 17 + (__laser_brain - 1) * 2;
	} else damage = 12;
	motion_add(__angle + 60 * other.accuracy, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x + lengthdir_x(__long_arms * 15, __angle - 60 * accuracy), y + lengthdir_y(__long_arms * 15, __angle - 60 * accuracy), LightningSlash)) {
	if (__laser_brain) {
		damage = 17 + (__laser_brain - 1) * 2;
	} else damage = 12;
	motion_add(__angle - 60 * other.accuracy, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 24, 1);

///// NTU ENERGY JACKHAMMER.wep.gml
global.sprEnergyJackHammer = mod_script_call("mod", "NTU", "ntu_sprite", "sprEnergyJackHammer");
#define weapon_name
return "ENERGY JACKHAMMER";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return global.sprEnergyJackHammer;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapMotorized;
#define weapon_text
return "electronic drilling";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergyJackhammerBurst")) {
	creator = other.id;
	ammo = 4;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU ENERGY SWORD GUN.wep.gml
global.sprEnergySwordGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprEnergySwordGun");
#define weapon_name
return "ENERGY SWORD GUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 25;
#define weapon_sprt
return global.sprEnergySwordGun;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "bolt marrow, laser brain, shotgun shoulders and long arms!?";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaBigUpg);
} else sound_play(sndPlasmaBig);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergySwordBullet")) {
	motion_add(__angle + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -6, 6);

///// NTU SUPER ENERGY SWORD GUN.wep.gml
global.sprSuperEnergySwordGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperEnergySwordGun");
global.sndLightningPlasma2 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightningPlasma2");
#define weapon_name
return "SUPER ENERGY SWORD GUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 18;
#define weapon_load
return 50;
#define weapon_sprt
return global.sprSuperEnergySwordGun;
#define weapon_area
return 22;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "this is nuts";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndLightningPlasma2);
motion_add(__angle + 180, 16);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergySwordBullet")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergySwordBullet")) {
	motion_add(__angle + 10 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergySwordBullet")) {
	motion_add(__angle + 20 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergySwordBullet")) {
	motion_add(__angle - 10 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergySwordBullet")) {
	motion_add(__angle - 20 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(10, -62, 16);

///// NTU TOXICTHROWER.wep.gml
#define weapon_name
return "TOXICTHROWER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 6;
#define weapon_sprt
return sprToxicThrower;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "gas gas gas";
#define weapon_fire
if (!instance_exists(/*!*/ToxicSound)) instance_create(x, y, /*!*/ToxicSound);
with (instance_create(x, y, ToxicBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 9;
	time = 1;
	event_perform(ev_alarm, 0);
}

///// NTU TOXIC DRAGON.wep.gml
global.sprToxicDragon = mod_script_call("mod", "NTU", "ntu_sprite", "sprToxicDragon");
#define weapon_name
return "TOXIC DRAGON";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprToxicDragon;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "stinky breath";
#define weapon_fire
if (!instance_exists(/*!*/ToxicDragonSound)) instance_create(x, y, /*!*/ToxicDragonSound);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ToxicDragonBurst")) {
	creator = other.id;
	ammo = 5;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU HEAVY HYPER RIFLE.wep.gml
global.sprHeavyHyperRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprHeavyHyperRifle");
#define weapon_name
return "HEAVY HYPER RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 10;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprHeavyHyperRifle;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "heavy burden";
#define weapon_fire
with (instance_create(x, y, HeavyBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 5;
	time = 1;
	event_perform(ev_alarm, 0);
}

///// NTU HYPER BLOOD LAUNCHER.wep.gml
global.sprHyperBloodLauncher = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperBloodLauncher");
#define weapon_name
return "HYPER BLOOD LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 9;
#define weapon_sprt
return global.sprHyperBloodLauncher;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "born in blood";
#define weapon_fire
var __angle = gunangle;
sound_play(sndBloodLauncher);
sound_play(sndHyperLauncher);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "HyperBloodGrenade")) {
	direction = __angle + (random(4) - 2) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -20, 4);

///// NTU AUTO SPLINTER GUN.wep.gml
global.sprAutoSplinterGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprAutoSplinterGun");
#define weapon_name
return "AUTO SPLINTER GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 10;
#define weapon_sprt
return global.sprAutoSplinterGun;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "225 splinters per minute";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSplinterGun);
with (instance_create(x, y, Splinter)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 20 + random(4));
	image_angle = direction;
	team = other.team;
	creator = other;
}
repeat (2) {
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(24) - 12) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(14) - 7) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(-3, -16, 3);

///// NTU DOUBLE MACHINEGUN.wep.gml
global.sprDoubleMachinegun = mod_script_call("mod", "NTU", "ntu_sprite", "sprDoubleMachinegun");
#define weapon_name
return "DOUBLE MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 5;
#define weapon_sprt
return global.sprDoubleMachinegun;
#define weapon_area
return 2;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "splitgun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndMachinegun);
repeat (3) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(70) - 35, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + 8 * other.accuracy + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle - 8 * other.accuracy + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(3, -7, 3);

///// NTU LOLLIPOP.wep.gml
global.sprLollipop = mod_script_call("mod", "NTU", "ntu_sprite", "sprLollipop");
#define weapon_name
return "LOLLIPOP";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 42;
#define weapon_sprt
return global.sprLollipop;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "lick it";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), "BigSlash")) {
	damage = 26;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-5, 14, 3);

///// NTU SMART GUN.wep.gml
#define weapon_name
return "SMART GUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return sprSmartGun;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "guns that think";
#define weapon_fire
var __angle = gunangle;
var __eagle_eyes = mod_script_call("mod", "NTU", "ntu_wep_eagle_eyes");
sound_play(sndSmartgun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
	target = instance_nearest(x + lengthdir_x(80, direction), y + lengthdir_y(80, direction), enemy);
	if (instance_exists(target)) if (!collision_line(x, y, target.x, target.y, Wall, false, true)) if (direction < point_direction(x, y, target.x, target.y) + 30 + 30 * __eagle_eyes) direction = point_direction(x, y, target.x, target.y) + random(12) - 6;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -7, 4);

///// NTU SMART MINIGUN.wep.gml
global.sprSmartMinigun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSmartMinigun");
#define weapon_name
return "SMART MINIGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 1;
#define weapon_sprt
return global.sprSmartMinigun;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "guns that think FAST";
#define weapon_fire
var __angle = gunangle;
var __eagle_eyes = mod_script_call("mod", "NTU", "ntu_wep_eagle_eyes");
sound_play(sndMinigun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(20) - 10) * other.accuracy, 16);
	target = instance_nearest(x + lengthdir_x(80, direction), y + lengthdir_y(80, direction), enemy);
	if (instance_exists(target)) if (!collision_line(x, y, target.x, target.y, Wall, false, true)) if (direction < point_direction(x, y, target.x, target.y) + 30 + 30 * __eagle_eyes) direction = point_direction(x, y, target.x, target.y) + random(20) - 10;
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 0.6);
weapon_post(4, -7, 4);

///// NTU MEGA LASER RIFLE.wep.gml
global.sprMegaLaserRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprMegaLaserRifle");
#define weapon_name
return "MEGA LASER RIFLE";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 22;
#define weapon_sprt
return global.sprMegaLaserRifle;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "SHOOP DA WHOOP";
#define weapon_fire
var __angle = gunangle;
sound_play(sndLaserUpg);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), "MegaLaser")) {
	image_angle = __angle + (random(8) - 4) * other.accuracy;
	team = other.team;
	event_perform(ev_alarm, 0);
}
motion_add(__angle + 180, 0.6);
weapon_post(6, -4, 4);

///// NTU SPLINTER CROSSBOW.wep.gml
global.sprSplinterCrossbow = mod_script_call("mod", "NTU", "ntu_sprite", "sprSplinterCrossbow");
#define weapon_name
return "SPLINTER CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 29;
#define weapon_sprt
return global.sprSplinterCrossbow;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "best of both worlds";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
sound_play(sndSplinterGun);
with (instance_create(x + lengthdir_x(2, __angle), y + lengthdir_y(2, __angle), Bolt)) {
	motion_add(__angle, 25);
	image_angle = direction;
	team = other.team;
	creator = other;
}
repeat (2) {
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(20) - 10) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(10) - 5) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(5, -40, 5);

///// NTU HYPER PLASMA CANNON.wep.gml
global.sprHyperPlasmaCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperPlasmaCannon");
global.sndLightningPlasma2 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightningPlasma2");
#define weapon_name
return "HYPER PLASMA CANNON";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 26;
#define weapon_sprt
return global.sprHyperPlasmaCannon;
#define weapon_area
return 14;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "bubbles";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndLightningPlasma2);
sound_play(sndHyperLauncher);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "HyperPlasmaCannon")) {
	direction = __angle + (random(4) - 2) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -20, 5);

///// NTU BULLET SHOTGUN.wep.gml
global.sprBulletShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBulletShotgun");
#define weapon_name
return "BULLET SHOTGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 6;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprBulletShotgun;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (7) {
	with (instance_create(x, y, Bullet1)) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -12, 8);

///// NTU BLOOD BULLET SHOTGUN.wep.gml
global.sprBloodBulletShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBloodBulletShotgun");
#define weapon_name
return "BLOOD BULLET SHOTGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 5;
#define weapon_load
return 16;
#define weapon_sprt
return global.sprBloodBulletShotgun;
#define weapon_area
return 2;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "spit blood";
#define weapon_fire
var __angle = gunangle;
sound_play_gun(sndShotgun, 0.2, 0.2);
repeat (7) {
	with (mod_script_call("mod", "NTU Blood Bullet", "scr_create", x, y)) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -10, 6);

///// NTU LINE GRENADE.wep.gml
global.sprLineNader = mod_script_call("mod", "NTU", "ntu_sprite", "sprLineNader");
#define weapon_name
return "LINE GRENADE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 15;
#define weapon_sprt
return global.sprLineNader;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "dangerous bliss";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHyperLauncher);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "LineGrenade")) {
	direction = __angle + (random(2) - 1) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -30, 8);

///// NTU HYPER LIGHTNING LAUNCHER.wep.gml
global.sprLightningHyperRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprLightningHyperRifle");
global.sndHyperLightning = mod_script_call("mod", "NTU", "ntu_sound", "sndHyperLightning");
#define weapon_name
return "HYPER LIGHTNING LAUNCHER";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 26;
#define weapon_sprt
return global.sprLightningHyperRifle;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "thunder lightning ever so frightning";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndHyperLightning);
sound_play(sndHyperLauncher);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "HyperLightning")) {
	direction = __angle + (random(4) - 2) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -20, 5);

///// NTU LINE OF FIRE.wep.gml
global.sprLineFire = mod_script_call("mod", "NTU", "ntu_sprite", "sprLineFire");
#define weapon_name
return "LINE OF FIRE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprLineFire;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "don't get in the line of fire";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "LineFire")) {
	direction = __angle + (random(2) - 1) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -30, 5);

///// NTU LINE OF TOXIC.wep.gml
global.sprLineToxic = mod_script_call("mod", "NTU", "ntu_sprite", "sprLineToxic");
#define weapon_name
return "LINE OF TOXIC";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 21;
#define weapon_sprt
return global.sprLineToxic;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "farts";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "LineToxic")) {
	direction = __angle + (random(2) - 1) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -30, 5);

///// NTU FROST SHOTGUN.wep.gml
global.sprFrostShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprFrostShotgun");
#define weapon_name
return "FROST SHOTGUN";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 10;
#define weapon_load
return 24;
#define weapon_sprt
return global.sprFrostShotgun;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "breezer";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (7) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FreezeBullet")) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(8, -14, 9);

///// NTU PISTOLE.wep.gml
global.sprPistole = mod_script_call("mod", "NTU", "ntu_sprite", "sprPistole");
#define weapon_name
return "PISTOLE";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 30;
#define weapon_sprt
return global.sprPistole;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "shotgun pistol yo";
#define weapon_fire
for (var ammo = 2; ammo > 0; ammo -= 1) if (instance_exists(self)) {
	repeat (5) with (instance_create(x, y, Bullet2)) {
		motion_add(other.gunangle + random_range(-20, 20) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	weapon_post(6, 12, 8);
	wait 3;
}

///// NTU HYPER ASSAULT SHOTGUN.wep.gml
global.sprHyperBurstShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperBurstShotgun");
#define weapon_name
return "HYPER ASSAULT SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprHyperBurstShotgun;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "hyper everything";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ShotgunBurst")) {
	creator = other.id;
	ammo = 5;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU HYPER ASSAULT SLUGGER.wep.gml
global.sprHyperBurstSlugger = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperBurstSlugger");
#define weapon_name
return "HYPER ASSAULT SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprHyperBurstSlugger;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "where's your gatling slugger now?!";
#define weapon_fire
with (instance_create(x, y, SlugBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 5;
	time = 1;
	event_perform(ev_alarm, 0);
}

///// NTU INFINITY SHOTGUN.wep.gml
global.sprInfinityShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprInfinityShotgun");
#define weapon_name
return "INFINITY SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 19;
#define weapon_sprt
return global.sprInfinityShotgun;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "eternal";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (7) {
	with (instance_create(x, y, Bullet2)) {
		motion_add(__angle + (random(36) - 18) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(7, -14, 10);

///// NTU INFINITY CROSSBOW.wep.gml
global.sprInfinityCrossbow = mod_script_call("mod", "NTU", "ntu_sprite", "sprInfinityCrossbow");
#define weapon_name
return "INFINITY CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 24;
#define weapon_sprt
return global.sprInfinityCrossbow;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "forever";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (instance_create(x, y, Bolt)) {
	motion_add(__angle, 25);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -40, 6);

///// NTU INCINERATOR.wep.gml
#define weapon_name
return "INCINERATOR";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 2;
#define weapon_sprt
return sprIncinerator;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "firestarter";
#define weapon_fire
var __angle = gunangle;
sound_play(sndIncinerator);
repeat (3) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(70) - 35, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
	motion_add(__angle + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
	motion_add(__angle + 15 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
	motion_add(__angle - 15 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -9, 5);

///// NTU FLAME POP GUN.wep.gml
global.sprFlamePopGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprFlamePopGun");
#define weapon_name
return "FLAME POP GUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprFlamePopGun;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "thy shall burn";
#define weapon_fire
var __angle = gunangle;
sound_play(sndMachinegun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
	motion_add(__angle + (random(16) - 8) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(3, -7, 3);

///// NTU MINI INCINERATOR.wep.gml
global.sprMiniIncinerator = mod_script_call("mod", "NTU", "ntu_sprite", "sprMiniIncinerator");
#define weapon_name
return "MINI INCINERATOR";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprMiniIncinerator;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "junior firestarter";
#define weapon_fire
var __angle = gunangle;
sound_play(sndIncinerator);
repeat (2) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(70) - 35, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
	motion_add(__angle + 6 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
	motion_add(__angle - 6 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -8, 4);

///// NTU FLAME SHOTGUN.wep.gml
#define weapon_name
return "FLAME SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 19;
#define weapon_sprt
return sprFlameShotgun;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "burn baby burn";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFireShotgun);
repeat (6) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -13, 9);

///// NTU DOUBLE FLAME SHOTGUN.wep.gml
#define weapon_name
return "DOUBLE FLAME SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 34;
#define weapon_sprt
return sprDoubleFlameShotgun;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "double burn, double furn";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDoubleFireShotgun);
repeat (14) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
		motion_add(__angle + (random(50) - 30) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 2);
weapon_post(7, -15, 16);

///// NTU AUTO FLAME SHOTGUN.wep.gml
#define weapon_name
return "AUTO FLAME SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return sprAutoFlameShotgun;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "feel the burn";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFireShotgun);
repeat (5) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
		motion_add(__angle + (random(30) - 15) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(5, -13, 9);

///// NTU QUADRUPLE FLAME SHOTGUN.wep.gml
global.sprQuadrupleFlameShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprQuadrupleFlameShotgun");
#define weapon_name
return "QUADRUPLE FLAME SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 44;
#define weapon_sprt
return global.sprQuadrupleFlameShotgun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDoubleFireShotgun);
sound_play(sndFireShotgun);
repeat (28) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
		motion_add(__angle + (random(50) - 30) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 2);
weapon_post(12, -31, 21);

///// NTU FLAME ERASER.wep.gml
global.sprFlameEraser = mod_script_call("mod", "NTU", "ntu_sprite", "sprFlameEraser");
#define weapon_name
return "FLAME ERASER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprFlameEraser;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "";
#define weapon_fire
sound_play(sndEraser);
sound_play(sndFireShotgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FlameEraserBurst")) {
	mox = mouse_x;
	moy = mouse_y;
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(6, -9, 6);

///// NTU SUPER FLAME ERASER.wep.gml
global.sprSuperFlameEraser = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperFlameEraser");
#define weapon_name
return "SUPER FLAME ERASER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 15;
#define weapon_load
return 60;
#define weapon_sprt
return global.sprSuperFlameEraser;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "vaporize";
#define weapon_fire
var __angle = gunangle;
var __dist = mod_script_call("mod", "NTU", "ntu_wep_dist");
motion_add(__angle + 180, 16);
sound_play(sndEraser);
sound_play(sndDoubleFireShotgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FlameEraserBurst")) {
	mox = mouse_x;
	moy = mouse_y;
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FlameEraserBurst")) {
	mox = x + lengthdir_x(__dist, __angle + 10 * other.accuracy + (random(4) - 2) * other.accuracy);
	moy = y + lengthdir_y(__dist, __angle + 10 * other.accuracy + (random(4) - 2) * other.accuracy);
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FlameEraserBurst")) {
	mox = x + lengthdir_x(__dist, __angle + 20 * other.accuracy + (random(4) - 2) * other.accuracy);
	moy = y + lengthdir_y(__dist, __angle + 20 * other.accuracy + (random(4) - 2) * other.accuracy);
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FlameEraserBurst")) {
	mox = x + lengthdir_x(__dist, __angle - 10 * other.accuracy + (random(4) - 2) * other.accuracy);
	moy = y + lengthdir_y(__dist, __angle - 10 * other.accuracy + (random(4) - 2) * other.accuracy);
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FlameEraserBurst")) {
	mox = x + lengthdir_x(__dist, __angle - 20 * other.accuracy + (random(4) - 2) * other.accuracy);
	moy = y + lengthdir_y(__dist, __angle - 20 * other.accuracy + (random(4) - 2) * other.accuracy);
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(9, -60, 15);

///// NTU MEGA INCINERATOR.wep.gml
global.sprMegaIncinerator = mod_script_call("mod", "NTU", "ntu_sprite", "sprMegaIncinerator");
#define weapon_name
return "MEGA INCINERATOR";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 2;
#define weapon_sprt
return global.sprMegaIncinerator;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "senior firestarter";
#define weapon_fire
var __angle = gunangle;
sound_play(sndIncinerator);
repeat (4) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(70) - 35, 4 + random(3));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
	motion_add(__angle + 6 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
	motion_add(__angle - 6 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
	motion_add(__angle + 18 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
	motion_add(__angle - 18 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -11, 7);

///// NTU QUADRUPLE POP GUN.wep.gml
global.sprQuadruplePopGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprQuadruplePopGun");
#define weapon_name
return "QUADRUPLE POP GUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 3;
#define weapon_load
return 2;
#define weapon_sprt
return global.sprQuadruplePopGun;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "omg";
#define weapon_fire
var __angle = gunangle;
sound_play(sndQuadMachinegun);
repeat (3) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(70) - 35, 4 + random(3));
with (instance_create(x, y, Bullet2)) {
	motion_add(__angle + 6 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet2)) {
	motion_add(__angle - 6 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet2)) {
	motion_add(__angle + 18 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bullet2)) {
	motion_add(__angle - 18 * other.accuracy + (random(6) - 3) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(7, -9, 6);

///// NTU LINE OF BLOOD.wep.gml
global.sprLineBloodNader = mod_script_call("mod", "NTU", "ntu_sprite", "sprLineBloodNader");
#define weapon_name
return "LINE OF BLOOD";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 16;
#define weapon_sprt
return global.sprLineBloodNader;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "it's a bloody blood bath";
#define weapon_fire
var __angle = gunangle;
sound_play(sndBloodLauncher);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "LineBlood")) {
	direction = __angle + (random(2) - 1) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -30, 8);

///// NTU EXPLOSIVE BOW.wep.gml
global.sprExplosiveBow = mod_script_call("mod", "NTU", "ntu_sprite", "sprExplosiveBow");
#define weapon_name
return "EXPLOSIVE BOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 29;
#define weapon_sprt
return global.sprExplosiveBow;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "the year of the bow";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ExplosiveBolt")) {
	motion_add(__angle, 22);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -40, 5);

///// NTU SLASH SHOTGUN.wep.gml
global.sprSlashShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSlashShotgun");
#define weapon_name
return "SLASH SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 22;
#define weapon_sprt
return global.sprSlashShotgun;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "shoot them out of there cover";
#define weapon_fire
var __angle = gunangle;
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SlashShotgunPrep")) {
	sound_play(sndSlugger);
	motion_add(__angle + (random(6) - 3) * other.accuracy, 10 + random(2));
	image_angle = direction;
	rate = 1;
	Direction = __angle;
	creator = other.id;
	ammo = 10;
	totalammo = ammo;
	time = 1;
	team = other.team;
	creator = other;
}
weapon_post(8, -14, 10);

///// NTU SPIRAL SLASH SHOTGUN.wep.gml
global.sprSpiralSlashShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSpiralSlashShotgun");
#define weapon_name
return "SPIRAL SLASH SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 28;
#define weapon_sprt
return global.sprSpiralSlashShotgun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "spinning";
#define weapon_fire
var __angle = gunangle;
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SlashShotgunPrep")) {
	sound_play(sndSlugger);
	motion_add(__angle + (random(6) - 3) * other.accuracy, 10 + random(2));
	image_angle = direction;
	rate = 4;
	Direction = __angle;
	creator = other.id;
	ammo = 40;
	totalammo = ammo;
	time = 1;
	team = other.team;
	creator = other;
}
weapon_post(10, -16, 12);

///// NTU SLASH ERASER.wep.gml
global.sprSlashEraser = mod_script_call("mod", "NTU", "ntu_sprite", "sprSlashEraser");
#define weapon_name
return "SLASH ERASER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 10;
#define weapon_load
return 40;
#define weapon_sprt
return global.sprSlashEraser;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "7 erasers for the cost of 5";
#define weapon_fire
var __angle = gunangle;
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SlashEraserPrep")) {
	sound_play(sndEraser);
	motion_add(__angle + (random(6) - 3) * other.accuracy, 10 + random(2));
	image_angle = direction;
	rate = 1;
	Direction = __angle;
	creator = other.id;
	ammo = 7;
	totalammo = ammo;
	time = 2;
	team = other.team;
	creator = other;
}
weapon_post(9, -15, 11);

///// NTU SUPER SPIRAL SLASH SHOTGUN.wep.gml
global.sprSuperSpiralSlashShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperSpiralSlashShotgun");
#define weapon_name
return "SUPER SPIRAL SLASH SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 11;
#define weapon_load
return 60;
#define weapon_sprt
return global.sprSuperSpiralSlashShotgun;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "vomit";
#define weapon_fire
var __angle = gunangle;
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SlashShotgunPrep")) {
	sound_play(sndSlugger);
	motion_add(__angle + (random(6) - 3) * other.accuracy, 10 + random(2));
	image_angle = direction;
	rate = 4;
	Direction = __angle;
	creator = other.id;
	ammo = 120;
	totalammo = ammo;
	time = 1;
	team = other.team;
	creator = other;
}
weapon_post(10, -16, 12);

///// NTU DIRECTOR SHOTGUN.wep.gml
global.sprDirectorShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprDirectorShotgun");
#define weapon_name
return "DIRECTOR SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 18;
#define weapon_sprt
return global.sprDirectorShotgun;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "click, click again to change direction";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (7) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet7")) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 9 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -13, 8);

///// NTU DOUBLE DIRECTOR SHOTGUN.wep.gml
global.sprDoubleDirectorShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprDoubleDirectorShotgun");
#define weapon_name
return "DOUBLE DIRECTOR SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 34;
#define weapon_sprt
return global.sprDoubleDirectorShotgun;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "I changed my mind";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDoubleShotgun);
repeat (14) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet7")) {
		motion_add(__angle + (random(50) - 30) * other.accuracy, 9 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 2);
weapon_post(8, -15, 17);

///// NTU QUADRUPLE DIRECTOR SHOTGUN.wep.gml
global.sprQuadrupleDirectorShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprQuadrupleDirectorShotgun");
#define weapon_name
return "QUADRUPLE DIRECTOR SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 43;
#define weapon_sprt
return global.sprQuadrupleDirectorShotgun;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "one direction";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDoubleShotgun);
sound_play(sndShotgun);
repeat (28) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet7")) {
		motion_add(__angle + (random(50) - 30) * other.accuracy, 9 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 2);
weapon_post(12, -30, 21);

///// NTU DIRECTOR RIFLE.wep.gml
global.sprDirectorRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprDirectorRifle");
#define weapon_name
return "DIRECTOR RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 18;
#define weapon_sprt
return global.sprDirectorRifle;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "click to change direction!";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DirectorBurst")) {
	creator = other.id;
	ammo = 5;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU FLAME DIRECTOR SHOTGUN.wep.gml
global.sprFlameDirectorShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprFlameDirectorShotgun");
#define weapon_name
return "FLAME DIRECTOR SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprFlameDirectorShotgun;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFireShotgun);
repeat (6) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet9")) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 8 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -13, 9);

///// NTU DOUBLE FLAME DIRECTOR SHOTGUN.wep.gml
global.sprDoubleFlameDirectorShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprDoubleFlameDirectorShotgun");
#define weapon_name
return "DOUBLE FLAME DIRECTOR SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 35;
#define weapon_sprt
return global.sprDoubleFlameDirectorShotgun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "two directions";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDoubleFireShotgun);
repeat (14) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet9")) {
		motion_add(__angle + (random(50) - 30) * other.accuracy, 8 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 2);
weapon_post(7, -15, 16);

///// NTU QUADRUPLE FLAME DIRECTOR SHOTGUN.wep.gml
global.sprQuadrupleFlameDirectorShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprQuadrupleFlameDirectorShotgun");
#define weapon_name
return "QUADRUPLE FLAME DIRECTOR SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 45;
#define weapon_sprt
return global.sprQuadrupleFlameDirectorShotgun;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "condensed fire";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFlakCannon);
sound_play(sndDoubleFireShotgun);
repeat (28) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet9")) {
		motion_add(__angle + (random(50) - 30) * other.accuracy, 8 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 2);
weapon_post(12, -31, 21);

///// NTU TIMETHROWER.wep.gml
global.sprTimeThrower = mod_script_call("mod", "NTU", "ntu_sprite", "sprTimeThrower");
#define weapon_name
return "TIMETHROWER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 6;
#define weapon_sprt
return global.sprTimeThrower;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "time is ticking";
#define weapon_fire
if (!instance_exists(/*!*/TimeThrowerBurst)) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "TimeThrowerBurst")) {
		creator = other.id;
		time = 1;
		team = other.team;
		event_perform(ev_alarm, 0);
	}
}

///// NTU HEAVY ASSAULT PISTOL.wep.gml
global.sprHeavyAPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprHeavyAPistol");
#define weapon_name
return "HEAVY ASSAULT PISTOL";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 10;
#define weapon_sprt
return global.sprHeavyAPistol;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "fan of heavy bullets <3";
#define weapon_fire
with (instance_create(x, y, HeavyBurst)) {
	accuracy = other.accuracy;
	creator = other.id;
	ammo = 2;
	time = 2;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU AUTO FLARE GUN.wep.gml
global.sprAutoFlareGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprAutoFlareGun");
#define weapon_name
return "AUTO FLARE GUN";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprAutoFlareGun;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFlare);
with (instance_create(x, y, Flare)) {
	sticky = 0;
	motion_add(__angle + (random(24) - 12) * other.accuracy, 9);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -9, 5);

///// NTU AUTO FLARE SHOTGUN.wep.gml
global.sprAutoFlareShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprAutoFlareShotgun");
#define weapon_name
return "AUTO FLARE SHOTGUN";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 3;
#define weapon_load
return 19;
#define weapon_sprt
return global.sprAutoFlareShotgun;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "signal in your face";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFlare);
repeat (4) {
	with (instance_create(x, y, Flare)) {
		sticky = 1;
		motion_add(__angle + (random(70) - 35) * other.accuracy, 9);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -11, 7);

///// NTU CAR THROWER.wep.gml
global.sprCarThrower = mod_script_call("mod", "NTU", "ntu_sprite", "sprCarThrower");
#define weapon_name
return "CAR THROWER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 32;
#define weapon_sprt
return global.sprCarThrower;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapMotorized;
#define weapon_text
return "amputated arm";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSnowBotThrow);
with (instance_create(x, y, CarThrow)) {
	team = other.team;
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
}
weapon_post(-7, 7, 12);

///// NTU DIRECTOR ERASER.wep.gml
global.sprDirectorEraser = mod_script_call("mod", "NTU", "ntu_sprite", "sprDirectorEraser");
#define weapon_name
return "DIRECTOR ERASER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprDirectorEraser;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "directing";
#define weapon_fire
sound_play(sndEraser);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DirectorEraserBurst")) {
	mox = mouse_x;
	moy = mouse_y;
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(6, -8, 5);

///// NTU FLAME DIRECTOR ERASER.wep.gml
global.sprFlameDirectorEraser = mod_script_call("mod", "NTU", "ntu_sprite", "sprFlameDirectorEraser");
#define weapon_name
return "FLAME DIRECTOR ERASER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 22;
#define weapon_sprt
return global.sprFlameDirectorEraser;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "redirecting";
#define weapon_fire
sound_play(sndEraser);
sound_play(sndFireShotgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FlameDirectorEraserBurst")) {
	mox = mouse_x;
	moy = mouse_y;
	creator = other.id;
	ammo = 16;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(6, -8, 5);

///// NTU SEEKER PISTOL.wep.gml
#define weapon_name
return "SEEKER PISTOL";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 24;
#define weapon_sprt
return sprSeekerPistol;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "eagle eyes makes them cleverer";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSeekerPistol);
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(20) - 10 - 10 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(20) - 10 + 10 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(-3, -15, 3);

///// NTU SEEKER SHOTGUN.wep.gml
#define weapon_name
return "SEEKER SHOTGUN";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 42;
#define weapon_sprt
return sprSeekerShotgun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "bolt marrow makes them faster";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSeekerShotgun);
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(12) - 6 - 15 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(12) - 6 + 15 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(12) - 6 - 30 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(12) - 6 + 30 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(12) - 6 - 45 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(12) - 6 + 45 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(-7, -30, 8);

///// NTU AUTO SEEKER PISTOL.wep.gml
global.sprAutoSeekerPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprAutoSeekerPistol");
#define weapon_name
return "AUTO SEEKER PISTOL";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 10;
#define weapon_sprt
return global.sprAutoSeekerPistol;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "good o'l automatic";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSeekerPistol);
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(30) - 15 - 15 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(30) - 15 + 15 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(-3, -14, 3);

///// NTU ASSAULT SEEKER SHOTGUN.wep.gml
global.sprAssaultSeekerShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprAssaultSeekerShotgun");
#define weapon_name
return "ASSAULT SEEKER SHOTGUN";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 9;
#define weapon_load
return 90;
#define weapon_sprt
return global.sprAssaultSeekerShotgun;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "have you tried eagle eyes+bolt marrow?";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SeekerShotgunBurst")) {
	creator = other.id;
	ammo = 3;
	time = 7;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU GRENADE PISTOL.wep.gml
global.sprGrenadePistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprGrenadePistol");
#define weapon_name
return "GRENADE PISTOL";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 10;
#define weapon_sprt
return global.sprGrenadePistol;
#define weapon_area
return 0;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "tiny";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SmallGrenadeBurst")) {
	creator = other.id;
	ammo = 2;
	time = 2;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU SPLINTER SEEKER.wep.gml
global.sprSplinterSeeker = mod_script_call("mod", "NTU", "ntu_sprite", "sprSplinterSeeker");
global.sndBigSplinter = mod_script_call("mod", "NTU", "ntu_sound", "sndBigSplinter");
#define weapon_name
return "SPLINTER SEEKER";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 31;
#define weapon_sprt
return global.sprSplinterSeeker;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "hybrid";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndBigSplinter);
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(20) - 10 - 20 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(20) - 10 + 20 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
repeat (2) {
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(20) - 10) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(10) - 5) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(4, -20, 4);

///// NTU SEEKER CROSSBOW.wep.gml
global.sprSeekerCrossbow = mod_script_call("mod", "NTU", "ntu_sprite", "sprSeekerCrossbow");
#define weapon_name
return "SEEKER CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 32;
#define weapon_sprt
return global.sprSeekerCrossbow;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "bolts of all kind";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSeekerShotgun);
sound_play(sndCrossbow);
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(20) - 10 - 20 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(20) - 10 + 20 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bolt)) {
	motion_add(__angle, 25);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -40, 5);

///// NTU SUPER AUTO CROSSBOW.wep.gml
global.sprSuperAutoCrossbow = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperAutoCrossbow");
#define weapon_name
return "SUPER AUTO CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 5;
#define weapon_load
return 18;
#define weapon_sprt
return global.sprSuperAutoCrossbow;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "this is mental!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSuperCrossbow);
with (instance_create(x, y, Bolt)) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bolt)) {
	motion_add(__angle + 6 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bolt)) {
	motion_add(__angle - 6 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bolt)) {
	motion_add(__angle + 12 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bolt)) {
	motion_add(__angle - 12 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 1);
weapon_post(9, -40, 10);

///// NTU TIME BOMB.wep.gml
global.sprTimeLauncher = mod_script_call("mod", "NTU", "ntu_sprite", "sprTimeLauncher");
#define weapon_name
return "TIME BOMB";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprTimeLauncher;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "feels like it is lagging";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "TimeBomb")) {
	sticky = 0;
	motion_add(__angle + (random(6) - 3) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -10, 2);

///// NTU ULTRA BLADE GUN.wep.gml
global.sprUltraBladeGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraBladeGun");
#define weapon_name
return "ULTRA BLADE GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 25;
#define weapon_load
return 30;
#define weapon_sprt
return global.sprUltraBladeGun;
#define weapon_area
return 28;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "ultra sharp";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 40)) exit;
var __angle = gunangle;
sound_play(sndDiscgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraBlade")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 12);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -20, 14);

///// NTU KRAKEN GUN.wep.gml
global.sprKrakenGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprKrakenGun");
global.sprTentacleSpawn = mod_script_call("mod", "NTU", "ntu_sprite", "sprTentacleSpawn");
global.sndWater1 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater1");
global.sndWater2 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater2");
#define weapon_name
return "KRAKEN GUN";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprKrakenGun;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "sea monster";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRoll);
sound_play(sndBloodLauncher);
sound_play(choose(global.sndWater1, global.sndWater2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Tentacle")) {
	image_angle = __angle + (random(30) - 15) * other.accuracy;
	creator = other.id;
	team = other.team;
	ammo = 16;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) {
		sprite_index = global.sprTentacleSpawn;
		image_angle = other.image_angle;
	}
	repeat (6) with (instance_create(x, y, FishBoost)) motion_add(__angle + random(60) - 30, 2 + random(4));
}
weapon_post(5, -8, 5);

///// NTU BIG KRAKEN GUN.wep.gml
global.sprBigKrakenGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBigKrakenGun");
global.sprTentacleSpawn = mod_script_call("mod", "NTU", "ntu_sprite", "sprTentacleSpawn");
global.sndWater1 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater1");
global.sndWater2 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater2");
#define weapon_name
return "BIG KRAKEN GUN";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 25;
#define weapon_sprt
return global.sprBigKrakenGun;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "hentai gun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRoll);
sound_play_gun(sndBloodLauncher, 0.2, 0.2);
sound_play(choose(global.sndWater1, global.sndWater2));
repeat (3) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Tentacle")) {
		image_angle = __angle + (random(40) - 20) * other.accuracy;
		creator = other.id;
		damage = 6;
		team = other.team;
		ammo = 16;
		event_perform(ev_alarm, 0);
		visible = false;
		with (instance_create(x, y, LightningSpawn)) {
			sprite_index = global.sprTentacleSpawn;
			image_angle = other.image_angle;
		}
	}
}
repeat (8) with (instance_create(x, y, FishBoost)) motion_add(__angle + random(60) - 30, 2 + random(4));
weapon_post(7, -11, 7);

///// NTU KRAKEN CANNON.wep.gml
global.sprKrakenCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprKrakenCannon");
global.sprTentacleSpawn = mod_script_call("mod", "NTU", "ntu_sprite", "sprTentacleSpawn");
global.sndWater1 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater1");
global.sndWater2 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater2");
#define weapon_name
return "KRAKEN CANNON";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 6;
#define weapon_load
return 40;
#define weapon_sprt
return global.sprKrakenCannon;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "release the kraken";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRoll);
sound_play(sndBloodLauncher);
sound_play(choose(global.sndWater1, global.sndWater2));
repeat (6) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Tentacle")) {
		creator = other.id;
		damage = 10 + irandom(4);
		image_angle = __angle + (random(50) - 25) * other.accuracy;
		team = other.team;
		ammo = 24;
		event_perform(ev_alarm, 0);
		visible = false;
		with (instance_create(x, y, LightningSpawn)) {
			sprite_index = global.sprTentacleSpawn;
			image_angle = other.image_angle;
		}
	}
}
repeat (12) with (instance_create(x, y, FishBoost)) motion_add(__angle + random(60) - 30, 2 + random(4));
weapon_post(10, -20, 10);

///// NTU DOUBLE SUPER PLASMA CANNON.wep.gml
global.sprDoubleSuperPlasmaCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprDoubleSuperPlasmaCannon");
global.sndSuperPlasmaCannon = mod_script_call("mod", "NTU", "ntu_sound", "sndSuperPlasmaCannon");
#define weapon_name
return "DOUBLE SUPER PLASMA CANNON";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 48;
#define weapon_load
return 670;
#define weapon_sprt
return global.sprDoubleSuperPlasmaCannon;
#define weapon_area
return 28;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "this will break your game";
#define weapon_fire
var __angle = gunangle;
sleep(30);
sound_play(global.sndSuperPlasmaCannon);
with (instance_create(x, y, PlasmaHuge)) {
	motion_add(__angle + random(4) - 2 + 16 * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, PlasmaHuge)) {
	motion_add(__angle + random(4) - 2 - 16 * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 12);
weapon_post(20, -30, 30);
resetSpeed = 0;

///// NTU MORPH HAMMER.wep.gml
global.sprMorphHammer = mod_script_call("mod", "NTU", "ntu_sprite", "sprMorphHammer");
global.sndLaserSwordUpg = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSwordUpg");
global.sndLaserSword = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSword");
global.sndSledgeHit = mod_script_call("mod", "NTU", "ntu_sound", "sndSledgeHit");
#define weapon_name
return "MORPH HAMMER";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 30;
#define weapon_sprt
return global.sprMorphHammer;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "peacekeeper";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLaserSwordUpg);
} else sound_play(global.sndLaserSword);
instance_create(x, y, Dust);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), "MorphSlash")) {
	if (__laser_brain) {
		damage = 8 + (__laser_brain - 1) * 2;
	} else damage = 6;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	snd_hit = global.sndSledgeHit;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 12, 1);

///// NTU ULTRA SEEKER PISTOL.wep.gml
global.sprUltraSeekerPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraSeekerPistol");
#define weapon_name
return "ULTRA SEEKER PISTOL";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 16;
#define weapon_sprt
return global.sprUltraSeekerPistol;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "I see green";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 22)) exit;
var __angle = gunangle;
sound_play(sndSeekerShotgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraSeekerBolt")) {
	motion_add(__angle + random(20) - 10 - 10 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraSeekerBolt")) {
	motion_add(__angle + random(20) - 10 + 10 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraSeekerBolt")) {
	motion_add(__angle + random(20) - 20 - 20 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraSeekerBolt")) {
	motion_add(__angle + random(20) - 20 + 20 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(-6, -30, 6);

///// NTU PANDA STICK.wep.gml
global.sprStick = mod_script_call("mod", "NTU", "ntu_sprite", "sprStick");
#define weapon_name
return "PANDA STICK";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 12;
#define weapon_sprt
return global.sprStick;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "panda does not like using weapons";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
ang = __angle;
move_contact_solid(ang, 3);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), Slash)) {
	ang = other.ang;
	damage = 2;
	motion_add(ang, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
speed = -speed * 0.5;
weapon_post(-5, 6, 1);

///// NTU SUPERHOT REVOLVER.wep.gml
global.sprSuperHotRevolver = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperHotRevolver");
global.sndSwapSuperHot = mod_script_call("mod", "NTU", "ntu_sound", "sndSwapSuperHot");
global.sndHeavyRevolver = mod_script_call("mod", "NTU", "ntu_sound", "sndHeavyRevolver");
#define weapon_name
return "SUPERHOT REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 10;
#define weapon_sprt
return global.sprSuperHotRevolver;
#define weapon_area
return 8;
#define weapon_swap
return global.sndSwapSuperHot;
#define weapon_text
return "try standing still with this gun";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndHeavyRevolver);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, HeavyBullet)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -6, 7);

///// NTU SUPERHOT ASSAULT RIFLE.wep.gml
global.sprSuperHotAssaultRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperHotAssaultRifle");
global.sndSwapSuperHot = mod_script_call("mod", "NTU", "ntu_sound", "sndSwapSuperHot");
#define weapon_name
return "SUPERHOT ASSAULT RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 8;
#define weapon_load
return 16;
#define weapon_sprt
return global.sprSuperHotAssaultRifle;
#define weapon_area
return 11;
#define weapon_swap
return global.sndSwapSuperHot;
#define weapon_text
return "SUPER! HOT!#SUPER! HOT!#SUPER! HOT!#SUPER! HOT!#SUPER! HOT!#SUPER! HOT!#SUPER! HOT!#SUPER! HOT!#SUPER! HOT!#SUPER! HOT!#SUPER! HOT!";
#define weapon_fire
with (instance_create(x, y, HeavyBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 3;
	time = 4;
	event_perform(ev_alarm, 0);
}

///// NTU FLAME WAVE GUN.wep.gml
global.sprFlameWaveGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprFlameWaveGun");
#define weapon_name
return "FLAME WAVE GUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 19;
#define weapon_sprt
return global.sprFlameWaveGun;
#define weapon_area
return 14;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "stef gun 2.0";
#define weapon_fire
sound_play(sndFireShotgun);
for (var i = 6; i >= 0; i--) if (instance_exists(self)) {
	sound_play(sndShotgun);
	for (var k = -1; k <= 1; k += 2) {
		with (instance_create(x, y, FlameShell)){
			motion_add(other.gunangle + random_range(-2, 2) + k * sin(i / 2) * 16, 15);
			image_angle = direction;
			team = other.team;
			creator = other;
		}
	}
	weapon_post(7, 3, 2);
	wait 1;
}

///// NTU SPLINTER SEEKER CROSSBOW.wep.gml
global.sprSplinterSeekerCrossbow = mod_script_call("mod", "NTU", "ntu_sprite", "sprSplinterSeekerCrossbow");
global.sndBigSplinter = mod_script_call("mod", "NTU", "ntu_sound", "sndBigSplinter");
#define weapon_name
return "SPLINTER SEEKER CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 33;
#define weapon_sprt
return global.sprSplinterSeekerCrossbow;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "ultimate hybrid";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndBigSplinter);
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(20) - 10 - 20 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Seeker)) {
	motion_add(__angle + random(20) - 10 + 20 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Bolt)) {
	motion_add(__angle, 25);
	image_angle = direction;
	team = other.team;
	creator = other;
}
repeat (2) {
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(20) - 10) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(10) - 5) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -40, 6);

///// NTU PLASMA SHOTGUN.wep.gml
global.sprPlasmaShotGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprPlasmaShotGun");
#define weapon_name
return "PLASMA SHOTGUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 28;
#define weapon_sprt
return global.sprPlasmaShotGun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "je ne parle pas francais";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaUpg);
} else sound_play(sndPlasma);
repeat (3) {
	with (instance_create(x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), PlasmaBall)) {
		motion_add(__angle + (random(60) - 30) * other.accuracy, 0.4 + random(3));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 2);
weapon_post(6, -4, 4);
resetSpeed = 0;

///// NTU SMALL MISSILE LAUNCHER.wep.gml
global.sprSmallMissileLauncher = mod_script_call("mod", "NTU", "ntu_sprite", "sprSmallMissileLauncher");
#define weapon_name
return "SMALL MISSILE LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 26;
#define weapon_sprt
return global.sprSmallMissileLauncher;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "theres a bigger one";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRocket);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Missile")) {
	motion_add(__angle + random(20) - 10 - 10 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Missile")) {
	motion_add(__angle + random(20) - 10 + 10 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 2);
weapon_post(4, -4, 4);

///// NTU MISSILE LAUNCHER.wep.gml
global.sprMissileLauncher = mod_script_call("mod", "NTU", "ntu_sprite", "sprMissileLauncher");
#define weapon_name
return "MISSILE LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 48;
#define weapon_sprt
return global.sprMissileLauncher;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "you should really try eagle eyes#with this gun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRocket);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Missile")) {
	motion_add(__angle + random(12) - 6 - 15 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Missile")) {
	motion_add(__angle + random(12) - 6 + 15 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Missile")) {
	motion_add(__angle + random(12) - 6 - 30 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Missile")) {
	motion_add(__angle + random(12) - 6 + 30 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Missile")) {
	motion_add(__angle + random(12) - 6 - 45 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Missile")) {
	motion_add(__angle + random(12) - 6 + 45 * other.accuracy, 2 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 4);
weapon_post(9, -10, 11);

///// NTU FAST PLASMA SHOTGUN.wep.gml
global.sprMiniPlasmaShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprMiniPlasmaShotgun");
#define weapon_name
return "FAST PLASMA SHOTGUN";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 18;
#define weapon_sprt
return global.sprMiniPlasmaShotgun;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "as requested";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPlasma);
repeat (7) {
	with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), "MiniPlasmaBall")) {
		motion_add(__angle + (random(60) - 30) * other.accuracy, 1 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -12, 8);

///// NTU FAST PLASMA MINIGUN.wep.gml
global.sprMiniPlasmaMinigun = mod_script_call("mod", "NTU", "ntu_sprite", "sprMiniPlasmaMinigun");
#define weapon_name
return "FAST PLASMA MINIGUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprMiniPlasmaMinigun;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "orbs of death";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaUpg);
} else sound_play(sndPlasma);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), "MiniPlasmaBall")) {
	motion_add(__angle + (random(26) - 13) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 1);
weapon_post(4, -3, 2);
resetSpeed = 0;

///// NTU BIG MISSILE LAUNCHER.wep.gml
global.sprBigMissileLauncher = mod_script_call("mod", "NTU", "ntu_sprite", "sprBigMissileLauncher");
#define weapon_name
return "BIG MISSILE LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 12;
#define weapon_load
return 90;
#define weapon_sprt
return global.sprBigMissileLauncher;
#define weapon_area
return 13;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "R.Y.N.O.";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "MissileBurst")) {
	creator = other.id;
	ammo = 3;
	time = 7;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU FAST PLASMA RIFLE.wep.gml
global.sprMiniPlasmaRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprMiniPlasmaRifle");
#define weapon_name
return "FAST PLASMA RIFLE";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 6;
#define weapon_sprt
return global.sprMiniPlasmaRifle;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaUpg);
} else sound_play(sndPlasma);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), "MiniPlasmaBall")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -3, 2);
resetSpeed = 0;

///// NTU SUPER ION CANNON.wep.gml
global.sprSuperIonCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperIonCannon");
#define weapon_name
return "SUPER ION CANNON";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 10;
#define weapon_load
return 24;
#define weapon_sprt
return global.sprSuperIonCannon;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "raining hell";
#define weapon_fire
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SuperIonBurst")) {
	creator = other.id;
	ammo = 40;
	time = 1;
	team = other.team;
	alarm[0] = 20;
}
weapon_post(4, 0, 12);

///// NTU SUPER NUKE LAUNCHER.wep.gml
global.sprSuperNukeLauncher = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperNukeLauncher");
#define weapon_name
return "SUPER NUKE LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 9;
#define weapon_load
return 100;
#define weapon_sprt
return global.sprSuperNukeLauncher;
#define weapon_area
return 14;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "true power";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "NukeBurst")) {
	creator = other.id;
	ammo = 3;
	time = 10;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU VIKING GREAT AXE.wep.gml
global.sprVikingAxe = mod_script_call("mod", "NTU", "ntu_sprite", "sprVikingAxe");
global.sprAxeSlash = mod_script_call("mod", "NTU", "ntu_sprite", "sprAxeSlash");
global.sndSledgeHit = mod_script_call("mod", "NTU", "ntu_sound", "sndSledgeHit");
#define weapon_name
return "VIKING GREAT AXE";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 38;
#define weapon_sprt
return global.sprVikingAxe;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "holy mother of thor!";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), "BigSlash")) {
	sprite_index = global.sprAxeSlash;
	damage = 25;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	snd_hit = global.sndSledgeHit;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-5, 10, 3);

///// NTU VIKING AXE.wep.gml
global.sprVikingSmallAxe = mod_script_call("mod", "NTU", "ntu_sprite", "sprVikingSmallAxe");
#define weapon_name
return "VIKING AXE";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 30;
#define weapon_sprt
return global.sprVikingSmallAxe;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "not for woodchopping";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), Slash)) {
	damage = 6;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 12, 1);

///// NTU GRENADE SHOTGUN.wep.gml
global.sprGrenadeShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprGrenadeShotgun");
#define weapon_name
return "GRENADE SHOTGUN";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 17;
#define weapon_sprt
return global.sprGrenadeShotgun;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "dangerzone";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenadeShotgun);
repeat (4) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SmallGrenade")) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 9 + random(3));
		image_angle = direction;
		team = other.team;
		alarm[0] = 14;
	}
}
weapon_post(7, -13, 11);

///// NTU AUTO GRENADE SHOTGUN.wep.gml
global.sprAutoGrenadeShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprAutoGrenadeShotgun");
#define weapon_name
return "AUTO GRENADE SHOTGUN";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 9;
#define weapon_sprt
return global.sprAutoGrenadeShotgun;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "rapid fire dangerzone";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenadeShotgun);
repeat (3) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SmallGrenade")) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 9 + random(3));
		image_angle = direction;
		team = other.team;
		alarm[0] = 14;
	}
}
weapon_post(7, -12, 10);

///// NTU GRENADE RIFLE.wep.gml
global.sprGrenadeRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprGrenadeRifle");
#define weapon_name
return "GRENADE RIFLE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 10;
#define weapon_sprt
return global.sprGrenadeRifle;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "most reliable!";
#define weapon_fire
sound_play(sndGrenadeRifle);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SmallGrenadeBurst")) {
	creator = other.id;
	ammo = 3;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU HYPER GRENADE RIFLE.wep.gml
global.sprHyperGrenadeRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperGrenadeRifle");
#define weapon_name
return "HYPER GRENADE RIFLE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 5;
#define weapon_sprt
return global.sprHyperGrenadeRifle;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "not the hyper launcher";
#define weapon_fire
sound_play(sndGrenadeRifle);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SmallGrenadeBurst")) {
	creator = other.id;
	ammo = 9;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU GHETTO BLASTER.wep.gml
global.sprGhettoBlaster = mod_script_call("mod", "NTU", "ntu_sprite", "sprGhettoBlaster");
global.sndGhettoBlast = mod_script_call("mod", "NTU", "ntu_sound", "sndGhettoBlast");
#define weapon_name
return "GHETTO BLASTER";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 10;
#define weapon_sprt
return global.sprGhettoBlaster;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapMotorized;
#define weapon_text
return "BLAST THE SOUND SYSTEMS! WOOOOOOOOOO";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
sound_play(global.sndGhettoBlast);
instance_create(x, y, Dust);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), "GhettoBlast")) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 6 + random(4));
	damage = 12;
	if (instance_exists(Player)) if (__laser_brain) damage = 18;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, -6);
weapon_post(-8, 5, 16);

///// NTU DOOM LAUNCHER.wep.gml
global.sprDoomLauncher = mod_script_call("mod", "NTU", "ntu_sprite", "sprDoomLauncher");
#define weapon_name
return "DOOM LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprDoomLauncher;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapMotorized;
#define weapon_text
return "let's launch some doom!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DoomNadeCluster")) {
	sticky = 0;
	motion_add(__angle + (random(6) - 3) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -10, 2);

///// NTU LASER SWORD.wep.gml
global.sprLaserSword = mod_script_call("mod", "NTU", "ntu_sprite", "sprLaserSword");
global.sndLaserSwordUpg = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSwordUpg");
global.sndLaserSword = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSword");
#define weapon_name
return "LASER SWORD";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprLaserSword;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "how does this thing even work?";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLaserSwordUpg);
} else sound_play(global.sndLaserSword);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), EnergySlash)) {
	damage = 12;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle + (random(2) - 1) * other.accuracy;
	team = other.team;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-5, 20, 3);

///// NTU BULLET SWORD.wep.gml
global.sprBulletSword = mod_script_call("mod", "NTU", "ntu_sprite", "sprBulletSword");
global.sndSword1 = mod_script_call("mod", "NTU", "ntu_sound", "sndSword1");
global.sndSword2 = mod_script_call("mod", "NTU", "ntu_sound", "sndSword2");
#define weapon_name
return "BULLET SWORD";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprBulletSword;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "bullet bullet bullet bullet sword sword sword sword";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(choose(global.sndSword1, global.sndSword2));
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), Slash)) {
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	damage = 6;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, Burst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 4;
	time = 2;
	event_perform(ev_alarm, 0);
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-5, 14, 3);

///// NTU GOLDEN CAR THROWER.wep.gml
global.sprGoldenCarThrower = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenCarThrower");
global.sprGoldenCarIdle = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenCarIdle");
global.sprGoldenCarHurt = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenCarHurt");
#define weapon_name
return "GOLDEN CAR THROWER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 30;
#define weapon_sprt
return global.sprGoldenCarThrower;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapMotorized;
#define weapon_text
return "golden hand";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSnowBotThrow);
with (instance_create(x, y, CarThrow)) {
	maxhealth = 30;
	spr_idle = global.sprGoldenCarIdle;
	spr_hurt = global.sprGoldenCarHurt;
	team = other.team;
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
}
weapon_post(-8, 8, 14);

///// NTU GOLDEN PLASMA GUN.wep.gml
global.sprGoldenPlasmaGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenPlasmaGun");
#define weapon_name
return "GOLDEN PLASMA GUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprGoldenPlasmaGun;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "solid gold baby!";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaUpg);
} else sound_play(sndPlasma);
with (instance_create(x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), PlasmaBall)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 4);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 1.5);
weapon_post(5, -3, 3);
resetSpeed = 0;

///// NTU GOLDEN SLUGGER.wep.gml
global.sprGoldenSlugger = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenSlugger");
#define weapon_name
return "GOLDEN SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprGoldenSlugger;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "blast of gold";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSlugger);
with (instance_create(x, y, Slug)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 17);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -15, 12);

///// NTU GOLDEN ASSAULT RIFLE.wep.gml
global.sprGoldenARifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenARifle");
#define weapon_name
return "GOLDEN ASSAULT RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 9;
#define weapon_sprt
return global.sprGoldenARifle;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "burst of gold";
#define weapon_fire
weapon_post(0, -2, 2);
with (instance_create(x, y, Burst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 3;
	time = 2;
	event_perform(ev_alarm, 0);
}

///// NTU GOLDEN SPLINTER GUN.wep.gml
global.sprGoldenSplinterGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenSplinterGun");
#define weapon_name
return "GOLDEN SPLINTER GUN";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 22;
#define weapon_sprt
return global.sprGoldenSplinterGun;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "shiny splinters";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSplinterGun);
repeat (2) {
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(12) - 6) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
repeat (2) {
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(20) - 10) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	with (instance_create(x, y, Splinter)) {
		motion_add(__angle + (random(10) - 5) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(-4, -16, 4);

///// NTU GOLDEN BAZOOKA.wep.gml
global.sprGoldenBazooka = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenBazooka");
global.sprGoldenRocket = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenRocket");
#define weapon_name
return "GOLDEN BAZOOKA";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 26;
#define weapon_sprt
return global.sprGoldenBazooka;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "GOLDEN ROCKETS";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRocket);
with (instance_create(x, y, Rocket)) {
	motion_add(__angle + (random(4) - 2) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	sprite_index = global.sprGoldenRocket;
}
weapon_post(11, -31, 5);

///// NTU GOLDEN SCREWDRIVER.wep.gml
global.sprGoldenScrewdriver = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenScrewdriver");
#define weapon_name
return "GOLDEN SCREWDRIVER";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 6;
#define weapon_sprt
return global.sprGoldenScrewdriver;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "expensive fixing";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndScrewdriver);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 10, __angle), y + lengthdir_y(__long_arms * 10, __angle), Shank)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 3 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 2);
weapon_post(-9, 12, 1);

///// NTU GUITAR.wep.gml
global.sndGuitarHitWall = mod_script_call("mod", "NTU", "ntu_sound", "sndGuitarHitWall");
global.sndGuitarHit = mod_script_call("mod", "NTU", "ntu_sound", "sndGuitarHit");
#define weapon_name
return "GUITAR";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 19;
#define weapon_sprt
return sprGuitar;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "good music";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndGuitar);
instance_create(x, y, Dust);
ang = __angle;
move_contact_solid(ang, 3);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), Slash)) {
	snd_wallhit = global.sndGuitarHitWall;
	snd_hit = global.sndGuitarHit;
	ang = other.ang;
	damage = 26;
	motion_add(ang, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
speed = -speed * 0.5;
weapon_post(-8, 12, 2);

///// NTU HEAVY CROSSBOW.wep.gml
#define weapon_name
return "HEAVY CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 45;
#define weapon_sprt
return sprHeavyCrossbow;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "heavy bolts homeing is good with bolt marrow";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHeavyCrossbow);
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -45, 8);

///// NTU BLADE GUN.wep.gml
global.sprBladeGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBladeGun");
#define weapon_name
return "BLADE GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 8;
#define weapon_sprt
return global.sprBladeGun;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "blades don't hurt you";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDiscgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Blade")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -10, 6);

///// NTU HEAVY AUTO CROSSBOW.wep.gml
#define weapon_name
return "HEAVY AUTO CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 15;
#define weapon_sprt
return sprHeavyAutoCrossbow;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "this one works well with bolt marrow!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHeavyCrossbow);
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle + random(18) - 9, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -40, 7);

///// NTU SUPER HEAVY CROSSBOW.wep.gml
global.sprSuperHeavyCrossbow = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperHeavyCrossbow");
#define weapon_name
return "SUPER HEAVY CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 10;
#define weapon_load
return 60;
#define weapon_sprt
return global.sprSuperHeavyCrossbow;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "5 heavy bolts per shot";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSuperCrossbow);
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle + 5 * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle - 5 * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle + 10 * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle - 10 * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 8);
weapon_post(10, -80, 16);

///// NTU SUPER HEAVY AUTO CROSSBOW.wep.gml
global.sprSuperHeavyAutoCrossbow = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperHeavyAutoCrossbow");
#define weapon_name
return "SUPER HEAVY AUTO CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 10;
#define weapon_load
return 26;
#define weapon_sprt
return global.sprSuperHeavyAutoCrossbow;
#define weapon_area
return 28;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "this is just ridiculous!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSuperCrossbow);
sound_play(sndHeavyCrossbow);
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle + 6 * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle - 6 * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle + 12 * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBolt)) {
	motion_add(__angle - 12 * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 1);
weapon_post(9, -70, 14);

///// NTU SPIRAL SLASH DIRECTOR SHOTGUN.wep.gml
global.sprSpiralSlashDirectorShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSpiralSlashDirectorShotgun");
#define weapon_name
return "SPIRAL SLASH DIRECTOR SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 38;
#define weapon_sprt
return global.sprSpiralSlashDirectorShotgun;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "click click click";
#define weapon_fire
var __angle = gunangle;
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SlashDirectorShotgunPrep")) {
	sound_play(sndSlugger);
	motion_add(__angle + (random(6) - 3) * other.accuracy, 10 + random(2));
	image_angle = direction;
	rate = 10;
	Direction = __angle;
	creator = other.id;
	ammo = 30;
	totalammo = ammo;
	time = 1;
	team = other.team;
	creator = other;
}
weapon_post(10, -16, 12);

///// NTU SPLIT BLADE GUN.wep.gml
global.sprSplitBladeGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSplitBladeGun");
#define weapon_name
return "SPLIT BLADE GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 8;
#define weapon_sprt
return global.sprSplitBladeGun;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "the blades split...";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDiscgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SplitBlade")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -11, 7);

///// NTU ROCKET GLOVE.wep.gml
global.sprRocketGlove = mod_script_call("mod", "NTU", "ntu_sprite", "sprRocketGlove");
#define weapon_name
return "ROCKET GLOVE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 23;
#define weapon_sprt
return global.sprRocketGlove;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "ROCKET PWUNCH!";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndRocket);
sound_loop(sndFlamerLoop);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "RocketGloveBurst")) {
	creator = other.id;
	ammo = 23;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
instance_create(x, y, Dust);
repeat (4) instance_create(x, y, Smoke);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), "RocketSlash")) {
	image_speed = 0;
	damage = 14;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(0, -10, 12);

///// NTU SWARM GUN.wep.gml
global.sprSwarmGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSwarmGun");
#define weapon_name
return "SWARM GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 6;
#define weapon_sprt
return global.sprSwarmGun;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "beeeeeeeeeees!!!";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SwarmBurst")) {
	creator = other.id;
	ammo = 6;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU INFINITY SLUGGER.wep.gml
global.sprInfinitySlugger = mod_script_call("mod", "NTU", "ntu_sprite", "sprInfinitySlugger");
#define weapon_name
return "INFINITY SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 21;
#define weapon_sprt
return global.sprInfinitySlugger;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "8";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSlugger);
with (instance_create(x, y, Slug)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(9, -15, 12);

///// NTU INFINITY LASER PISTOL.wep.gml
global.sprInfinityLaserGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprInfinityLaserGun");
#define weapon_name
return "INFINITY LASER PISTOL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 9;
#define weapon_sprt
return global.sprInfinityLaserGun;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "boundlessness";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle;
	team = other.team;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
weapon_post(3, -4, 3);

///// NTU SWARM SHOTGUN.wep.gml
global.sprSwarmShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSwarmShotgun");
global.sndSwarm = mod_script_call("mod", "NTU", "ntu_sound", "sndSwarm");
#define weapon_name
return "SWARM SHOTGUN";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 22;
#define weapon_sprt
return global.sprSwarmShotgun;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "honeybee";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndSwarm);
repeat (6) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SwarmBolt")) {
		motion_add(__angle + random(12) - 6 - 15 * other.accuracy, 2 + random(3));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
repeat (6) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SwarmBolt")) {
		motion_add(__angle + (random(12) - 6) * other.accuracy, 2 + random(3));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
repeat (6) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SwarmBolt")) {
		motion_add(__angle + random(12) - 6 + 15 * other.accuracy, 2 + random(3));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -4, 6);

///// NTU ULTRA MEGA LASER PISTOL.wep.gml
global.sprUltraMegaLaserPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraMegaLaserPistol");
global.sndThunder = mod_script_call("mod", "NTU", "ntu_sound", "sndThunder");
#define weapon_name
return "ULTRA MEGA LASER PISTOL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 12;
#define weapon_load
return 50;
#define weapon_sprt
return global.sprUltraMegaLaserPistol;
#define weapon_area
return 28;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "the tam gun";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 26)) exit;
var __angle = gunangle;
sound_play(sndUltraLaserUpg);
sound_play(global.sndThunder);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "MegaLaser")) {
	image_angle = __angle + (random(2) - 1) * other.accuracy;
	team = other.team;
	image_yscale += 0.2;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "MegaLaser")) {
	image_angle = __angle + random(2) - 1 + 7 * other.accuracy;
	team = other.team;
	image_yscale += 0.2;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "MegaLaser")) {
	image_angle = __angle + random(2) - 1 + 14 * other.accuracy;
	team = other.team;
	image_yscale += 0.2;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "MegaLaser")) {
	image_angle = __angle + random(2) - 1 - 7 * other.accuracy;
	team = other.team;
	image_yscale += 0.2;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "MegaLaser")) {
	image_angle = __angle + random(2) - 1 - 14 * other.accuracy;
	team = other.team;
	image_yscale += 0.2;
	bounce = 1;
	event_perform(ev_alarm, 0);
}
weapon_post(12, -25, 18);

///// NTU LIGHTNING MINIGUN.wep.gml
global.sprLightningMinigun = mod_script_call("mod", "NTU", "ntu_sprite", "sprLightningMinigun");
global.sndLightning2 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning2");
global.sndLightning3 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning3");
global.sndLightning1 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning1");
#define weapon_name
return "LIGHTNING MINIGUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprLightningMinigun;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "lightning doesn't hit#the same place twice";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(choose(global.sndLightning2, global.sndLightning3));
} else sound_play(global.sndLightning1);
with (instance_create(x, y, Lightning)) {
	image_angle = __angle + (random(26) - 13) * other.accuracy;
	team = other.team;
	ammo = 26;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
}
weapon_post(8, -6, 8);

///// NTU SUPER SPLIT BLADE GUN.wep.gml
global.sprSuperSplitBladeGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperSplitBladeGun");
#define weapon_name
return "SUPER SPLIT BLADE GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 8;
#define weapon_load
return 12;
#define weapon_sprt
return global.sprSuperSplitBladeGun;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "very sharp";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDiscgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SuperSplitBlade")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(7, -14, 12);

///// NTU BOMB LAUNCHER.wep.gml
global.sprBombLauncher = mod_script_call("mod", "NTU", "ntu_sprite", "sprBombLauncher");
#define weapon_name
return "BOMB LAUNCHER";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprBombLauncher;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "bomberman";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SplitGrenade")) {
	sticky = 1;
	motion_add(__angle + (random(6) - 3) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -12, 4);

///// NTU ULTRA POP GUN.wep.gml
global.sprUltraPopGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraPopGun");
#define weapon_name
return "ULTRA POP GUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 2;
#define weapon_sprt
return global.sprUltraPopGun;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 2)) exit;
var __angle = gunangle;
sound_play(sndMachinegun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet5")) {
	motion_add(__angle + (random(16) - 8) * other.accuracy, 14 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -8, 4);

///// NTU HEAVY SLUGGER.wep.gml
#define weapon_name
return "HEAVY SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 10;
#define weapon_sprt
return sprHeavySlugger;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "such weight";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHeavySlugger);
with (instance_create(x, y, HeavySlug)) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 12);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(10, -16, 18);

///// NTU AUTO EXPLOSIVE BOW.wep.gml
global.sprAutoExploBow = mod_script_call("mod", "NTU", "ntu_sprite", "sprAutoExploBow");
#define weapon_name
return "AUTO EXPLOSIVE BOW";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 12;
#define weapon_sprt
return global.sprAutoExploBow;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ExplosiveBolt")) {
	motion_add(__angle + (random(16) - 8) * other.accuracy, 22);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -36, 4);

///// NTU YOYO GUN.wep.gml
global.sprYoyoGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprYoyoGun");
#define weapon_name
return "YOYO GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprYoyoGun;
#define weapon_area
return 2;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "away! and return!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDiscgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -12, 6);

///// NTU SUPER YOYO GUN.wep.gml
global.sprSuperYoyoGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperYoyoGun");
#define weapon_name
return "SUPER YOYO GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 8;
#define weapon_load
return 8;
#define weapon_sprt
return global.sprSuperYoyoGun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "cutting edge technology";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDiscgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle + random(10) - 5, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle + 10 + (random(10) - 5) * other.accuracy, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle - 10 + (random(10) - 5) * other.accuracy, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle + 20 + (random(10) - 5) * other.accuracy, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle - 20 + (random(10) - 5) * other.accuracy, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 1);
weapon_post(8, -20, 10);

///// NTU SHANK GUN.wep.gml
global.sprShankGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprShankGun");
#define weapon_name
return "SHANK GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 12;
#define weapon_sprt
return global.sprShankGun;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "long arms, bolt marrow and shotgun shoulders#all work on this gun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ShankBullet")) {
	motion_add(__angle + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -5, 4);

///// NTU SUPER SHANK GUN.wep.gml
global.sprSuperShankGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperShankGun");
#define weapon_name
return "SUPER SHANK GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 10;
#define weapon_load
return 35;
#define weapon_sprt
return global.sprSuperShankGun;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "shank them to dead";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHeavyCrossbow);
motion_add(__angle + 180, 16);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ShankBullet")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ShankBullet")) {
	motion_add(__angle + 10 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ShankBullet")) {
	motion_add(__angle + 20 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ShankBullet")) {
	motion_add(__angle - 10 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ShankBullet")) {
	motion_add(__angle - 20 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -50, 14);

///// NTU ENERGY SHANK GUN.wep.gml
global.sprEnergyShankGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprEnergyShankGun");
#define weapon_name
return "ENERGY SHANK GUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 25;
#define weapon_sprt
return global.sprEnergyShankGun;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "works with all weapon specific mutations";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaBigUpg);
} else sound_play(sndPlasmaBig);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergyShankBullet")) {
	motion_add(__angle + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -4, 6);

///// NTU SUPER ENERGY SHANK GUN.wep.gml
global.sprSuperEnergyShankGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperEnergyShankGun");
global.sndLightningPlasma2 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightningPlasma2");
#define weapon_name
return "SUPER ENERGY SHANK GUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 17;
#define weapon_load
return 50;
#define weapon_sprt
return global.sprSuperEnergyShankGun;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "new meta";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndLightningPlasma2);
motion_add(__angle + 180, 16);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergyShankBullet")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergyShankBullet")) {
	motion_add(__angle + 10 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergyShankBullet")) {
	motion_add(__angle + 20 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergyShankBullet")) {
	motion_add(__angle - 10 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "EnergyShankBullet")) {
	motion_add(__angle - 20 * other.accuracy + (random(4) - 2) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(10, -55, 16);

///// NTU LIGHTNING CROSSBOW.wep.gml
global.sprLightningCrossbow = mod_script_call("mod", "NTU", "ntu_sprite", "sprLightningCrossbow");
#define weapon_name
return "LIGHTNING CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 25;
#define weapon_sprt
return global.sprLightningCrossbow;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "lightning jolt!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "LightningBolt")) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -40, 4);

///// NTU SHOTGUN RIFLE.wep.gml
global.sprShotgunRifle1 = mod_script_call("mod", "NTU", "ntu_sprite", "sprShotgunRifle1");
global.sprShotgunRifle2 = mod_script_call("mod", "NTU", "ntu_sprite", "sprShotgunRifle2");
global.sprShotgunRifle1 = mod_script_call("mod", "NTU", "ntu_sprite", "sprShotgunRifle1");
#define weapon_name
return "SHOTGUN RIFLE";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 18;
#define weapon_sprt
return global.sprShotgunRifle1;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "true hybrid gun";
#define weapon_fire
var __angle = gunangle;
if (wep_type[258] == 2) {
	sound_play(sndShotgun);
	repeat (8) {
		with (instance_create(x, y, Bullet2)) {
			motion_add(__angle + (random(40) - 20) * other.accuracy, 12 + random(6));
			image_angle = direction;
			team = other.team;
		}
	}
	weapon_post(6, -12, 9);
	wep_type[258] = 1;
	wep_sprt[258] = global.sprShotgunRifle2;
	wep_cost[258] = 5;
} else {
	with (instance_create(x, y, Burst)) {
		accuracy = other.accuracy;
		team = other.team;
		creator = other;
		ammo = 5;
		time = 2;
		event_perform(ev_alarm, 0);
	}
	wep_type[258] = 2;
	wep_sprt[258] = global.sprShotgunRifle1;
	wep_cost[258] = 1;
}

///// NTU BULLET POPPER.wep.gml
global.sprBulletPopper1 = mod_script_call("mod", "NTU", "ntu_sprite", "sprBulletPopper1");
global.sprBulletPopper1 = mod_script_call("mod", "NTU", "ntu_sprite", "sprBulletPopper1");
global.sprBulletPopper2 = mod_script_call("mod", "NTU", "ntu_sprite", "sprBulletPopper2");
#define weapon_name
return "BULLET POPPER";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 4;
#define weapon_sprt
if (instance_is(self, Player)) {
	return ("ntu_bullet_popper" in self) && ntu_bullet_popper ? global.sprBulletPopper1 : global.sprBulletPopper2;
} else return global.sprBulletPopper1;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "make a decision, gun!";
#define weapon_fire
if (("ntu_bullet_popper" in self) && ntu_bullet_popper) {
	ntu_bullet_popper = 0;
	for (var ammo = 2; ammo > 0; ammo--) if (instance_exists(self)) {
		sound_play_gun(sndPopgun, 0.2, 0.3);
		with (instance_create(x, y, Shell)) motion_add(other.gunangle + 180 + random(50) - 25, 2 + random(2));
		with (instance_create(x, y, Bullet2)) {
			motion_add(other.gunangle + random_range(-7, 7), 14 + random(2));
			image_angle = direction;
			team = other.team;
			creator = other;
		}
		weapon_post(4, -6, 3);
		wait 2;
	}
} else {
	ntu_bullet_popper = 1;
	var __angle = gunangle;
	sound_play_gun(sndMachinegun, 0.2, 0.3);
	with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
	with (instance_create(x, y, Bullet1)) {
		motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	weapon_post(2, -6, 3);
}

///// NTU HYPER HEAVY SLUGGER.wep.gml
global.sprHyperHeavySlugger = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperHeavySlugger");
#define weapon_name
return "HYPER HEAVY SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 11;
#define weapon_sprt
return global.sprHyperHeavySlugger;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "heavy slugger with infinite range!";
#define weapon_fire
sound_play(sndHyperSlugger);
sound_play(sndHeavySlugger);
with (instance_create(rx, ry, HyperSlug)) {
	direction = other.gunangle + (random(4) - 2) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
	damage = 60;
	sprite_index = sprHeavySlug;
}
weapon_post(8, -20, 4);

///// NTU HYPER MACHINEGUN.wep.gml
global.sprHyperMachinegun = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperMachinegun");
global.sndHeavyRevolver = mod_script_call("mod", "NTU", "ntu_sound", "sndHeavyRevolver");
#define weapon_name
return "HYPER MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 5;
#define weapon_sprt
return global.sprHyperMachinegun;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "hyper machinegun shoots through walls";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndHeavyRevolver);
sound_play(sndHyperSlugger);
weapon_post(8, -10, 4);
with (instance_create(x, y, HeavyBullet)) {
	damage = 10;
	team = other.team;
	creator = other;
	direction = __angle + random_range(-2, 2) * other.accuracy;
	image_angle = direction;
	wait 1; if (!instance_exists(self)) break;
	move_contact_solid(direction, 16);
	repeat (5) with (instance_create(x, y, Smoke)) motion_add(random(360), random(4));
	sleep(50);
	var dx = lengthdir_x(4, direction);
	var dy = lengthdir_y(4, direction);
	var steps = 0;
	repeat (100) {
		steps += 1;
		x += dx;
		y += dy;
		if (random(10) < 1) with (instance_create(x, y, Smoke)) motion_add(random(360), random(2));
		var q = instance_place(x, y, hitme);
		instance_create(x, y, EmoteIndicator);
		trace(q);
		if (q != noone && q.team != team && q.my_health > 0) break;
	}
	trace(steps);
	speed = 16;
	wait 20;
	if (instance_exists(self)) instance_destroy();
}

///// NTU SPIRAL YOYO GUN.wep.gml
global.sprSpiralYoyoGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprSpiralYoyoGun");
#define weapon_name
return "SPIRAL YOYO GUN";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 10;
#define weapon_load
return 8;
#define weapon_sprt
return global.sprSpiralYoyoGun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "this looks stupid";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDiscgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy + 60, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy + 120, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy + 180, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy + 240, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Yoyo")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy + 300, 6);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -11, 12);

///// NTU ELECTRIC GUITAR.wep.gml
global.sprElectricGuitar = mod_script_call("mod", "NTU", "ntu_sprite", "sprElectricGuitar");
global.sndGuitarHitWall = mod_script_call("mod", "NTU", "ntu_sound", "sndGuitarHitWall");
global.sndGuitarHit = mod_script_call("mod", "NTU", "ntu_sound", "sndGuitarHit");
#define weapon_name
return "ELECTRIC GUITAR";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 16;
#define weapon_sprt
return global.sprElectricGuitar;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "let's rock";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndGuitar);
instance_create(x, y, Dust);
ang = __angle;
move_contact_solid(ang, 4);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), LightningSlash)) {
	snd_wallhit = global.sndGuitarHitWall;
	snd_hit = global.sndGuitarHit;
	ang = other.ang;
	damage = 22;
	motion_add(ang, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
speed = -speed * 0.5;
weapon_post(-8, 12, 2);

///// NTU BROKEN STER GUN.wep.gml
global.sprBrokenSterGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBrokenSterGun");
#define weapon_name
return "BROKEN STER GUN";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 40;
#define weapon_load
return 300;
#define weapon_sprt
return global.sprBrokenSterGun;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "you broke the game";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRocket);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SterNuke")) {
	motion_add(__angle + random(4) - 2 + 10 * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SterNuke")) {
	motion_add(__angle + random(4) - 2 - 10 * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(20, -60, 40);

///// NTU HEAVY MINIGUN.wep.gml
global.sprHeavyMinigun = mod_script_call("mod", "NTU", "ntu_sprite", "sprHeavyMinigun");
#define weapon_name
return "HEAVY MINIGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 1;
#define weapon_sprt
return global.sprHeavyMinigun;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "AAW YEA SON DAT SHI DOPE";
#define weapon_fire
var __angle = gunangle;
sound_play(sndMinigun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(80) - 40, 3 + random(2));
with (instance_create(x, y, HeavyBullet)) {
	motion_add(__angle + (random(26) - 13) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 1);
weapon_post(6, -8, 5);

///// NTU HEAVY DOUBLE MINIGUN.wep.gml
global.sprHeavyDoubleMinigun = mod_script_call("mod", "NTU", "ntu_sprite", "sprHeavyDoubleMinigun");
#define weapon_name
return "HEAVY DOUBLE MINIGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 1;
#define weapon_sprt
return global.sprHeavyDoubleMinigun;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "those who can't hear me I say";
#define weapon_fire
var __angle = gunangle;
sound_play(sndDoubleMinigun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(80) - 40, 3 + random(2));
repeat (2) {
	with (instance_create(x, y, HeavyBullet)) {
		motion_add(__angle + (random(32) - 16) * other.accuracy, 16);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(__angle + 180, 1.4);
weapon_post(9, -8, 10);

///// NTU FAT REVOLVER.wep.gml
global.sprFatRevolver = mod_script_call("mod", "NTU", "ntu_sprite", "sprFatRevolver");
global.sndHeavyRevolver = mod_script_call("mod", "NTU", "ntu_sound", "sndHeavyRevolver");
#define weapon_name
return "FAT REVOLVER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 5;
#define weapon_sprt
return global.sprFatRevolver;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "CHUBBY GUN";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndHeavyRevolver);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
repeat (4) with (instance_create(x, y, Smoke)) motion_add(__angle + (random(30) - 15) * other.accuracy, 3 + random(3));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FatBullet")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 12);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -6, 6);

///// NTU FAT MACHINEGUN.wep.gml
global.sprFatMachinegun = mod_script_call("mod", "NTU", "ntu_sprite", "sprFatMachinegun");
global.sndHeavyRevolver = mod_script_call("mod", "NTU", "ntu_sound", "sndHeavyRevolver");
#define weapon_name
return "FAT MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 5;
#define weapon_load
return 5;
#define weapon_sprt
return global.sprFatMachinegun;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "BIG AND FAT";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndHeavyRevolver);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
repeat (4) with (instance_create(x, y, Smoke)) motion_add(__angle + (random(30) - 15) * other.accuracy, 3 + random(3));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FatBullet")) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 12);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -6, 5);

///// NTU BOUNCER LASER PISTOL.wep.gml
global.sprBouncerLaserGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBouncerLaserGun");
global.sprBouncingLaser = mod_script_call("mod", "NTU", "ntu_sprite", "sprBouncingLaser");
#define weapon_name
return "BOUNCER LASER PISTOL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 11;
#define weapon_sprt
return global.sprBouncerLaserGun;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "unpredictable";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle + (random(2) - 1) * other.accuracy;
	team = other.team;
	event_perform(ev_alarm, 0);
	laserhit = 4;
	sprite_index = global.sprBouncingLaser;
	image_yscale -= 0.2;
}
weapon_post(2, -3, 2);

///// NTU BOUNCER LASER RIFLE.wep.gml
global.sprBouncerLaserRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprBouncerLaserRifle");
global.sprBouncingLaser = mod_script_call("mod", "NTU", "ntu_sprite", "sprBouncingLaser");
#define weapon_name
return "BOUNCER LASER RIFLE";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 8;
#define weapon_sprt
return global.sprBouncerLaserRifle;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "all over the place";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndLaserUpg);
} else sound_play(sndLaser);
with (instance_create(x, y, Laser)) {
	image_angle = __angle + (random(8) - 4) * other.accuracy;
	team = other.team;
	event_perform(ev_alarm, 0);
	laserhit = 6;
	sprite_index = global.sprBouncingLaser;
	image_yscale -= 0.2;
}
weapon_post(5, -3, 2);

///// NTU FAT SMG.wep.gml
global.sprFatSmg = mod_script_call("mod", "NTU", "ntu_sprite", "sprFatSmg");
global.sndHeavyRevolver = mod_script_call("mod", "NTU", "ntu_sound", "sndHeavyRevolver");
#define weapon_name
return "FAT SMG";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprFatSmg;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "DEEP PENETRATION";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndHeavyRevolver);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(60) - 30, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FatBullet")) {
	motion_add(__angle + (random(32) - 16) * other.accuracy, 12);
	image_angle = direction;
	team = other.team;
	creator = other;
}
repeat (4) with (instance_create(x, y, Smoke)) motion_add(__angle + (random(30) - 15) * other.accuracy, 3 + random(3));
weapon_post(4, -6, 6);

///// NTU GHOST BLADE.wep.gml
global.sprGhostBlade = mod_script_call("mod", "NTU", "ntu_sprite", "sprGhostBlade");
global.sndSword1 = mod_script_call("mod", "NTU", "ntu_sound", "sndSword1");
global.sndSword2 = mod_script_call("mod", "NTU", "ntu_sound", "sndSword2");
#define weapon_name
return "GHOST BLADE";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 41;
#define weapon_sprt
return global.sprGhostBlade;
#define weapon_area
return 13;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "spooky";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(choose(global.sndSword1, global.sndSword2));
sound_play(sndBloodLauncher);
instance_create(x, y, Smoke);
instance_create(x, y, Dust);
repeat (4 + irandom(3)) with (mod_script_call("mod", "NTU", "ntu_create", x, y, "GhostEffect")) motion_add(__angle + (random(30) - 15) * other.accuracy, 3 + random(4));
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20 + 26, __angle), y + lengthdir_y(__long_arms * 20 + 26, __angle), "GhostSlash")) {
	damage = 13;
	motion_add(__angle, 7 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), "GhostSlash")) {
	damage = 13;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-7, 12, 6);

///// NTU GYRO BURSTER.wep.gml
global.sprGyroBurster = mod_script_call("mod", "NTU", "ntu_sprite", "sprGyroBurster");
global.sndGyroBurst = mod_script_call("mod", "NTU", "ntu_sound", "sndGyroBurst");
#define weapon_name
return "GYRO BURSTER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 30;
#define weapon_load
return 50;
#define weapon_sprt
return global.sprGyroBurster;
#define weapon_area
return 14;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "bwbwpebffpwefnowobfo";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
sound_play(global.sndGyroBurst);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "GyroBurst")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 4);
	team = other.team;
	creator = other;
}
weapon_post(4, -6, 10);

///// NTU INFUSER.wep.gml
global.sprNewInfuser = mod_script_call("mod", "NTU", "ntu_sprite", "sprNewInfuser");
#define weapon_name
return "INFUSER";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 22;
#define weapon_sprt
return global.sprNewInfuser;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "sucks out the blood";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "InfuseBolt")) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -40, 4);

///// NTU GHOST SHOVEL.wep.gml
global.sprGhostShovel = mod_script_call("mod", "NTU", "ntu_sprite", "sprGhostShovel");
#define weapon_name
return "GHOST SHOVEL";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 56;
#define weapon_sprt
return global.sprGhostShovel;
#define weapon_area
return 16;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "digging up graves";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndShovel);
sound_play(sndBloodLauncher);
instance_create(x, y, Smoke);
instance_create(x, y, Dust);
repeat (3 + irandom(3)) with (mod_script_call("mod", "NTU", "ntu_create", x, y, "GhostEffect")) motion_add(__angle + (random(30) - 15) * other.accuracy, 3 + random(4));
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), "GhostSlash")) {
	damage = 15;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
repeat (3 + irandom(3)) with (mod_script_call("mod", "NTU", "ntu_create", x, y, "GhostEffect")) motion_add(__angle + 70 + (random(30) - 15) * other.accuracy, 3 + random(4));
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 15, __angle + 60 * accuracy), y + lengthdir_y(__long_arms * 15, __angle + 60 * accuracy), "GhostSlash")) {
	damage = 15;
	motion_add(__angle + 60 * other.accuracy, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
repeat (3 + irandom(3)) with (mod_script_call("mod", "NTU", "ntu_create", x, y, "GhostEffect")) motion_add(__angle - 70 + (random(30) - 15) * other.accuracy, 3 + random(4));
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 15, __angle - 60 * accuracy), y + lengthdir_y(__long_arms * 15, __angle - 60 * accuracy), "GhostSlash")) {
	damage = 16;
	motion_add(__angle - 60 * other.accuracy, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20 + 26, __angle), y + lengthdir_y(__long_arms * 20 + 26, __angle), "GhostSlash")) {
	damage = 16;
	motion_add(__angle, 7 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 15 + 26, __angle + 60 * accuracy), y + lengthdir_y(__long_arms * 15 + 26, __angle + 60 * accuracy), "GhostSlash")) {
	damage = 16;
	motion_add(__angle + 60 * other.accuracy, 7 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 15 + 26, __angle - 60 * accuracy), y + lengthdir_y(__long_arms * 15 + 26, __angle - 60 * accuracy), "GhostSlash")) {
	damage = 16;
	motion_add(__angle - 60 * other.accuracy, 7 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 24, 1);

///// NTU GOLDEN FLAMETHROWER.wep.gml
global.sprGoldenFlameThrower = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenFlameThrower");
#define weapon_name
return "GOLDEN FLAMETHROWER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 7;
#define weapon_sprt
return global.sprGoldenFlameThrower;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "shiny flames";
#define weapon_fire
if (!instance_exists(FlameSound)) instance_create(x, y, FlameSound);
with (instance_create(x, y, FlameBurst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 11;
	time = 1;
	event_perform(ev_alarm, 0);
}

///// NTU GOLDEN SLEDGEHAMMER.wep.gml
global.sprGoldenHeavySlash = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenHeavySlash");
#define weapon_name
return "GOLDEN SLEDGEHAMMER";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 33;
#define weapon_sprt
return sprGoldHammer;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "hammer time";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), Slash)) {
	sprite_index = global.sprGoldenHeavySlash;
	damage = 17;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 12, 2);

///// NTU GOLDEN SHOVEL.wep.gml
global.sprGoldenShovel = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenShovel");
global.sprGoldenSlash = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenSlash");
#define weapon_name
return "GOLDEN SHOVEL";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 32;
#define weapon_sprt
return global.sprGoldenShovel;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "dig";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play_gun(sndShovel, 0.2, 0.3);
instance_create(x, y, Dust);
var l = __long_arms * 20;
for (var i = -1; i <= 1; i++) {
	var d = __angle + i * 60 * accuracy;
	with (instance_create(x + lengthdir_x(l, d), y + lengthdir_y(l, d), Slash)) {
		sprite_index = global.sprGoldenSlash;
		damage = 10;
		motion_add(__angle, 2 + 3 * __long_arms);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 24, 2);

///// NTU GOLDEN EXPLOSIVE BOW.wep.gml
global.sprGoldenExplosiveBow = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenExplosiveBow");
#define weapon_name
return "GOLDEN EXPLOSIVE BOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 26;
#define weapon_sprt
return global.sprGoldenExplosiveBow;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "golden explosives";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "GoldenExplosiveBolt")) {
	motion_add(__angle, 22);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -40, 6);

///// NTU GOLDEN ASSAULT PISTOL.wep.gml
global.sprGoldenAPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenAPistol");
#define weapon_name
return "GOLDEN ASSAULT PISTOL";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 6;
#define weapon_sprt
return global.sprGoldenAPistol;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "red velvet cake";
#define weapon_fire
with (instance_create(x, y, Burst)) {
	accuracy = other.accuracy;
	team = other.team;
	creator = other;
	ammo = 2;
	time = 2;
	event_perform(ev_alarm, 0);
}

///// NTU GOLDEN SHORTGUN.wep.gml
global.sprGoldenShortgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenShortgun");
#define weapon_name
return "GOLDEN SHORTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 9;
#define weapon_sprt
return global.sprGoldenShortgun;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "no golden sex jokes";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (22) {
	with (instance_create(x, y, Bullet2)) {
		motion_add(__angle + (random(80) - 40) * other.accuracy, 6 + random(7));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(8, -13, 10);

///// NTU GOLDEN BULLET SHOTGUN.wep.gml
global.sprGoldenBulletShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenBulletShotgun");
#define weapon_name
return "GOLDEN BULLET SHOTGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 6;
#define weapon_load
return 18;
#define weapon_sprt
return global.sprGoldenBulletShotgun;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "maroon fiber";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (8) {
	with (instance_create(x, y, Bullet1)) {
		motion_add(__angle + (random(36) - 18) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(7, -13, 9);

///// NTU GOLDEN PISTOLE.wep.gml
global.sprGoldenPistole = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenPistole");
#define weapon_name
return "GOLDEN PISTOLE";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 26;
#define weapon_sprt
return global.sprGoldenPistole;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "pistole is not a typo";
#define weapon_fire
for (var ammo = 2; ammo > 0; ammo -= 1) if (instance_exists(self)) {
	repeat (5) with (instance_create(x, y, Bullet2)) {
		motion_add(other.gunangle + random_range(-20, 20) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	weapon_post(6, 12, 8);
	wait 3;
}

///// NTU GOLDEN POP GUN.wep.gml
global.sprGoldenPopGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenPopGun");
#define weapon_name
return "GOLDEN POP GUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 2;
#define weapon_sprt
return global.sprGoldenPopGun;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "more range";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPopgun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet2)) {
	motion_add(__angle + (random(16) - 8) * other.accuracy, 16 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 3);

///// NTU GOLDEN POP RIFLE.wep.gml
global.sprGoldenPopRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenPopRifle");
#define weapon_name
return "GOLDEN POP RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 7;
#define weapon_sprt
return global.sprGoldenPopRifle;
#define weapon_area
return 17;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "gold infused pellets";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Burst2")) {
	creator = other.id;
	ammo = 3;
	time = 2;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU GOLDEN GLOVE.wep.gml
global.sprGoldenGlove = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenGlove");
global.sprGoldenSlash = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenSlash");
#define weapon_name
return "GOLDEN GLOVE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 20;
#define weapon_sprt
return global.sprGoldenGlove;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "gold touch of death";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
ang = __angle;
repeat (5) {
	move_contact_solid(ang, 28);
	instance_create(x, y, Dust);
	with (instance_create(x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), Slash)) {
		sprite_index = global.sprGoldenSlash;
		ang = other.ang;
		damage = 9;
		motion_add(ang, 2 + 3 * __long_arms);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(ang, 3);
alarm[3] = 4;
speed = -speed * 0.5;
weapon_post(-12, 8, 5);

///// NTU GOLDEN FLARE GUN.wep.gml
global.sprGoldenFlareGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenFlareGun");
#define weapon_name
return "GOLDEN FLARE GUN";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 22;
#define weapon_sprt
return global.sprGoldenFlareGun;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "thats a fancy signal";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFlare);
with (instance_create(x, y, Flare)) {
	sticky = 0;
	motion_add(__angle + (random(14) - 7) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -10, 7);

///// NTU GOLDEN SMG.wep.gml
global.sprGoldenSmg = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenSmg");
#define weapon_name
return "GOLDEN SMG";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprGoldenSmg;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "gold infused bullets";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(60) - 30, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(24) - 12) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);

///// NTU ULTRA SLUGGER.wep.gml
global.sprUltraSlugger = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraSlugger");
#define weapon_name
return "ULTRA SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 15;
#define weapon_sprt
return global.sprUltraSlugger;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "biological slugs";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 24)) exit;
var __angle = gunangle;
sound_play(sndSuperSlugger);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraSlug")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(9, -15, 16);

///// NTU GOLDEN BOUNCER SMG.wep.gml
global.sprGoldenBouncerSMG = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenBouncerSMG");
global.sndBouncerFire = mod_script_call("mod", "NTU", "ntu_sound", "sndBouncerFire");
#define weapon_name
return "GOLDEN BOUNCER SMG";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprGoldenBouncerSMG;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "fast bouncing";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndBouncerFire);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(60) - 30, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet3")) {
	motion_add(__angle + (random(34) - 17) * other.accuracy, 6.4);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 3);

///// NTU ENERGY GLOVE.wep.gml
global.sprEnergyGlove = mod_script_call("mod", "NTU", "ntu_sprite", "sprEnergyGlove");
global.sndLaserSwordUpg = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSwordUpg");
global.sndLaserSword = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSword");
#define weapon_name
return "ENERGY GLOVE";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 26;
#define weapon_sprt
return global.sprEnergyGlove;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "batteries inlcuded";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLaserSwordUpg);
} else sound_play(global.sndLaserSword);
instance_create(x, y, Dust);
ang = __angle;
repeat (3) {
	move_contact_solid(ang, 44);
	instance_create(x, y, Dust);
	instance_create(x, y, Dust);
	with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), EnergyHammerSlash)) {
		motion_add(__angle, 1 + 2 * __long_arms);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(ang, 3);
alarm[3] = 4;
speed = -speed * 0.5;
weapon_post(-12, 8, 4);

///// NTU SEEKER CANNON.wep.gml
global.sprSeekerCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprSeekerCannon");
#define weapon_name
return "SEEKER CANNON";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 14;
#define weapon_load
return 60;
#define weapon_sprt
return global.sprSeekerCannon;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "spray and pray";
#define weapon_fire
sound_play(sndSeekerPistol);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SeekerCannonBurst")) {
	creator = other.id;
	ammo = 30;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(-3, -15, 3);

///// NTU SUPER DIRECTOR SLUGGER.wep.gml
global.sprSuperDirectorSlugger = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperDirectorSlugger");
#define weapon_name
return "SUPER DIRECTOR SLUGGER";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 40;
#define weapon_sprt
return global.sprSuperDirectorSlugger;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "focus all force#onto one point";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSuperSlugger);
motion_add(__angle + 180, 3);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DirectorSlug")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DirectorSlug")) {
	motion_add(__angle + 10 * other.accuracy + (random(8) - 4) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DirectorSlug")) {
	motion_add(__angle + 20 * other.accuracy + (random(8) - 4) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DirectorSlug")) {
	motion_add(__angle - 10 * other.accuracy + (random(8) - 4) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DirectorSlug")) {
	motion_add(__angle - 20 * other.accuracy + (random(8) - 4) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -10, 15);

///// NTU CHARGE SHOTGUN.wep.gml
global.sprChargeShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprChargeShotgun");
#define weapon_name
return "CHARGE SHOTGUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 30;
#define weapon_sprt
return global.sprChargeShotgun;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "get a load of this";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ChargeShotgun")) {
	maxcharge = 27;
	type = 2;
	cost = 1;
	creator = other.id;
	chargetime = 2;
	costtime = 14;
	team = other.team;
	event_perform(ev_alarm, 0);
	event_perform(ev_alarm, 1);
}

///// NTU CHARGE LASER.wep.gml
global.sprChargeLaserGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprChargeLaserGun");
#define weapon_name
return "CHARGE LASER";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 34;
#define weapon_sprt
return global.sprChargeLaserGun;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "wind it up";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ChargeLaser")) {
	maxcharge = 27;
	type = 5;
	cost = 1;
	creator = other.id;
	chargetime = 1;
	costtime = 14;
	team = other.team;
	event_perform(ev_alarm, 0);
	event_perform(ev_alarm, 1);
}

///// NTU CHARGE FLAK CANNON.wep.gml
global.sprChargeFlakCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprChargeFlakCannon");
#define weapon_name
return "CHARGE FLAK CANNON";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 40;
#define weapon_sprt
return global.sprChargeFlakCannon;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "more pellets";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ChargeFlakCannon")) {
	maxcharge = 38;
	type = 2;
	cost = 1;
	creator = other.id;
	chargetime = 1;
	costtime = 13;
	team = other.team;
	event_perform(ev_alarm, 0);
	event_perform(ev_alarm, 1);
}

///// NTU ULTRA FLAK CANNON.wep.gml
global.sprUltraFlakCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraFlakCannon");
#define weapon_name
return "ULTRA FLAK CANNON";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 24;
#define weapon_sprt
return global.sprUltraFlakCannon;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "green ball of green pellets";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 27)) exit;
var __angle = gunangle;
sound_play(sndUltraGrenade);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraFlakBullet")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 12 + random(3));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -34, 8);

///// NTU GOLDEN OOPS GUN.wep.gml
global.sprGoldenOopsGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprGoldenOopsGun");
#define weapon_name
return "GOLDEN OOPS GUN";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 7;
#define weapon_sprt
return global.sprGoldenOopsGun;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "you are not supposed to live";
#define weapon_fire
// <does nothing>

///// NTU ULTRA WAVE GUN.wep.gml
global.sprUltraWaveGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraWaveGun");
#define weapon_name
return "ULTRA WAVE GUN";
#define weapon_type
return 2;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprUltraWaveGun;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "ultra wave fun";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 26)) exit;
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraWaveBurst")) {
	creator = other.id;
	ammo = 7;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU LASER WAVE GUN.wep.gml
global.sprLaserWaveGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprLaserWaveGun");
#define weapon_name
return "LASER WAVE GUN";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 7;
#define weapon_load
return 10;
#define weapon_sprt
return global.sprLaserWaveGun;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "hands in de aihr";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "LaserWaveBurst")) {
	creator = other.id;
	ammo = 20;
	time = 2;
	team = other.team;
	event_perform(ev_alarm, 0);
}
weapon_post(-3, -4, 3);

///// NTU ULTRA GLOVE.wep.gml
global.sprUltraGlove = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraGlove");
#define weapon_name
return "ULTRA GLOVE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 18;
#define weapon_sprt
return global.sprUltraGlove;
#define weapon_area
return 19;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "ultra fingers";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 18)) exit;
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndUltraShovel);
instance_create(x, y, Dust);
instance_create(x, y, Smoke);
ang = __angle;
repeat (3) {
	move_contact_solid(ang, 40);
	instance_create(x, y, Dust);
	instance_create(x, y, Smoke);
	with (instance_create(x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), Slash)) {
		damage = 30;
		sprite_index = sprUltraSlash;
		ang = other.ang;
		motion_add(ang, 2 + 3 * __long_arms);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(ang, 3);
alarm[3] = 4;
speed = -speed * 0.5;
weapon_post(-12, 8, 8);

///// NTU ULTRA SPLINTER GUN.wep.gml
global.sprUltraSplinterGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraSplinterGun");
#define weapon_name
return "ULTRA SPLINTER GUN";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 18;
#define weapon_sprt
return global.sprUltraSplinterGun;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "dodge this";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 16)) exit;
var __angle = gunangle;
sound_play(sndUltraCrossbow);
sound_play(sndSplinterGun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraSplinter")) {
	motion_add(__angle + (random(6) - 3) * other.accuracy, 20 + random(4));
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraSplinter")) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 20 + random(4));
	image_angle = direction;
	team = other.team;
	creator = other;
}
repeat (2) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraSplinter")) {
		motion_add(__angle + (random(20) - 10) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraSplinter")) {
		motion_add(__angle + (random(10) - 5) * other.accuracy, 20 + random(4));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(-6, -20, 6);

///// NTU ULTRA SUPER CROSSBOW.wep.gml
global.sprUltraSuperCrossbow = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraSuperCrossbow");
#define weapon_name
return "ULTRA SUPER CROSSBOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 24;
#define weapon_sprt
return global.sprUltraSuperCrossbow;
#define weapon_area
return 22;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "this will waste your rads";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 80)) exit;
var __angle = gunangle;
sound_play(sndSuperCrossbow);
sound_play(sndUltraCrossbow);
with (instance_create(x, y, UltraBolt)) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, UltraBolt)) {
	motion_add(__angle + 5 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, UltraBolt)) {
	motion_add(__angle - 5 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, UltraBolt)) {
	motion_add(__angle + 10 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, UltraBolt)) {
	motion_add(__angle - 10 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 1);
weapon_post(9, -62, 16);

///// NTU ULTRA INFUSER.wep.gml
global.sprNewUltraInfuser = mod_script_call("mod", "NTU", "ntu_sprite", "sprNewUltraInfuser");
#define weapon_name
return "ULTRA INFUSER";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 18;
#define weapon_sprt
return global.sprNewUltraInfuser;
#define weapon_area
return 20;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "sucks out the rads";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 15)) exit;
var __angle = gunangle;
sound_play(sndUltraCrossbow);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "UltraInfuseBolt")) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(6, -45, 7);

///// NTU SNOW BLOWER.wep.gml
global.sprSnowBlower = mod_script_call("mod", "NTU", "ntu_sprite", "sprSnowBlower");
#define weapon_name
return "SNOW BLOWER";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 4;
#define weapon_sprt
return global.sprSnowBlower;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "burn burn burn";
#define weapon_fire
if (!instance_exists(/*!*/SnowSound)) instance_create(x, y, /*!*/SnowSound);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SnowBurst")) {
	creator = other.id;
	ammo = 7;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU BULLET DISPERSE GUN.wep.gml
global.sprBulletDisperseGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBulletDisperseGun");
global.sndHeavyRevolver = mod_script_call("mod", "NTU", "ntu_sound", "sndHeavyRevolver");
#define weapon_name
return "BULLET DISPERSE GUN";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprBulletDisperseGun;
#define weapon_area
return 3;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "no sense was made";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndHeavyRevolver);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DisperseBullet")) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	time = 2;
	event_perform(ev_alarm, 0);
}
weapon_post(4, -8, 5);

///// NTU FROST FLARE GUN.wep.gml
global.sprFrostFlareGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprFrostFlareGun");
#define weapon_name
return "FROST FLARE GUN";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 26;
#define weapon_sprt
return global.sprFrostFlareGun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapFlame;
#define weapon_text
return "signal for yeti";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFlare);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FrostFlare")) {
	sticky = 0;
	motion_add(__angle + (random(14) - 7) * other.accuracy, 9);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(5, -10, 5);

///// NTU BULLET DISPERSE MACHINEGUN.wep.gml
global.sprBulletDisperseMachinegun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBulletDisperseMachinegun");
#define weapon_name
return "BULLET DISPERSE MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 8;
#define weapon_load
return 4;
#define weapon_sprt
return global.sprBulletDisperseMachinegun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "rapid fire goodness";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHeavyMachinegun);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DisperseBullet")) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	time = 1;
	event_perform(ev_alarm, 0);
}
weapon_post(4, -8, 5);

///// NTU POP DISPERSE GUN.wep.gml
global.sprPopDisperseGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprPopDisperseGun");
#define weapon_name
return "POP DISPERSE GUN";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 12;
#define weapon_sprt
return global.sprPopDisperseGun;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "no sense was made";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DispersePellet")) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	time = 2;
	event_perform(ev_alarm, 0);
}
weapon_post(4, -8, 6);

///// NTU LASER DISPERSE GUN.wep.gml
global.sprLaserDisperseGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprLaserDisperseGun");
#define weapon_name
return "LASER DISPERSE GUN";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 12;
#define weapon_sprt
return global.sprLaserDisperseGun;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "no sense was made";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPlasma);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DisperseLaser")) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	time = 2;
	event_perform(ev_alarm, 0);
}
weapon_post(5, -9, 6);

///// NTU POTATO CANNON.wep.gml
global.sprPotatoCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprPotatoCannon");
global.sndPotato = mod_script_call("mod", "NTU", "ntu_sound", "sndPotato");
#define weapon_name
return "POTATO CANNON";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 4;
#define weapon_sprt
return global.sprPotatoCannon;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapMotorized;
#define weapon_text
return "look mummy its me";
#define weapon_fire
var __angle = gunangle;
ammo[1] -= 10;
ammo[2] -= 1;
ammo[3] -= 1;
ammo[4] -= 1;
sound_play(sndPartyHorn);
sound_play(global.sndPotato);
repeat (69) with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 10 + random(10));
repeat (10) with (instance_create(x, y, Dust)) motion_add(random(360), 0.2 + random(12));
repeat (10) with (instance_create(x, y, Smoke)) motion_add(random(360), 0.2 + random(12));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Potato")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
if (random(50) < 1) with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Sheep")) motion_add(__angle + random(8) - 4, 16 + random(8));
if (random(100) < 1) with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ExplosiveSheep")) motion_add(__angle + random(8) - 4, 16 + random(8));
if (random(900) < 1) instance_create(x, y, ScrapBoss);
if (random(900) < 1) instance_create(x, y, LilHunter);
if (random(900) < 1) instance_create(x, y, /*!*/ChesireCat);
if (random(900) < 1) instance_create(x, y, BanditBoss);
if (random(54) < 1) move_contact_solid(direction + 180, 300);
if (random(54) < 1) move_contact_solid(direction, 300);
move_contact_solid(random(360), random(8));
weapon_post(choose(-24, 24), -16, 69);

///// NTU ROGUE RIFLE.wep.gml
#define weapon_name
return "ROGUE RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 6;
#define weapon_sprt
return sprRogueRifle;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "popo nono";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "RogueRifleBurst")) {
	creator = other.id;
	ammo = 2;
	time = 2;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU HYPER INFUSER.wep.gml
global.sprHyperInfuser = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperInfuser");
#define weapon_name
return "HYPER INFUSER";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 4;
#define weapon_sprt
return global.sprHyperInfuser;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "shoot through walls";
#define weapon_fire
var __angle = gunangle;
sound_play(sndHeavyCrossbow);
sound_play(sndHyperSlugger);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "HyperInfuseBolt")) {
	direction = __angle + (random(4) - 2) * other.accuracy;
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -30, 7);

///// NTU CHARGE SPLINTERGUN.wep.gml
global.sprChargeSplinterGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprChargeSplinterGun");
#define weapon_name
return "CHARGE SPLINTERGUN";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 30;
#define weapon_sprt
return global.sprChargeSplinterGun;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "infuse with blood";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ChargeSplinterGun")) {
	maxcharge = 27;
	type = 3;
	cost = 1;
	creator = other.id;
	chargetime = 2;
	costtime = 14;
	team = other.team;
	event_perform(ev_alarm, 0);
	event_perform(ev_alarm, 1);
}

///// NTU MONEY GUN.wep.gml
global.sprMoneyGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprMoneyGun");
#define weapon_name
return "MONEY GUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 1;
#define weapon_sprt
return global.sprMoneyGun;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "phat stax";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPopgun);
sound_play(sndPartyHorn);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(60) - 30, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "MoneyBullet")) {
	motion_add(__angle + (random(40) - 20) * other.accuracy, 18);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -6, 3);

///// NTU HUNTER HEAVY SNIPER.wep.gml
global.sprHeavySniper = mod_script_call("mod", "NTU", "ntu_sprite", "sprHeavySniper");
#define weapon_name
return "HUNTER HEAVY SNIPER";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 7;
#define weapon_load
return 11;
#define weapon_sprt
return global.sprHeavySniper;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "efficiency is on par";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSniperFire);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, HeavyBullet)) {
	motion_add(__angle + 4, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBullet)) {
	motion_add(__angle - 4, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBullet)) {
	motion_add(__angle - 8, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBullet)) {
	motion_add(__angle + 8, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, HeavyBullet)) {
	motion_add(__angle, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -16, 8);

///// NTU AUTO TOXIC BOW.wep.gml
global.sprAutoToxicBow = mod_script_call("mod", "NTU", "ntu_sprite", "sprAutoToxicBow");
#define weapon_name
return "AUTO TOXIC BOW";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 8;
#define weapon_sprt
return global.sprAutoToxicBow;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "so many farts";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (instance_create(x, y, ToxicBolt)) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -36, 4);

///// NTU SUPER TOXIC BOW.wep.gml
global.sprSuperToxicBow = mod_script_call("mod", "NTU", "ntu_sprite", "sprSuperToxicBow");
#define weapon_name
return "SUPER TOXIC BOW";
#define weapon_type
return 3;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 34;
#define weapon_sprt
return global.sprSuperToxicBow;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "diarrhea";
#define weapon_fire
var __angle = gunangle;
sound_play(sndSuperCrossbow);
with (instance_create(x, y, ToxicBolt)) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, ToxicBolt)) {
	motion_add(__angle + 5 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, ToxicBolt)) {
	motion_add(__angle - 5 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, ToxicBolt)) {
	motion_add(__angle + 10 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x, y, ToxicBolt)) {
	motion_add(__angle - 10 * other.accuracy, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 1);
weapon_post(8, -55, 12);

///// NTU TOXIC ION CANNON.wep.gml
global.sprToxicIonCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprToxicIonCannon");
#define weapon_name
return "TOXIC ION CANNON";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprToxicIonCannon;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "shit from above";
#define weapon_fire
sound_play(sndGrenade);
sound_play(sndToxicBarrelGas);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ToxicIonBurst")) {
	creator = other.id;
	ammo = 20;
	time = 1;
	team = other.team;
	alarm[0] = 14;
}
weapon_post(3, 0, 6);

///// NTU DISPERSE BAZOOKA.wep.gml
global.sprDisperseBazooka = mod_script_call("mod", "NTU", "ntu_sprite", "sprDisperseBazooka");
#define weapon_name
return "DISPERSE BAZOOKA";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 10;
#define weapon_load
return 46;
#define weapon_sprt
return global.sprDisperseBazooka;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "medicinal herb gun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRocket);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DisperseRocket")) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	time = 5;
}
weapon_post(4, -30, 4);

///// NTU LIGHTNING SCREWDRIVER.wep.gml
global.sprLightningScrewDriver = mod_script_call("mod", "NTU", "ntu_sprite", "sprLightningScrewDriver");
#define weapon_name
return "LIGHTNING SCREWDRIVER";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 9;
#define weapon_sprt
return global.sprLightningScrewDriver;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndScrewdriver);
instance_create(x, y, Dust);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 10, __angle), y + lengthdir_y(__long_arms * 10, __angle), "LightningShank")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 3 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 4);
weapon_post(-8, 14, 2);

///// NTU SNOW DRAGON.wep.gml
global.sprSnowDragon = mod_script_call("mod", "NTU", "ntu_sprite", "sprSnowDragon");
#define weapon_name
return "SNOW DRAGON";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprSnowDragon;
#define weapon_area
return 14;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "cold breath";
#define weapon_fire
if (!instance_exists(/*!*/SnowDragonSound)) instance_create(x, y, /*!*/SnowDragonSound);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SnowDragonBurst")) {
	creator = other.id;
	ammo = 4;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU THUNDERCRACK.wep.gml
global.sprThunderCrack = mod_script_call("mod", "NTU", "ntu_sprite", "sprThunderCrack");
global.sprTentacleSpawn = mod_script_call("mod", "NTU", "ntu_sprite", "sprTentacleSpawn");
global.sndWater1 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater1");
global.sndWater2 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater2");
global.sndLightning3 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning3");
global.sndLightning1 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning1");
#define weapon_name
return "THUNDERCRACK";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 26;
#define weapon_sprt
return global.sprThunderCrack;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "Eye of the storming ocean";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
sound_play(sndRoll);
sound_play(sndBloodLauncher);
sound_play(choose(global.sndWater1, global.sndWater2));
repeat (2) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Tentacle")) {
		image_angle = __angle + (random(30) - 15) * other.accuracy;
		creator = other.id;
		team = other.team;
		ammo = 17;
		damage = 8;
		event_perform(ev_alarm, 0);
		visible = false;
		with (instance_create(x, y, LightningSpawn)) {
			sprite_index = global.sprTentacleSpawn;
			image_angle = other.image_angle;
		}
		repeat (6) with (instance_create(x, y, FishBoost)) motion_add(__angle + random(60) - 30, 2 + random(4));
	}
}
if (__laser_brain) {
	sound_play(global.sndLightning3);
} else sound_play(global.sndLightning1);
repeat (2) {
	with (instance_create(x, y, Lightning)) {
		image_angle = __angle + random(30) - 15 - 30 * other.accuracy;
		team = other.team;
		ammo = 9;
		event_perform(ev_alarm, 0);
		visible = false;
		with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
	}
	with (instance_create(x, y, Lightning)) {
		image_angle = __angle + random(30) - 15 + 30 * other.accuracy;
		team = other.team;
		ammo = 9;
		event_perform(ev_alarm, 0);
		visible = false;
		with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
	}
}
weapon_post(6, -9, 6);
if (wep_type[wep] == 4) {
	wep_type[wep] = 5;
} else wep_type[wep] = 4;

///// NTU ICE CANNON.wep.gml
global.sprIceCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprIceCannon");
global.sndFrostShot1 = mod_script_call("mod", "NTU", "ntu_sound", "sndFrostShot1");
#define weapon_name
return "ICE CANNON";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 36;
#define weapon_sprt
return global.sprIceCannon;
#define weapon_area
return 14;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "Iceball!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
sound_play(global.sndFrostShot1);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "IceCannonBall")) {
	image_angle = random(360);
	motion_add(__angle + (random(8) - 4) * other.accuracy, 3);
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 2);
weapon_post(8, -6, 8);

///// NTU FROST FLAK CANNON.wep.gml
global.sprFrostFlakCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprFrostFlakCannon");
#define weapon_name
return "FROST FLAK CANNON";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 10;
#define weapon_load
return 30;
#define weapon_sprt
return global.sprFrostFlakCannon;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "Shards of pain";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FrostFlakBullet")) {
	motion_add(__angle + (random(10) - 5) * other.accuracy, 11 + random(2));
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -32, 5);

///// NTU TOXIC CANNON.wep.gml
global.sprToxicCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprToxicCannon");
#define weapon_name
return "TOXIC CANNON";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 32;
#define weapon_sprt
return global.sprToxicCannon;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "Ball of gass";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenade);
sound_play(sndToxicBoltGas);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ToxicCannonBall")) {
	image_angle = __angle;
	motion_add(__angle + (random(8) - 4) * other.accuracy, 3);
}
motion_add(__angle + 180, 2);
weapon_post(7, -6, 6);

///// NTU BLOOD CANNON.wep.gml
#define weapon_name
return "BLOOD CANNON";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 4;
#define weapon_load
return 36;
#define weapon_sprt
return sprBloodCannon;
#define weapon_area
return 14;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "voices!";
#define weapon_fire
var __angle = gunangle;
sound_play(sndBloodCannon);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "BloodCannonBall")) {
	image_angle = random(360);
	motion_add(__angle + (random(8) - 4) * other.accuracy, 3);
	team = other.team;
	creator = other;
}
motion_add(__angle + 180, 2);
weapon_post(8, -6, 8);

///// NTU BLACK SWORD.wep.gml
global.sndSword1 = mod_script_call("mod", "NTU", "ntu_sound", "sndSword1");
global.sndSword2 = mod_script_call("mod", "NTU", "ntu_sound", "sndSword2");
#define weapon_name
return "BLACK SWORD";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 14;
#define weapon_sprt
return sprBlackSword;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "chicken hates her sword";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
if (my_health > 0) {
	sound_play(choose(global.sndSword1, global.sndSword2));
} else sound_play(sndBlackSwordMega);
instance_create(x, y, Dust);
ang = __angle;
move_contact_solid(ang, 5);
instance_create(x, y, Dust);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), "BlackSwordSlash")) {
	ang = other.ang;
	motion_add(ang, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
speed = -speed * 0.5;
weapon_post(-6, 9, 2);

///// NTU DARK SWORD.wep.gml
global.sprDarkSword = mod_script_call("mod", "NTU", "ntu_sprite", "sprDarkSword");
global.sndSword1 = mod_script_call("mod", "NTU", "ntu_sound", "sndSword1");
global.sndSword2 = mod_script_call("mod", "NTU", "ntu_sound", "sndSword2");
#define weapon_name
return "DARK SWORD";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 28;
#define weapon_sprt
return global.sprDarkSword;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapSword;
#define weapon_text
return "chicken is being destroyed by her sword";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
if (my_health > 0) {
	sound_play(choose(global.sndSword1, global.sndSword2));
} else {
	sound_play(sndBloodCannonLoop);
	sound_play(sndBlackSwordMega);
}
instance_create(x, y, Dust);
ang = __angle;
move_contact_solid(ang, 5);
instance_create(x, y, Dust);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), "DarkSwordSlash")) {
	ang = other.ang;
	motion_add(ang, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
speed = -speed * 0.5;
weapon_post(-7, 10, 3);

///// NTU DOOM PISTOL.wep.gml
global.sprDoomPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprDoomPistol");
#define weapon_name
return "DOOM PISTOL";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 9;
#define weapon_sprt
return global.sprDoomPistol;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "cute bots of doom";
#define weapon_fire
var __angle = gunangle;
sound_play(sndGrenadeRifle);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DoomGrenade")) {
	walk = 6 + irandom(6);
	motion_add(__angle + (random(6) - 3) * other.accuracy, 16);
	team = other.team;
	creator = other;
}
weapon_post(4, -6, 2);

///// NTU DOOM RIFLE.wep.gml
global.sprDoomRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprDoomRifle");
#define weapon_name
return "DOOM RIFLE";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 3;
#define weapon_load
return 15;
#define weapon_sprt
return global.sprDoomRifle;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "doombots are cool";
#define weapon_fire
sound_play(sndGrenadeShotgun);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DoomBurst")) {
	creator = other.id;
	ammo = 3;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU ENERGY SHOVEL.wep.gml
global.sprEnergyShovel = mod_script_call("mod", "NTU", "ntu_sprite", "sprEnergyShovel");
global.sndLaserSwordUpg = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSwordUpg");
global.sndLaserSword = mod_script_call("mod", "NTU", "ntu_sound", "sndLaserSword");
#define weapon_name
return "ENERGY SHOVEL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 5;
#define weapon_load
return 24;
#define weapon_sprt
return global.sprEnergyShovel;
#define weapon_area
return 12;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "energetic digging";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLaserSwordUpg);
} else sound_play(global.sndLaserSword);
instance_create(x, y, Dust);
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), EnergySlash)) {
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle - 60 * accuracy), y + lengthdir_y(__long_arms * 20, __angle - 60 * accuracy), EnergySlash)) {
	motion_add(__angle - 60, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
with (instance_create(x + lengthdir_x(__long_arms * 20, __angle + 60 * accuracy), y + lengthdir_y(__long_arms * 20, __angle + 60 * accuracy), EnergySlash)) {
	motion_add(__angle + 60, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 7);
weapon_post(-5, 26, 2);

///// NTU HYPER GATLING BAZOOKA.wep.gml
global.sprHyperGatlingBazooka = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperGatlingBazooka");
#define weapon_name
return "HYPER GATLING BAZOOKA";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 1;
#define weapon_sprt
return global.sprHyperGatlingBazooka;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "bazooka minigun";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRocket);
with (instance_create(x, y, Rocket)) {
	motion_add(__angle + (random(26) - 13) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(10, -30, 4);

///// NTU ION BOMB.wep.gml
global.sprIonBomb = mod_script_call("mod", "NTU", "ntu_sprite", "sprIonBomb");
#define weapon_name
return "ION BOMB";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 11;
#define weapon_sprt
return global.sprIonBomb;
#define weapon_area
return 4;
#define weapon_swap
return sndSwapExplosive;
#define weapon_text
return "explosions out of nowhere";
#define weapon_fire
sound_play(sndRocket);
sound_play(sndHyperSlugger);
instance_create(mouse_x, mouse_y, Explosion);
weapon_post(4, -8, 8);

///// NTU FROST SMG.wep.gml
global.sprFrostSMG = mod_script_call("mod", "NTU", "ntu_sprite", "sprFrostSMG");
global.sndFrostShot1 = mod_script_call("mod", "NTU", "ntu_sound", "sndFrostShot1");
#define weapon_name
return "FROST SMG";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 3;
#define weapon_sprt
return global.sprFrostSMG;
#define weapon_area
return 6;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndFrostShot1);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(60) - 30, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FreezeBullet")) {
	motion_add(__angle + (random(32) - 16) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 3);

///// NTU FROST ASSAULT RIFLE.wep.gml
global.sprFrostARifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprFrostARifle");
#define weapon_name
return "FROST ASSAULT RIFLE";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 6;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprFrostARifle;
#define weapon_area
return 5;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "";
#define weapon_fire
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FrostBurst")) {
	creator = other.id;
	ammo = 3;
	time = 2;
	team = other.team;
	event_perform(ev_alarm, 0);
}

///// NTU MINI LIGHTNING PISTOL.wep.gml
global.sprMiniLightningPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprMiniLightningPistol");
global.sndLightning3 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning3");
global.sndLightning1 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning1");
#define weapon_name
return "MINI LIGHTNING PISTOL";
#define weapon_type
return 5;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 6;
#define weapon_sprt
return global.sprMiniLightningPistol;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "and we're back";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(global.sndLightning3);
} else sound_play(global.sndLightning1);
with (instance_create(x, y, Lightning)) {
	image_angle = __angle + (random(30) - 15) * other.accuracy;
	team = other.team;
	ammo = 7;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
}
weapon_post(3, -2, 4);

///// NTU KRAKEN PISTOL.wep.gml
global.sprKrakenPistol = mod_script_call("mod", "NTU", "ntu_sprite", "sprKrakenPistol");
global.sprTentacleSpawn = mod_script_call("mod", "NTU", "ntu_sprite", "sprTentacleSpawn");
global.sndWater1 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater1");
global.sndWater2 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater2");
#define weapon_name
return "KRAKEN PISTOL";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 1;
#define weapon_load
return 11;
#define weapon_sprt
return global.sprKrakenPistol;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "kraken gives you iframes";
#define weapon_fire
var __angle = gunangle;
sound_play(sndRoll);
sound_play(sndBloodLauncher);
sound_play(choose(global.sndWater1, global.sndWater2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Tentacle")) {
	image_angle = __angle + (random(30) - 15) * other.accuracy;
	creator = other.id;
	team = other.team;
	ammo = 8;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) {
		sprite_index = global.sprTentacleSpawn;
		image_angle = other.image_angle;
	}
	repeat (6) with (instance_create(x, y, FishBoost)) motion_add(__angle + random(60) - 30, 2 + random(4));
}
weapon_post(5, -8, 5);

///// NTU FROST HAMMER.wep.gml
global.sprFrostHammer = mod_script_call("mod", "NTU", "ntu_sprite", "sprFrostHammer");
#define weapon_name
return "FROST HAMMER";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 38;
#define weapon_sprt
return global.sprFrostHammer;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "hammer time";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), "FrostSlash")) {
	damage = 17;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 12, 1);

///// NTU TRIDENT.wep.gml
global.sprTrident = mod_script_call("mod", "NTU", "ntu_sprite", "sprTrident");
#define weapon_name
return "TRIDENT";
#define weapon_type
return 0;
#define weapon_auto
return 0;
#define weapon_cost
return 0;
#define weapon_load
return 34;
#define weapon_sprt
return global.sprTrident;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "hammer time";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
sound_play(sndBloodLauncher);
sound_play(sndRoll);
instance_create(x, y, Dust);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(__long_arms * 20, __angle), y + lengthdir_y(__long_arms * 20, __angle), "KrakenSlash")) {
	damage = 16;
	motion_add(__angle, 2 + 3 * __long_arms);
	image_angle = direction;
	team = other.team;
	creator = other;
}
wepangle = -wepangle;
motion_add(__angle, 6);
weapon_post(-4, 12, 1);

///// NTU ULTRA KRAKEN CANNON.wep.gml
global.sprUltraKrakenCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraKrakenCannon");
global.sprUltraTentacle = mod_script_call("mod", "NTU", "ntu_sprite", "sprUltraTentacle");
global.sprTentacleSpawn = mod_script_call("mod", "NTU", "ntu_sprite", "sprTentacleSpawn");
global.sndWater1 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater1");
global.sndWater2 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater2");
#define weapon_name
return "ULTRA KRAKEN CANNON";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 7;
#define weapon_load
return 55;
#define weapon_sprt
return global.sprUltraKrakenCannon;
#define weapon_area
return 11;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "true power of the ocean";
#define weapon_fire
if (mod_script_call("mod", "NTU", "ntu_wep_rad", 30)) exit;
var __angle = gunangle;
motion_add(__angle + 180, 4);
sound_play(sndRoll);
sound_play(sndBloodCannon);
sound_play(choose(global.sndWater1, global.sndWater2));
repeat (8) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Tentacle")) {
		sprite_index = global.sprUltraTentacle;
		ultra = 1;
		creator = other.id;
		damage = 18;
		image_angle = __angle + (random(60) - 30) * other.accuracy;
		team = other.team;
		ammo = 48;
		event_perform(ev_alarm, 0);
		visible = false;
		with (instance_create(x, y, LightningSpawn)) {
			sprite_index = global.sprTentacleSpawn;
			image_angle = other.image_angle;
		}
	}
}
repeat (12) with (instance_create(x, y, FishBoost)) motion_add(__angle + random(60) - 30, 2 + random(4));
weapon_post(10, -20, 10);

///// NTU KRAKEN ION CANNON.wep.gml
global.sprKrakenIonCannon = mod_script_call("mod", "NTU", "ntu_sprite", "sprKrakenIonCannon");
#define weapon_name
return "KRAKEN ION CANNON";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 4;
#define weapon_load
return 13;
#define weapon_sprt
return global.sprKrakenIonCannon;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "sharknado";
#define weapon_fire
sound_play(sndRoll);
sound_play(sndBloodLauncher);
sound_play(sndHyperSlugger);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "KrakenIonBurst")) {
	creator = other.id;
	ammo = 20;
	time = 1;
	team = other.team;
	alarm[0] = 14;
}
weapon_post(4, -2, 7);

///// NTU DIMENSION GENERATOR.wep.gml
global.sprDimensionGenerator = mod_script_call("mod", "NTU", "ntu_sprite", "sprDimensionGenerator");
global.sprTentacleSpawn = mod_script_call("mod", "NTU", "ntu_sprite", "sprTentacleSpawn");
global.sndLightningPlasma2 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightningPlasma2");
global.sndWater1 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater1");
global.sndWater2 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater2");
#define weapon_name
return "DIMENSION GENERATOR";
#define weapon_type
return 4;
#define weapon_auto
return 0;
#define weapon_cost
return 10;
#define weapon_load
return 50;
#define weapon_sprt
return global.sprDimensionGenerator;
#define weapon_area
return 18;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "the tentacle universe";
#define weapon_fire
var __angle = gunangle;
motion_add(__angle + 180, 3);
sound_play(global.sndLightningPlasma2);
sound_play(sndRoll);
sound_play(sndBloodCannon);
sound_play(choose(global.sndWater1, global.sndWater2));
repeat (2) {
	with (instance_create(x, y, Lightning)) {
		image_angle = __angle + (random(60) - 30) * other.accuracy;
		team = other.team;
		ammo = 6;
		event_perform(ev_alarm, 0);
		visible = false;
		with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
	}
}
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Tentacle")) {
	image_angle = __angle + (random(30) - 15) * other.accuracy;
	creator = other.id;
	team = other.team;
	ammo = 7;
	damage = 14;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) {
		sprite_index = global.sprTentacleSpawn;
		image_angle = other.image_angle;
	}
	repeat (6) with (instance_create(x, y, FishBoost)) motion_add(__angle + random(60) - 30, 2 + random(4));
}
sound_loop(sndBloodCannonLoop);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Implosion")) {
	damage = 12;
	kraken = 1;
	sticky = 0;
	motion_add(__angle + (random(6) - 3) * other.accuracy, 5);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(8, -14, 6);

///// NTU ELECTRO GUN.wep.gml
global.sprElectroGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprElectroGun");
#define weapon_name
return "ELECTRO GUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 12;
#define weapon_sprt
return global.sprElectroGun;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "considered plasma & lightning";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaUpg);
} else sound_play(sndPlasma);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), "ElectroBall")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ElectroBallSpawn")) {
		motion_add(__angle + random(8) - 4, 1);
		image_angle = direction;
	}
}
motion_add(__angle + 180, 1.5);
weapon_post(5, -3, 3);
resetSpeed = 0;

///// NTU ELECTRO RIFLE.wep.gml
global.sprElectroRifle = mod_script_call("mod", "NTU", "ntu_sprite", "sprElectroRifle");
#define weapon_name
return "ELECTRO RIFLE";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 6;
#define weapon_sprt
return global.sprElectroRifle;
#define weapon_area
return 9;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "connected";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaUpg);
} else sound_play(sndPlasma);
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), "ElectroBall")) {
	motion_add(__angle + (random(16) - 8) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ElectroBallSpawn")) {
		motion_add(__angle + random(8) - 4, 1);
		image_angle = direction;
	}
}
motion_add(__angle + 180, 2);
weapon_post(4, -3, 2);
resetSpeed = 0;

///// NTU AUTO BULLET SHOTGUN.wep.gml
global.sprAutoBulletShotgun = mod_script_call("mod", "NTU", "ntu_sprite", "sprAutoBulletShotgun");
#define weapon_name
return "AUTO BULLET SHOTGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 6;
#define weapon_load
return 8;
#define weapon_sprt
return global.sprAutoBulletShotgun;
#define weapon_area
return 8;
#define weapon_swap
return sndSwapShotgun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndShotgun);
repeat (6) {
	with (instance_create(x, y, Bullet1)) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(6, -11, 7);

///// NTU TRIPLE ELECTRO GUN.wep.gml
global.sprTripleElectroGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprTripleElectroGun");
global.sndSpark1 = mod_script_call("mod", "NTU", "ntu_sound", "sndSpark1");
global.sndSpark2 = mod_script_call("mod", "NTU", "ntu_sound", "sndSpark2");
#define weapon_name
return "TRIPLE ELECTRO GUN";
#define weapon_type
return 5;
#define weapon_auto
return 1;
#define weapon_cost
return 3;
#define weapon_load
return 5;
#define weapon_sprt
return global.sprTripleElectroGun;
#define weapon_area
return 13;
#define weapon_swap
return sndSwapEnergy;
#define weapon_text
return "chain lightning";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (__laser_brain) {
	sound_play(sndPlasmaUpg);
} else sound_play(sndPlasma);
sound_play(choose(global.sndSpark1, global.sndSpark2));
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), "ElectroBall")) {
	motion_add(__angle + random(8) - 4 + 24 * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ElectroBallSpawn")) {
		motion_add(__angle + random(8) - 4, 1);
		image_angle = direction;
	}
}
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), "ElectroBall")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ElectroBallSpawn")) {
		motion_add(__angle + random(8) - 4, 1);
		image_angle = direction;
	}
}
with (mod_script_call("mod", "NTU", "ntu_create", x + lengthdir_x(8, __angle), y + lengthdir_y(8, __angle), "ElectroBall")) {
	motion_add(__angle + random(8) - 4 - 24 * other.accuracy, 2);
	image_angle = direction;
	team = other.team;
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "ElectroBallSpawn")) {
		motion_add(__angle + random(8) - 4, 1);
		image_angle = direction;
	}
}
motion_add(__angle + 180, 2);
weapon_post(5, -3, 4);
resetSpeed = 0;

///// NTU FROG PISTOL.wep.gml
#define weapon_name
return "FROG PISTOL";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 2;
#define weapon_load
return 5;
#define weapon_sprt
return sprFrogBlaster;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "burp";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFrogPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
repeat (3) {
	with (instance_create(x, y, EnemyBullet2)) {
		motion_add(__angle + (random(16) - 8) * other.accuracy, 9 + random(7));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(2, -6, 4);

///// NTU GOLDEN FROG PISTOL.wep.gml
#define weapon_name
return "GOLDEN FROG PISTOL";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 3;
#define weapon_sprt
return sprGoldFrogBlaster;
#define weapon_area
return -1;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "automatic burping";
#define weapon_fire
var __angle = gunangle;
sound_play(sndFrogPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
repeat (5) {
	with (instance_create(x, y, EnemyBullet2)) {
		motion_add(__angle + (random(26) - 13) * other.accuracy, 11 + random(5));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
weapon_post(3, -7, 5);

///// NTU HYPER GLOVE.wep.gml
global.sprHyperGlove = mod_script_call("mod", "NTU", "ntu_sprite", "sprHyperGlove");
#define weapon_name
return "HYPER GLOVE";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 2;
#define weapon_load
return 16;
#define weapon_sprt
return global.sprHyperGlove;
#define weapon_area
return 7;
#define weapon_swap
return sndSwapHammer;
#define weapon_text
return "this is the 350th weapon!";
#define weapon_fire
var __angle = gunangle;
var __long_arms = mod_script_call("mod", "NTU", "ntu_wep_long_arms");
sound_play(sndHammer);
instance_create(x, y, Dust);
ang = __angle;
speed = 0;
direction = __angle;
repeat (10) {
	distancetravel = 0;
	do {
		x += lengthdir_x(2, ang);
		y += lengthdir_y(2, ang);
		distancetravel += 2;
	} until (!place_free(x + lengthdir_x(2, ang), y + lengthdir_y(2, ang)) || place_meeting(x + lengthdir_x(2, ang), y + lengthdir_y(2, ang), Wall) || distancetravel > 18);
	instance_create(x, y, Dust);
	with (instance_create(x + lengthdir_x(__long_arms * 20, ang), y + lengthdir_y(__long_arms * 20, ang), Slash)) {
		ang = other.ang;
		damage = 14;
		motion_add(ang, 2 + 3 * __long_arms);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
motion_add(ang, 3);
alarm[3] = 4;
speed = -speed * 0.5;
weapon_post(-12, 8, 4);

///// NTU BULLET DISPERSE DISPERSE GUN.wep.gml
global.sprBulletDisperseDisperseGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprBulletDisperseDisperseGun");
global.sndHeavyRevolver = mod_script_call("mod", "NTU", "ntu_sound", "sndHeavyRevolver");
#define weapon_name
return "BULLET DISPERSE DISPERSE GUN";
#define weapon_type
return 1;
#define weapon_auto
return 0;
#define weapon_cost
return 15;
#define weapon_load
return 14;
#define weapon_sprt
return global.sprBulletDisperseDisperseGun;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapPistol;
#define weapon_text
return "disperse it all!";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndHeavyRevolver);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "DisperseDisperseBullet")) {
	motion_add(__angle + (random(12) - 6) * other.accuracy, 10);
	image_angle = direction;
	team = other.team;
	time = 2;
	event_perform(ev_alarm, 0);
}
weapon_post(6, -9, 6);

///// NTU ELEMENTAL GUN.wep.gml
global.sprElementalGun = mod_script_call("mod", "NTU", "ntu_sprite", "sprElementalGun");
global.sprTentacleSpawn = mod_script_call("mod", "NTU", "ntu_sprite", "sprTentacleSpawn");
global.sndFrostShot1 = mod_script_call("mod", "NTU", "ntu_sound", "sndFrostShot1");
global.sndWater1 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater1");
global.sndWater2 = mod_script_call("mod", "NTU", "ntu_sound", "sndWater2");
global.sndLightning2 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning2");
global.sndLightning3 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning3");
global.sndLightning1 = mod_script_call("mod", "NTU", "ntu_sound", "sndLightning1");
#define weapon_name
return "ELEMENTAL GUN";
#define weapon_type
return 4;
#define weapon_auto
return 1;
#define weapon_cost
return 3;
#define weapon_load
return 10;
#define weapon_sprt
return global.sprElementalGun;
#define weapon_area
return 15;
#define weapon_swap
return sndSwapDragon;
#define weapon_text
return "the four ultra elements";
#define weapon_fire
var __angle = gunangle;
var __laser_brain = mod_script_call("mod", "NTU", "ntu_wep_laser_brain");
if (!instance_exists(/*!*/SnowSound)) instance_create(x, y, /*!*/SnowSound);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "SnowBurst")) {
	creator = other.id;
	ammo = 4;
	time = 1;
	team = other.team;
	event_perform(ev_alarm, 0);
	burnafter = 1;
}
sound_play(global.sndFrostShot1);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "FreezeBullet")) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
sound_play(sndFireShotgun);
repeat (4) {
	with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Bullet6")) {
		motion_add(__angle + (random(40) - 20) * other.accuracy, 12 + random(6));
		image_angle = direction;
		team = other.team;
		creator = other;
	}
}
sound_play(sndRoll);
sound_play(sndBloodLauncher);
sound_play(choose(global.sndWater1, global.sndWater2));
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Tentacle")) {
	image_angle = __angle + (random(30) - 15) * other.accuracy;
	creator = other.id;
	team = other.team;
	ammo = 15;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) {
		sprite_index = global.sprTentacleSpawn;
		image_angle = other.image_angle;
	}
	repeat (6) with (instance_create(x, y, FishBoost)) motion_add(__angle + random(60) - 30, 2 + random(4));
}
if (__laser_brain) {
	sound_play(choose(global.sndLightning2, global.sndLightning3));
} else sound_play(global.sndLightning1);
with (instance_create(x, y, Lightning)) {
	image_angle = __angle + (random(6) - 3) * other.accuracy;
	team = other.team;
	ammo = 15;
	event_perform(ev_alarm, 0);
	visible = false;
	with (instance_create(x, y, LightningSpawn)) image_angle = other.image_angle;
}
if (wep_type[wep] == 4) {
	wep_type[wep] = 5;
} else wep_type[wep] = 4;
weapon_post(6, -9, 12);

///// NTU TERMITE GUN.wep.gml
global.sndTermite = mod_script_call("mod", "NTU", "ntu_sound", "sndTermite");
#define weapon_name
return "TERMITE GUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 2;
#define weapon_sprt
return sprSmg;
#define weapon_area
return 10;
#define weapon_swap
return sndSwapMotorized;
#define weapon_text
return "termites eat enemy mutants";
#define weapon_fire
var __angle = gunangle;
sound_play(global.sndTermite);
with (mod_script_call("mod", "NTU", "ntu_create", x, y, "Termite")) {
	motion_add(__angle + (random(32) - 16) * other.accuracy, 4 + irandom(8));
	team = other.team;
	creator = other;
}
weapon_post(2, -4, 2);
