#define init
global.sprOmegaRevolver = sprite_add_weapon("sprOmegaRevolver.png", 5, 7);
#define weapon_name
return "Omega Revolver";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 0;
#define weapon_sprt
return global.sprOmegaRevolver;
#define weapon_area
return 0;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndPistol);
with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
with (instance_create(x, y, Bullet1)) {
	motion_add(__angle + (random(8) - 4) * other.accuracy, 16);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(2, -6, 4);