#define init
global.sprPlantmg = sprite_add_weapon("sprPlantmg.png", 11, 15);
global.pmgjammed = 0;
#define weapon_name
return "PLANT'S MACHINEGUN";
#define weapon_type
return 1;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 5;
#define weapon_sprt
return global.sprPlantmg;
#define weapon_area
return 0;
#define weapon_swap
return sndSwapMachinegun;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
if (global.pmgjammed == 0){
	pmgjam = irandom(5);
	if (pmgjam = 4) {
		sound_play(sndEmpty);
		global.pmgjammed = 1;
	} else {
		sound_play(sndMachinegun);
		with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
		with (instance_create(x, y, Bullet1)) {
			motion_add(__angle + (random(12) - 6) * other.accuracy, 16);
			image_angle = direction;
			team = other.team;
			creator = other;
		}
	}
} else {
	sound_play(sndHeavyMachinegun);
	with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
	with (instance_create(x, y, Shell)) motion_add(__angle + other.right * 100 + random(50) - 25, 2 + random(2));
	with (instance_create(x, y, Bullet1)) {
		motion_add(__angle + (random(8)) * other.accuracy, 16);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	with (instance_create(x, y, Bullet1)) {
		motion_add(__angle + (random(8) - 8) * other.accuracy, 16);
		image_angle = direction;
		team = other.team;
		creator = other;
	}
	global.pmgjammed = 0
}
weapon_post(2, -6, 3);