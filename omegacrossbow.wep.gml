#define init
global.sprOmegaCrossbow = sprite_add_weapon("sprOmegaCrossbow.png", 1, 6);
#define weapon_name
return "Omega Crossbow";
#define weapon_type
return 3;
#define weapon_auto
return 1;
#define weapon_cost
return 1;
#define weapon_load
return 0;
#define weapon_sprt
return global.sprOmegaCrossbow;
#define weapon_area
return 1;
#define weapon_swap
return sndSwapBow;
#define weapon_text
return "";
#define weapon_fire
var __angle = gunangle;
sound_play(sndCrossbow);
with (instance_create(x, y, Bolt)) {
	motion_add(__angle, 24);
	image_angle = direction;
	team = other.team;
	creator = other;
}
weapon_post(4, -40, 4);